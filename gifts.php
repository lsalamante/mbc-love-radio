<?php
#INCLUDES
include('jp_library/jp_lib.php');
include('jp_library/s_upload.php');

if (!isset($_SESSION['is_logged_in'])) {
  header("Location: " . "login.php");
  die();
}

$DYNAMIC_TABLE = true;
$PICKERS = true;
$LOADER = true;

if (isset($_POST['title']) &&
isset($_POST['sub_title']) &&
isset($_POST['description']) &&
isset($_POST['start_date']) &&
isset($_POST['end_date']) &&
isset($_POST['weblink']) &&
isset($_FILES['thumbnail'])
) {
  if (isset($_POST['notify']) == false) {
    $_POST['notify'] = 0;
  }

  $all_ok = 0;

  #HACK
  $date = $_POST['start_date'];
  $date = explode("-", $date);
  $_POST['start_date'] = date('Y-m-d', strtotime("$date[2]-$date[0]-$date[1]"));

  $date = $_POST['end_date'];
  $date = explode("-", $date);
  $_POST['end_date'] = date('Y-m-d', strtotime("$date[2]-$date[0]-$date[1]"));

  $params['table'] = "gifts";
  $params['data'] = $_POST;
  $result = jp_add($params);

  $last_gift_id = jp_last_added(); #get our last ID

  if ($result) {
    $s_upload = s_upload("uploads/", "gift_thumbnail_", $_FILES["thumbnail"], $last_gift_id);

    if ($s_upload['status']) {
      $thumbnail = $BASE_URL . $s_upload['path']; #get file URL

      unset($params); #unset our favourite variable right here
      unset($result); #unset our favourite variable right here

      $params['table'] = 'gifts';
      $params['where'] = "id = $last_gift_id";

      $params['data'] = array(
        "thumbnail" => $thumbnail,
      );

      $result = jp_update($params);

      if ($_POST['notify'] == 1) {
        ###########FIREBASE HERE
        require_once __DIR__ . '/firebase.php';
        require_once __DIR__ . '/gift.php';

        $firebase = new Firebase();
        $gift = new Gift();

        $gift->setId($last_gift_id);
        $gift->setTitle($_POST['title']);
        $gift->setSubTitle($_POST['sub_title']);
        $gift->setDescription($_POST['description']);
        $gift->setStartDate($_POST['start_date']);
        $gift->setEndDate($_POST['end_date']);
        $gift->setThumbnailUrl($thumbnail);
        $gift->setWeblink($_POST['weblink']);

        $json = '';
        $response = '';

        $json = $gift->getGift();

        $notification['title'] = "New gift available!";
        $notification['body'] = $_POST['title'];

        $response = $firebase->sendToTopic('exclusive_content', $json, $notification);
        ###########/ FIREBASE HERE
      }

      if ($result) {
        $all_ok = 1;
      }
    }
  }

  if ($all_ok) {
    $status_msg = "Successfully added new gift.";
  } else {
    $status_msg = "Failed to add new gift.";
  }

}

if (isset($_POST['delete_id'])) {
  #delete from DB
  $delete_id = $_POST['delete_id'];

  $d['select'] = "thumbnail";
  $d['table'] = "gifts";
  $d['where'] = "id = $delete_id";

  $fileUrl = mysqli_fetch_assoc(jp_get($d));
  $fileUrl = array_pop(explode("/", $fileUrl['thumbnail']));

  $params['table'] = "gifts";
  $params['where'] = "id = $delete_id";
  $result = jp_delete($params);

  #delete from DIR
  $unlinked = 0;
  $file = "uploads/" . $fileUrl;
  if (unlink($file)) {
    $unlinked = 1;
  }

  if ($result || $unlinked) {
    $status_msg = "Row deleted.";
    $all_ok = 1;
  }
}

#Refresh our variables right here
unset($params);
unset($result);
#VIEWING
$params['table'] = "gifts";
$params['filters'] = "ORDER BY id DESC";
$gifts = jp_get($params);

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading"> Add new gift
                <br> <sub <?php if (isset($all_ok)) { if ($all_ok) { echo "class='status-ok'"; } else { echo "class='status-not-ok'"; } ?>
                  <?php } ?>
                  ><?php echo isset($status_msg) ? $status_msg : ''; ?>
                </header>
                <div class="panel-body">
                  <form class="form-horizontal" role="form"
                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                  enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="title" class="col-lg-2 col-sm-2 control-label">Title</label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="title" name="title"
                      placeholder="Gift title" required></div>
                    </div>
                    <div class="form-group">
                      <label for="sub_title" class="col-lg-2 col-sm-2 control-label">Sub title</label>
                      <div class="col-lg-10">
                        <input type="text" class="form-control" id="sub_title" name="sub_title" required
                        placeholder="Sub title"></div>
                      </div>
                      <div class="form-group">
                        <label for="location" class="col-lg-2 col-sm-2 control-label">Description</label>
                        <div class="col-lg-10">
                          <textarea  class="form-control" id="description" name="description" placeholder="Gift description" ></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                          <label for="weblink" class="col-lg-2 col-sm-2 control-label">Web link</label>
                          <div class="col-lg-10">
                              <input type="url" class="form-control" id="weblink" name="weblink"
                                     placeholder="Link to external websites"></div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-lg-2">Start date</label>
                        <div class="col-lg-10">
                          <input class="form-control form-control-inline input-medium default-date-picker"
                          required placeholder="MM-DD-YYYY"
                          name="start_date" type="text" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-lg-2">End date</label>
                        <div class="col-lg-10">
                          <input class="form-control form-control-inline input-medium default-date-picker"
                          required placeholder="MM-DD-YYYY"
                          name="end_date" type="text" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="home_file" class="col-lg-2 col-sm-2 control-label">Thumbnail</label>
                        <div class="col-lg-10">
                          <input type="file" id="thumbnail" name="thumbnail" required>
                          <div class="loader-div hidden"><img class="loader" src="img/hourglass.gif"/>
                            Uploading...
                          </div>
                          <p class="help-block">Image files only. (jpeg, jpg, png, gif)</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="home_file" class="col-lg-2 col-sm-2 control-label">Notify all users for
                          adding this item?</label>
                          <div class="col-lg-10">
                            <input type="checkbox" value="1" style="width:20px"
                            class="checkbox form-control " name="notify">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-info">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <section class="panel">
                      <header class="panel-heading"> Gifts<span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                      </span></header>
                      <div class="panel-body">
                        <div class="adv-table">
                          <table class="display table table-bordered table-striped" id="dynamic-table">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Sub title</th>
                                <th>Description</th>
                                <th>Start date</th>
                                <th>End date</th>
                                <th>Thumbnail URL</th>
                                <th>Weblink</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              foreach ($gifts as $row) {
                                echo '<tr>';
                                echo '<td>' . $row["id"] . '</td>';
                                echo '<td>' . $row["title"] . '</td>';
                                echo '<td>' . $row["sub_title"] . '</td>';
                                echo '<td>' . $row["description"] . '</td>';

                                $row['start_date'] = date('M d, Y', strtotime($row['start_date']));
                                echo '<td>' . $row["start_date"] . '</td>';
                                $row['end_date'] = date('M d, Y', strtotime($row['end_date']));
                                echo '<td>' . $row["end_date"] . '</td>';

                                echo '<td><a href="' . $row["thumbnail"] . '" target="_blank">' . $row["thumbnail"] . '</a></td>';
                                echo '<td>' . $row["weblink"] . '</td>';
                                echo '<td>'; ?>

                                  <form style='display:inline;'
                                  onsubmit="return confirm('Edit row #<?= $row['id'] ?>?');"
                                  action="edit_gift.php" method="post" class="no-loader"s>
                                  <input type="hidden" name="edit_id" value="<?= $row["id"]; ?>">
                                  <input type="hidden" name="edit_page" value="<?= $PAGE_NAME ?>">
                                  <button class="btn btn-primary btn-xs" type="submit"><i
                                    class="fa fa-pencil "></i></button>
                                  </form>

                                  <form style='display:inline;'
                                  onsubmit="return confirm('Are you sure you want to delete that?');"
                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post" class="no-loader">
                                  <input type="hidden" name="delete_id" value="<?php echo $row["id"]; ?>">
                                  <button class="btn btn-danger btn-xs" type="submit"><i
                                    class="fa fa-trash-o "></i></button>
                                  </form>
                                  <?php
                                  echo '</td>';
                                  echo '</tr>';
                                }

                                ?>
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th>#</th>
                                  <th>Title</th>
                                  <th>Sub title</th>
                                  <th>Description</th>
                                  <th>Start date</th>
                                  <th>End date</th>
                                  <th>Thumbnail URL</th>
                                  <th>Weblink</th>
                                  <th></th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                        </div>
                      </section>
                    </div>

                  </div>
                  <!-- page end-->
                </section>
              </section>
              <!--main content end-->
              <!-- Right Slidebar start -->
              <?php
              if ($RIGHT_SIDEBAR) {
                include('right-sidebar.php');
              }
              ?>
              <!-- Right Slidebar end -->
              <!--footer start-->
              <?php include('footer.php'); ?>
              <!--footer end-->
            </section>
            <?php include('scripts.php'); ?>
          </body>

          </html>
