<?php

class Gift
{

    private $title;
    private $sub_title;
    private $description;
    private $start_date;
    private $end_date;
    private $thumbnail;
    private $id;
    private $weblink;

    function __construct()
    {

    }

    public function setTitle($arg)
    {
        $this->title = $arg;
    }
    public function setId($arg)
    {
        $this->id = $arg;
    }

    public function setSubTitle($arg)
    {
        $this->sub_title = $arg;
    }

    public function setDescription($arg)
    {
        $this->description = $arg;
    }

    public function setStartDate($arg)
    {
        $this->start_date = $arg;
    }

    public function setEndDate($arg)
    {
        $this->end_date = $arg;
    }

    public function setThumbnailUrl($arg)
    {
        $this->thumbnail = $arg;
    }
    public function setWeblink($arg)
    {
        $this->weblink = $arg;
    }

    public function getGift()
    {
        $res = array();
        $res['data']['status'] = true;
        $res['data']['category'] = "gift";

        $res['data']['id'] = $this->id;
        $res['data']['title'] = $this->title;
        $res['data']['sub_title'] = $this->sub_title;
        $res['data']['description'] = $this->description;
        $res['data']['start_date'] = $this->start_date;
        $res['data']['end_date'] = $this->end_date;
        $res['data']['thumbnail'] = $this->thumbnail;
        $res['data']['weblink'] = $this->weblink;

        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }

}
