<?php 
#INCLUDES
include('jp_library/jp_lib.php');

if(isset($_SESSION['is_logged_in'])){
    header("Location: " . "index.php");
    die();
}

?>
    <!DOCTYPE html>
    <html lang="en">
    <?php include('header.php') ?>

        <body class="login-body">
            <div class="container">
                <form class="form-signin" action="index.php" method="post">
                    <h2 class="form-signin-heading">sign in now</h2>
                    <div class="login-wrap">
<!--                    TODO: CHANGE TO EMAIL TYPE-->
                        <input type="email" style="font-size:12px" class="form-control" placeholder="Email" autofocus required name="email">
                        <input style="margin-top:10px" type="password" class="form-control" placeholder="Password" required name="password">
                        <div><?php if(isset($_SESSION['err_msg']) != "") echo $_SESSION['err_msg']; ?></div>
<!--                        <label class="checkbox">-->
<!--                            <input type="checkbox" value="remember-me"> Remember me <span class="pull-right">-->
<!--                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>-->
<!---->
<!--                </span> </label>-->
                        <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
                        
                    </div>
                    <!-- Modal -->
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Forgot Password?</h4> </div>
                                <div class="modal-body">
                                    <p>Enter your e-mail address below to reset your password.</p>
                                    <input type="text" name="recovery_email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix"> </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                    <button class="btn btn-success" type="button">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- modal -->
                </form>
            </div>
            <!-- js placed at the end of the document so the pages load faster -->
            <script src="js/jquery.js"></script>
            <script src="js/bootstrap.min.js"></script>
        </body>

    </html>