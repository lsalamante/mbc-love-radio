<?php

class Song
{

    private $title;
    private $artist;
    private $lyrics;
    private $thumbnail;

    function __construct()
    {

    }

    public function setTitle($arg)
    {
        $this->title = $arg;
    }

    public function setArtist($arg)
    {
        $this->artist = $arg;
    }

    public function setLyrics($arg)
    {
        $this->lyrics = $arg;
    }

    public function setThumbnailUrl($arg)
    {
        $this->thumbnail = $arg;
    }

    public function getSong()
    {
        $res = array();
        $res['data']['status'] = true;
        $res['data']['category'] = "song";

        $res['data']['title'] = $this->title;
        $res['data']['artist'] = $this->artist;
        $res['data']['lyrics'] = $this->lyrics;
        $res['data']['thumbnail'] = $this->thumbnail;

        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }

}
