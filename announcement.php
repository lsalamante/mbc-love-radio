<?php
class Announcement {

    private $title;
    private $message;

    function __construct() {
        
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function getAnnouncement() {
        $res = array();
        $res['data']['status'] = true;
        $res['data']['category'] = "announcement";
        $res['data']['title'] = $this->title;
        $res['data']['message'] = $this->message;
        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }

}
