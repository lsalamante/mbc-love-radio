<?php
#INCLUDES
include('jp_library/jp_lib.php');

if (isset($_POST['email']) && isset($_POST['password'])) {


    #GET THE ID AND FULL NAME
    $params['table'] = "admin";
    $params['where'] = "email = '" . $_POST['email'] . "'";

    $row = mysqli_fetch_assoc(jp_get($params));

    $id = $row['id'];
    $full_name = $row['fname'] . ' ' . $row['lname'];

    unset($params);

    $params['table'] = "admin";
    $params['where'] = "id = '" . $id . "'";

    $row = mysqli_fetch_assoc(jp_get($params));

    if (sha1($_POST['password']) == $row['password']) {

        $_SESSION['full_name'] = $full_name;

        $_SESSION['is_logged_in'] = 1;

        $_SESSION['my_id'] = $id;

    } else {
        $_SESSION['err_msg'] = "Wrong email or password.";
        header("Location: " . "login.php");
        die();
    }


} else if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}


$DYNAMIC_TABLE = true;

#QUERIES
#not sure if i'm putting it in the right place
#EDIT: Yep. Right place
#Procedural style

#INSERT
if (isset($_POST['fname']) && isset($_POST['lname']) && isset($_POST['add_email']) && isset($_POST['add_password'])) {
    $params['table'] = 'admin';
    $params['data'] = array(
        "fname" => $_POST['fname'],
        "lname" => $_POST['lname'],
        "email" => $_POST['add_email'],
        "password" => sha1($_POST['add_password']),
    );
    $result = jp_add($params);
    if ($result) {
        $status_msg = "Successfully added.";
        $all_ok = 1;
    } else {
        $status_msg = "Duplicate email. Please try again.";
        $all_ok = 0;
    }
} else if (isset($_POST['delete_id'])) {
    $delete_id = $_POST['delete_id'];
    $params['table'] = "admin";
    $params['where'] = "id = '$delete_id'";
    $result = jp_delete($params);
    if ($result) {
        $status_msg = "Row deleted.";
        $all_ok = 1;
    }
}

unset($params);
#VIEWING
$params['table'] = "admin";
#EXCLUDE YOURSELF
$params['where'] = "id != '" . $_SESSION['my_id'] . "'";
$admin = jp_get($params);

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
<section id="container">
    <!--header start-->
    <header class="header white-bg">
        <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
        <!--logo start-->
        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
            <!--  notification end -->
        </div>
        <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- page start-->
            <div class="row">

                <div class="col-lg-6">
                    <section class="panel">
                        <header class="panel-heading"> Add new administrator<br> <sub
                                <?php if (isset($all_ok)) {
                                    if ($all_ok) {
                                        echo "class='status-ok'";
                                    } else {
                                        echo "class='status-not-ok'";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo isset($status_msg) ? $status_msg : ''; ?></sub></header>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form"
                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">
                                <div class="form-group">
                                    <label for="fname" class="col-lg-2 col-sm-2 control-label">First Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="fname" name="fname"
                                               placeholder="First name"></div>
                                </div>
                                <div class="form-group">
                                    <label for="lname" class="col-lg-2 col-sm-2 control-label">Last Name</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="lname" name="lname"
                                               placeholder="Last name"></div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-lg-2 col-sm-2 control-label">Email</label>
                                    <div class="col-lg-10">
                                        <input type="email" class="form-control" id="email" name="add_email"
                                               placeholder="Email" required>
                                        <p class="help-block">This field is required.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-lg-2 col-sm-2 control-label">Password</label>
                                    <div class="col-lg-10">
                                        <input type="password" class="form-control" id="password" name="add_password"
                                               placeholder="Password" pattern=".{8,}" required
                                               title="8 characters minimum.">
                                        <p class="help-block">This field is required.</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
                <div class="col-lg-6">
                    <section class="panel">
                        <header class="panel-heading"> Administrators <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span></header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($admin as $row) {
                                        echo '<tr>';
                                        echo '<td>' . $row["id"] . '</td>';
                                        echo '<td>' . $row["fname"] . '</td>';
                                        echo '<td>' . $row["lname"] . '</td>';
                                        echo '<td>' . $row["email"] . '</td>';
                                        echo '<td>'; ?>

                                        <form style='display:inline;'
                                              onsubmit="return confirm('Edit row #<?= $row['id'] ?>?');"
                                              action="edit_admin.php" method="post">
                                            <input type="hidden" name="edit_id" value="<?= $row["id"]; ?>">
                                            <input type="hidden" name="edit_page" value="<?= $PAGE_NAME ?>">
                                            <button class="btn btn-primary btn-xs" type="submit"><i
                                                    class="fa fa-pencil "></i></button>
                                        </form>

                                        <form style='display:inline;'
                                              onsubmit="return confirm('Are you sure you want to delete that?');"
                                              action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">
                                            <input type="hidden" name="delete_id" value="<?php echo $row["id"]; ?>">
                                            <button class="btn btn-danger btn-xs" type="submit"><i
                                                    class="fa fa-trash-o "></i></button>
                                        </form>

                                        <?php
                                        echo '</td>';
                                        echo '</tr>';
                                    }

                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>First name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
</section>
<?php include('scripts.php'); ?>
</body>

</html>