<?php
include('jp_library/jp_lib.php');
include('jp_library/s_upload.php');

if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}
if (!isset($_POST['edit_id'])) {
    header("Location: " . "vods.php");
    die();
}
if (isset($_POST['update'])) {

    $all_ok = 0;

    $params['table'] = 'vods';
    $params['data'] = array(
        'title' => $_POST['title'],
        'category_id' => $_POST['category_id'],
        'link' => $_POST['link'],
        'duration' => $_POST['duration'],
    );
    $edit_id = $_POST['edit_id'];
    $params['where'] = "id = $edit_id";

    $result = jp_update($params);

    $s_upload['status'] = 1;

    if ($_FILES['thumbnail']['name'] != "") {
        $s_upload = s_upload("uploads/", "vod_thumbnail_", $_FILES["thumbnail"], $_POST['edit_id']);

        if ($s_upload['status']) { #if upload is successful
            $thumbnail = $BASE_URL . $s_upload['path']; #get file URL

            unset($params); #unset our favourite variable right here
            unset($result); #unset our favourite variable right here

            $params['table'] = 'vods';
            $params['where'] = "id = $edit_id";

            $params['data'] = array(
                "thumbnail" => $thumbnail,
            );

            $result = jp_update($params);
        }
    }

    if ($s_upload['status']) {
        $status_msg = "Updated successfully.<br><a href='" . $_POST['edit_page'] . "'>&laquo; Back to VODs</a>";
        $all_ok= 1;
    } else {
        $status_msg = "Update failed.";
    }
}

$DYNAMIC_TABLE = true;
$PICKERS = true;

$params['table'] = "vods";
$params['where'] = "id = '" . $_POST['edit_id'] . "'";
$result = mysqli_fetch_assoc(jp_get($params));

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
<section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
        <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
        <!--logo start-->
        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
            <!--  notification end -->
        </div>
        <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- page start-->

            <div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?= $_POST['edit_page'] ?>">&laquo; Back to VODs</a></li>
                    <li>Edit</a></li>
                </ul>
                <!--breadcrumbs end -->
            </div>

            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading"> Edit VOD
                        <br> <sub
                            <?php if (isset($all_ok)) {
                                if ($all_ok) {
                                    echo "class='status-ok'";
                                } else {
                                    echo "class='status-not-ok'";
                                }
                                ?>
                            <?php } ?>
                        ><?php echo isset($status_msg) ? $status_msg : ''; ?></sub>
                    </header>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form"
                              action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                              enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-lg-2 col-sm-2 control-label">Title</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="title" name="title"
                                           placeholder="Event title" required value="<?= $result['title'] ?>"></div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-lg-2 col-sm-2 control-label">VOD Category</label>
                                <div class="col-lg-10">
                                    <select class="form-control" name="category_id" required>
                                        <?php
                                        unset($params);
                                        $params['table'] = "vod_category";
                                        $params['filters'] = "ORDER BY id DESC";
                                        $categories = jp_get($params);
                                        foreach ($categories as $row) {
                                            $id = $row['id'];
                                            $name = $row['title'];

                                            $selected = "";
                                            if ($result['category_id'] == $id) {
                                                $selected = "selected";
                                            }

                                            echo "<option value='$id' " . $selected . ">$name</option>";
                                            unset($selected);
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="link" class="col-lg-2 col-sm-2 control-label">VOD link</label>
                                <div class="col-lg-10">
                                    <input type="url" class="form-control" id="link" name="link" required placeholder="http://example.com/videos/123456789/player?" value="<?= $result['link'] ?>">
                                    <span class="help-block">Please enter a valid VOD URL. Example:  <em> http://example.com/videos/123456789/player?</em></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-lg-2 col-sm-2 control-label">Duration</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="duration" name="duration" required placeholder="hh:mm:ss" pattern="([0-5][0-9]):([0-5][0-9]):([0-5][0-9])" required title="hh:mm:ss" data-mask="99:99:99" value="<?= $result['duration'] ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="thumbnail_preview" class="col-lg-2 col-sm-2 control-label">Thumbnail
                                    preview</label>
                                <div class="col-lg-10">
                                    <img class="img-responsive" src="<?= $result['thumbnail'] ?>"/></div>
                            </div>
                            <div class="form-group">
                                <label for="home_file" class="col-lg-2 col-sm-2 control-label">Thumbnail</label>
                                <div class="col-lg-10">
                                    <input type="file" id="thumbnail" name="thumbnail">
                                    <p class="help-block">Image files only. (jpeg, jpg, png, gif)</p>
                                </div>
                            </div>
                            <input type="hidden" value="1" name="update">
                            <input type="hidden" value="<?= $_POST['edit_id'] ?>" name="edit_id">
                            <input type="hidden" value="<?= $_POST['edit_page'] ?>" name="edit_page">
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>


            <!-- page end-->
        </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
</section>
<?php include('scripts.php'); ?>
</body>

</html>
