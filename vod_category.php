<?php
#INCLUDES
include('jp_library/jp_lib.php');
include('jp_library/s_upload.php');

if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}

$DYNAMIC_TABLE = true;
$LOADER = true;

if (isset($_POST['title']) &&
    isset($_FILES['thumbnail'])
) {

    $all_ok = 0;

    $params['table'] = "vod_category";
    $params['data'] = $_POST;
    $result = jp_add($params);

    $last_category_id = jp_last_added(); #get our last ID

    if ($result) {
        $s_upload = s_upload("uploads/", "vod_category_", $_FILES["thumbnail"], $last_category_id);

        if ($s_upload['status']) { #if upload is successful
            $thumbnail = $BASE_URL . $s_upload['path']; #get file URL

            unset($params); #unset our favourite variable right here
            unset($result); #unset our favourite variable right here

            $params['table'] = 'vod_category';
            $params['where'] = "id = $last_category_id";

            $params['data'] = array(
                "thumbnail" => $thumbnail,
            );

            $result = jp_update($params);
            if ($result) {
                $all_ok = 1;
            }
        } else { #delete last id
            #HACK only!
            #todo make it so that it never inserts at all
            #possible or no?
            unset($params); #unset our favourite variable right here
            unset($result); #unset our favourite variable right here

            #delete from DB
            $delete_id = $last_category_id;
            $params['table'] = "vod_category";
            $params['where'] = "id = $delete_id";
            $result = jp_delete($params);
        }
    }

    if ($all_ok) {
        $status_msg = "Successfully added new category.";
    } else {
        $status_msg = "Failed to add new category.";
    }

}

if (isset($_POST['disable_id'])) {
    #delete from DB
    $disable_id = $_POST['disable_id'];
    $params['table'] = "vod_category";
    $params['where'] = "id = $disable_id";
    $params['data'] = array(
      "status" => 0,
    );
    $result = jp_update($params);

    if ($result) {
        $status_msg = "Category disabled.";
        $all_ok = 1;
        unset($params);
        unset($result);
    }
}

if (isset($_POST['enable_id'])) {
    #delete from DB
    $enable_id = $_POST['enable_id'];
    $params['table'] = "vod_category";
    $params['where'] = "id = $enable_id";
    $params['data'] = array(
        "status" => 1,
    );
    $result = jp_update($params);

    if ($result) {
        $status_msg = "Category enabled.";
        $all_ok = 1;
        unset($params);
        unset($result);
    }
}

if(isset($_POST['feature'])){
  $id = $_POST['feature_id'];
  $params['table'] = "vod_category";
  $params['where'] = "id = $id";
  $params['data'] = array(
      "is_featured" => 1,
      "featured_at" => date("Y-m-d h:i:s")
  );
  $result = jp_update($params);

  if ($result) {
      $status_msg = "Category featured.";
      $all_ok = 1;
      unset($params);
      unset($result);
  }
}else if(isset($_POST['unfeature'])){
  $id = $_POST['feature_id'];
  $params['table'] = "vod_category";
  $params['where'] = "id = $id";
  $params['data'] = array(
      "is_featured" => 0,
      "featured_at" => null

  );
  $result = jp_update($params);

  if ($result) {
      $status_msg = "Category unfeatured.";
      $all_ok = 1;
      unset($params);
      unset($result);
  }
}
#Refresh our variables right here
unset($params);
#VIEWING
$params['table'] = "vod_category";
$params['filters'] = "ORDER BY id DESC";
$vod_category = jp_get($params);

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
<section id="container">
    <!--header start-->
    <header class="header white-bg">
        <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
        <!--logo start-->
        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
            <!--  notification end -->
        </div>
        <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-6">
                    <section class="panel">
                        <header class="panel-heading"> Add new category
                            <br> <sub
                                <?php if (isset($all_ok)) {
                                    if ($all_ok) {
                                        echo "class='status-ok'";
                                    } else {
                                        echo "class='status-not-ok'";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo isset($status_msg) ? $status_msg : ''; ?></sub>
                        </header>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form"
                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                                  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="title" class="col-lg-2 col-sm-2 control-label">Category Title</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Category title" required></div>
                                </div>
                                <div class="form-group">
                                    <label for="home_file" class="col-lg-2 col-sm-2 control-label">Thumbnail</label>
                                    <div class="col-lg-10">
                                        <input type="file" id="thumbnail" name="thumbnail" required>
                                        <div class="loader-div hidden"><img class="loader" src="img/hourglass.gif" /> Uploading...</div>
                                        <p class="help-block">Image files only. (jpeg, jpg, png, gif)</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
                <div class="col-lg-6">
                    <section class="panel">
                        <header class="panel-heading"> VOD Categories<span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span></header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category</th>
                                        <th>Thumbnail URL</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($vod_category as $row) {
                                        echo '<tr>';
                                        echo '<td>' . $row["id"] . '</td>';
                                        echo '<td>' . $row["title"] . '</td>';
                                        echo '<td><a href="' . $row["thumbnail"] . '" target="_blank">' . $row["thumbnail"] . '</a></td>';
                                        echo '<td>'; ?>

                                        <form style='display:inline;'
                                              onsubmit="return confirm('Edit row #<?= $row['id'] ?>?');"
                                              action="edit_vod_category.php" method="post" class="no-loader">
                                            <input type="hidden" name="edit_id" value="<?= $row["id"]; ?>">
                                            <input type="hidden" name="edit_page" value="<?= $PAGE_NAME ?>">
                                            <button class="btn btn-primary btn-xs" type="submit"><i
                                                    class="fa fa-pencil "></i></button>
                                        </form>
                                        <?php if($row['status'] == 1){ ?>
                                        <form style='display:inline;'
                                              onsubmit="return confirm('Are you sure you want to disable this category?');"
                                              action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                                              class="no-loader">
                                            <input type="hidden" name="disable_id" value="<?php echo $row["id"]; ?>">
                                            <button class="btn btn-danger btn-xs" title="Disable" type="submit"><i
                                                    class="fa fa-ban "></i></button>
                                        </form>
                                        <?php }else if($row['status'] == 0){?>
                                            <form style='display:inline;'
                                                  onsubmit="return confirm('Are you sure you want to enable this category?');"
                                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                                                  class="no-loader">
                                                <input type="hidden" name="enable_id" value="<?php echo $row["id"]; ?>">
                                                <button class="btn btn-info btn-xs" title="Enable" type="submit"><i
                                                        class="fa fa-check-circle "></i></button>
                                            </form>
                                        <?php }?>
                                        <?php if($row['is_featured'] == 1){ ?>
                                        <form style='display:inline;'
                                              onsubmit="return confirm('Are you sure you want to remove this category from the featured list?');"
                                              action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                                              class="no-loader">
                                            <input type="hidden" name="feature_id" value="<?php echo $row["id"]; ?>">
                                            <button class="btn btn-danger btn-xs" title="un-feature" type="submit" name="unfeature"><i
                                                    class="fa fa-star-o"></i></button>
                                        </form>
                                        <?php }else if($row['is_featured'] == 0){?>
                                            <form style='display:inline;'
                                                  onsubmit="return confirm('Are you sure you want to feature this category?');"
                                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                                                  class="no-loader">
                                                <input type="hidden" name="feature_id" value="<?php echo $row["id"]; ?>">
                                                <button class="btn btn-warning btn-xs" title="feature" type="submit" name="feature"><i
                                                        class="fa fa-star"></i></button>
                                            </form>
                                        <?php }?>
                                        <?php
                                        echo '</td>';
                                        echo '</tr>';
                                    }

                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Thumbnail URL</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
</section>
<?php include('scripts.php'); ?>
</body>

</html>
