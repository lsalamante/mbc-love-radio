<aside>
  <div id="sidebar" class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <li>
        Admin
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'index.php' || $PAGE_NAME == 'edit_admin.php') echo 'active'?>" href="index.php"> <i class="fa fa-lock"></i> <span>Admin</span> </a>
      </li>
      <li>
        App data
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'home.php') echo 'active'?>" href="home.php"> <i class="fa fa-home"></i> <span>App homescreen</span> </a>
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'vod_category.php' || $PAGE_NAME == 'edit_vod_category.php') echo 'active'?>" href="vod_category.php"> <i class="fa fa-book"></i> <span>VOD categories</span> </a>
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'terms.php') echo 'active'?>" href="terms.php"> <i class="fa fa-file-text-o"></i> <span>Terms of use</span> </a>
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'privacy.php') echo 'active'?>" href="privacy.php"> <i class="fa fa-exclamation"></i> <span>Privacy policy</span> </a>
      </li>
      <li>
        App data with notifications
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'vods.php' || $PAGE_NAME == 'edit_vod.php') echo 'active'?>" href="vods.php"> <i class="fa fa-video-camera"></i> <span>Videos on demand</span> </a>
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'events.php' || $PAGE_NAME == 'edit_event.php') echo 'active'?>" href="events.php"> <i class="fa fa-calendar"></i> <span>Events</span> </a>
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'gifts.php' || $PAGE_NAME == 'edit_gift.php') echo 'active'?>" href="gifts.php"> <i class="fa fa-gift"></i> <span>Gifts</span> </a>
      </li>
      <li>
        Livestreaming
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'livestream_video.php') echo 'active'?>" href="livestream_video.php"> <i class="fa fa-cloud"></i> <span>App livestream video</span> </a>
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'livestream_radio.php') echo 'active'?>" href="livestream_radio.php"> <i class="fa fa-headphones"></i> <span>App livestream radio</span> </a>
      </li>
      <li>
        Notifications
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'announcements.php') echo 'active'?>" href="announcements.php"> <i class="fa fa-bullhorn"></i> <span>Announcements</span> </a>
      </li>
      <li>
        End users
      </li>
      <li>
        <a class="<?php if($PAGE_NAME == 'users.php') echo 'active'?>" href="users.php"> <i class="fa fa-user"></i> <span>Registered users</span> </a>
      </li>

      &nbsp;
      <!--            TODO HYPERLINKS-->

      <?php if($LOCAL_MODE) { ?>
        <li>
          &nbsp;
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-laptop"></i> <span>Layouts</span> </a>
          <ul class="sub">
            <li><a href="boxed_page.html">Boxed Page</a></li>
            <li><a href="horizontal_menu.html">Horizontal Menu</a></li>
            <li><a href="header-color.html">Different Color Top bar</a></li>
            <li><a href="mega_menu.html">Mega Menu</a></li>
            <li><a href="language_switch_bar.html">Language Switch Bar</a></li>
            <li><a href="email_template.html" target="_blank">Email Template</a></li>
          </ul>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-book"></i> <span>UI Elements</span> </a>
          <ul class="sub">
            <li><a href="general.html">General</a></li>
            <li><a href="buttons.html">Buttons</a></li>
            <li><a href="modal.html">Modal</a></li>
            <li><a href="toastr.html">Toastr Notifications</a></li>
            <li><a href="widget.html">Widget</a></li>
            <li><a href="slider.html">Slider</a></li>
            <li><a href="nestable.html">Nestable</a></li>
            <li><a href="font_awesome.html">Font Awesome</a></li>
          </ul>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-cogs"></i> <span>Components</span> </a>
          <ul class="sub">
            <li><a href="grids.html">Grids</a></li>
            <li><a href="calendar.html">Calendar</a></li>
            <li><a href="gallery.html">Gallery</a></li>
            <li><a href="todo_list.html">Todo List</a></li>
            <li><a href="draggable_portlet.html">Draggable Portlet</a></li>
            <li><a href="tree.html">Tree View</a></li>
          </ul>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-tasks"></i> <span>Form Stuff</span> </a>
          <ul class="sub">
            <li><a href="form_component.html">Form Components</a></li>
            <li><a href="advanced_form_components.html">Advanced Components</a></li>
            <li><a href="form_wizard.html">Form Wizard</a></li>
            <li><a href="form_validation.html">Form Validation</a></li>
            <li><a href="dropzone.html">Dropzone File Upload</a></li>
            <li><a href="inline_editor.html">Inline Editor</a></li>
            <li><a href="image_cropping.html">Image Cropping</a></li>
            <li><a href="file_upload.html">Multiple File Upload</a></li>
          </ul>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-th"></i> <span>Data Tables</span> </a>
          <ul class="sub">
            <li><a href="basic_table.html">Basic Table</a></li>
            <li><a href="responsive_table.html">Responsive Table</a></li>
            <li><a href="dynamic_table.html">Dynamic Table</a></li>
            <li><a href="editable_table.html">Editable Table</a></li>
          </ul>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class=" fa fa-envelope"></i> <span>Mail</span> </a>
          <ul class="sub">
            <li><a href="inbox.html">Inbox</a></li>
            <li><a href="inbox_details.html">Inbox Details</a></li>
          </ul>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class=" fa fa-bar-chart-o"></i> <span>Charts</span> </a>
          <ul class="sub">
            <li><a href="morris.html">Morris</a></li>
            <li><a href="chartjs.html">Chartjs</a></li>
            <li><a href="flot_chart.html">Flot Charts</a></li>
            <li><a href="xchart.html">xChart</a></li>
          </ul>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-shopping-cart"></i> <span>Shop</span> </a>
          <ul class="sub">
            <li><a href="product_list.html">List View</a></li>
            <li><a href="product_details.html">Details View</a></li>
          </ul>
        </li>
        <li>
          <a href="google_maps.html"> <i class="fa fa-map-marker"></i> <span>Google Maps </span> </a>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-comments-o"></i> <span>Chat Room</span> </a>
          <ul class="sub">
            <li><a href="lobby.html">Lobby</a></li>
            <li><a href="chat_room.html"> Chat Room</a></li>
          </ul>
        </li>
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-glass"></i> <span>Extra</span> </a>
          <ul class="sub">
            <li><a href="blank.html">Blank Page</a></li>
            <li><a href="sidebar_closed.html">Sidebar Closed</a></li>
            <li><a href="people_directory.html">People Directory</a></li>
            <li><a href="coming_soon.html">Coming Soon</a></li>
            <li><a href="lock_screen.html">Lock Screen</a></li>
            <li><a href="profile.html">Profile</a></li>
            <li><a href="invoice.html">Invoice</a></li>
            <li><a href="project_list.html">Project List</a></li>
            <li><a href="project_details.html">Project Details</a></li>
            <li><a href="search_result.html">Search Result</a></li>
            <li><a href="pricing_table.html">Pricing Table</a></li>
            <li><a href="faq.html">FAQ</a></li>
            <li><a href="fb_wall.html">FB Wall</a></li>
            <li><a href="404.html">404 Error</a></li>
            <li><a href="500.html">500 Error</a></li>
          </ul>
        </li>
        <li>
          <a href="login.html"> <i class="fa fa-user"></i> <span>Login Page</span> </a>
        </li>
        <!--multi level menu start-->
        <li class="sub-menu">
          <a href="javascript:;"> <i class="fa fa-sitemap"></i> <span>Multi level Menu</span> </a>
          <ul class="sub">
            <li><a href="javascript:;">Menu Item 1</a></li>
            <li class="sub-menu"> <a href="boxed_page.html">Menu Item 2</a>
              <ul class="sub">
                <li><a href="javascript:;">Menu Item 2.1</a></li>
                <li class="sub-menu"> <a href="javascript:;">Menu Item 3</a>
                  <ul class="sub">
                    <li><a href="javascript:;">Menu Item 3.1</a></li>
                    <li><a href="javascript:;">Menu Item 3.2</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <?php }  ?>
        <!--multi level menu end-->
      </ul>
      <!-- sidebar menu end-->
    </div>
  </aside>
