-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2017 at 09:14 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `fname`, `lname`, `email`, `password`) VALUES
(6, 'John', 'Doe', 'john@doe.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8'),
(9, 'Love', 'Admin', 'admin@loveradio.com.ph', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `chat_message` mediumtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `user_id`, `nickname`, `chat_message`, `timestamp`) VALUES
(2, 1, '', '', '2016-11-24 06:43:46'),
(3, 2, '', '', '2016-11-24 06:44:52'),
(4, 2, '3213', '123', '2016-11-24 06:45:40'),
(5, 2, '123', '321123', '2016-11-24 06:45:40'),
(6, 3, '', '', '2016-11-24 06:43:46'),
(7, 3, '', '', '2016-11-24 06:44:52'),
(8, 4, '3213', '123', '2016-11-24 06:45:40'),
(9, 3, '123', '321123', '2016-11-24 06:45:40'),
(10, 4, '', '', '2016-11-24 06:43:46'),
(11, 3, '', '', '2016-11-24 06:44:52'),
(12, 5, '3213', '123', '2016-11-24 06:45:40'),
(13, 3, '123', '321123', '2016-11-24 06:45:40'),
(14, 1, '', '', '2016-11-24 06:43:46'),
(15, 1, '', '', '2016-11-24 06:44:52'),
(16, 1, '3213', '123', '2016-11-24 06:45:40'),
(17, 1, '123', '321123', '2016-11-24 06:45:40'),
(18, 1, 'vorname', 'amsdaskldaklsdlkashdjlashdl', '2016-11-24 07:44:27'),
(19, 1, 'vorname', 'amsdaskldaklsdlkashdjlashdl', '2016-11-24 07:45:08'),
(20, 1, 'vorname', 'amsdaskldaklsdlkashdjlashdl', '2016-11-24 07:45:24');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `device_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `month_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `sub_title` text NOT NULL,
  `location` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `description` text NOT NULL,
  `thumbnail` text NOT NULL,
  `weblink` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `month_id`, `title`, `sub_title`, `location`, `date`, `time`, `description`, `thumbnail`, `weblink`) VALUES
(1, 10, 'Rakrakan Festival', 'October Fest', 'Metro Walk', '2016-09-29', '10:00:00', 'Full description', 'http://betaprojex.com/loveradio-app/uploads/event_thumbnail_1.jpg', ''),
(2, 12, 'Christmas concert', 'Lorem ipsum', 'A quick brown fox avenue ', '2016-12-24', '12:00:00', 'Full description', 'http://betaprojex.com/loveradio-app/uploads/event_thumbnail_2.jpg', ''),
(3, 12, 'Christmas concert part 2', 'Grumpy wizards', 'Hello world', '2016-12-25', '19:00:00', 'Full description', 'http://betaprojex.com/loveradio-app/uploads/event_thumbnail_3.jpg', ''),
(4, 2, 'Valentines Day', 'Valentines Concert', 'SM Mall of Asia', '2016-02-14', '18:00:00', 'Full description', 'http://betaprojex.com/loveradio-app/uploads/event_thumbnail_4.jpg', ''),
(5, 3, 'et commodo', 'dapibus dolor vel', 'pulvinar sed', '2016-03-03', '12:48:00', 'Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.', 'https://robohash.org/saepereprehenderitcommodi.jpg?size=300x300&set=set1', ''),
(6, 8, 'fermentum', 'nullam orci', 'enim in tempor turpis nec euismod', '2016-08-18', '08:24:00', 'Phasellus id sapien in sapien iaculis congue.', 'https://robohash.org/aperiammaximenobis.bmp?size=300x300&set=set1', ''),
(7, 2, 'fermentum donec ut', 'neque', 'orci pede venenatis non sodales sed tincidunt eu felis fusce', '2016-02-09', '03:45:00', 'In quis justo.', 'https://robohash.org/aliascorporisharum.png?size=300x300&set=set1', ''),
(8, 3, 'luctus ultricies eu', 'dis', 'non mauris morbi non lectus aliquam sit amet diam', '2016-03-22', '11:30:00', 'Nullam varius. Nulla facilisi.', 'https://robohash.org/insapienteaut.bmp?size=300x300&set=set1', ''),
(9, 5, 'vestibulum eget vulputate', 'habitasse platea', 'orci mauris lacinia sapien quis', '2016-05-11', '11:00:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/utinmaiores.png?size=300x300&set=set1', ''),
(10, 7, 'ante nulla justo', 'ut erat curabitur', 'justo', '2016-07-29', '06:47:00', 'Cras in purus eu magna vulputate luctus.', 'https://robohash.org/officiaharumnihil.png?size=300x300&set=set1', ''),
(11, 10, 'non', 'dolor', 'duis bibendum felis sed interdum venenatis turpis', '2015-10-29', '01:11:00', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.', 'https://robohash.org/velremid.jpg?size=300x300&set=set1', ''),
(12, 1, 'consequat', 'donec semper sapien a', 'dui nec nisi volutpat eleifend donec ut dolor morbi vel', '2016-01-06', '09:35:00', 'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 'https://robohash.org/laudantiumvelitex.jpg?size=300x300&set=set1', ''),
(13, 11, 'mus', 'erat', 'vestibulum ante ipsum primis in faucibus orci luctus', '2015-11-13', '10:44:00', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 'https://robohash.org/autetdebitis.bmp?size=300x300&set=set1', ''),
(14, 12, 'augue aliquam', 'eget tincidunt', 'ornare imperdiet sapien urna', '2015-12-01', '07:33:00', 'Nullam varius.', 'https://robohash.org/illorerumquibusdam.jpg?size=300x300&set=set1', ''),
(15, 10, 'in hac', 'id mauris vulputate elementum nullam', 'nisl aenean lectus pellentesque eget nunc donec quis', '2015-10-24', '04:53:00', 'Donec vitae nisi.', 'https://robohash.org/quisetporro.jpg?size=300x300&set=set1', ''),
(16, 3, 'felis donec semper', 'orci', 'potenti in eleifend', '2016-03-20', '10:06:00', 'Integer ac neque. Duis bibendum.', 'https://robohash.org/doloraliquidrepellat.jpg?size=300x300&set=set1', ''),
(17, 12, 'tristique fusce', 'quis augue luctus', 'id lobortis convallis tortor risus dapibus augue vel', '2015-12-22', '07:52:00', 'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.', 'https://robohash.org/minuseosaut.jpg?size=300x300&set=set1', ''),
(18, 6, 'quam', 'blandit nam nulla integer', 'sollicitudin mi sit amet lobortis sapien sapien non', '2016-06-21', '03:13:00', 'Morbi non quam nec dui luctus rutrum. Nulla tellus.', 'https://robohash.org/ullamdoloremnulla.bmp?size=300x300&set=set1', ''),
(19, 2, 'proin interdum mauris', 'nulla eget eros', 'ut massa volutpat convallis morbi', '2016-02-25', '04:03:00', 'Curabitur convallis.', 'https://robohash.org/sapientequisnecessitatibus.bmp?size=300x300&set=set1', ''),
(20, 8, 'augue vestibulum', 'feugiat et eros', 'ipsum ac tellus semper interdum mauris ullamcorper purus sit', '2016-08-08', '03:39:00', 'Fusce consequat.', 'https://robohash.org/magnammaximevitae.jpg?size=300x300&set=set1', ''),
(21, 5, 'ultrices aliquet maecenas', 'ut ultrices vel augue', 'consequat lectus in est risus auctor', '2016-05-08', '10:01:00', 'Nulla tempus.', 'https://robohash.org/animidoloremaut.png?size=300x300&set=set1', ''),
(22, 12, 'ut', 'iaculis diam erat', 'enim leo rhoncus sed vestibulum sit amet', '2015-12-03', '01:10:00', 'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.', 'https://robohash.org/veniamutquam.jpg?size=300x300&set=set1', ''),
(23, 10, 'in faucibus', 'eu orci mauris lacinia', 'maecenas leo odio condimentum id luctus nec molestie sed justo', '2016-10-14', '07:21:00', 'Quisque ut erat.', 'https://robohash.org/ducimusquiat.png?size=300x300&set=set1', ''),
(24, 6, 'nisi at nibh', 'nunc commodo placerat praesent', 'dictumst etiam faucibus cursus urna ut tellus nulla ut', '2016-06-28', '12:55:00', 'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.', 'https://robohash.org/vitaeevenietlaboriosam.jpg?size=300x300&set=set1', ''),
(25, 1, 'massa', 'consectetuer adipiscing elit proin interdum', 'ante', '2016-01-24', '04:52:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.', 'https://robohash.org/recusandaeexcepturivoluptatum.png?size=300x300&set=set1', ''),
(26, 11, 'aliquet maecenas', 'mus', 'justo pellentesque viverra pede', '2015-11-13', '12:06:00', 'Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.', 'https://robohash.org/voluptaslaboreut.bmp?size=300x300&set=set1', ''),
(27, 5, 'convallis eget eleifend', 'est risus auctor sed', 'platea dictumst morbi', '2016-05-10', '07:13:00', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.', 'https://robohash.org/exidvel.png?size=300x300&set=set1', ''),
(28, 3, 'morbi non', 'volutpat quam pede lobortis ligula', 'luctus et ultrices posuere cubilia', '2016-03-29', '01:31:00', 'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.', 'https://robohash.org/voluptatemautemsunt.png?size=300x300&set=set1', ''),
(29, 4, 'interdum', 'pellentesque eget', 'nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum', '2016-04-09', '02:59:00', 'Aliquam non mauris.', 'https://robohash.org/atabveritatis.bmp?size=300x300&set=set1', ''),
(30, 9, 'augue luctus', 'est', 'vivamus metus arcu adipiscing', '2016-09-05', '02:32:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/eumdoloresmollitia.jpg?size=300x300&set=set1', ''),
(31, 6, 'nunc', 'odio consequat varius integer', 'nec condimentum neque sapien', '2016-06-03', '06:27:00', 'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.', 'https://robohash.org/autempariaturipsa.bmp?size=300x300&set=set1', ''),
(32, 2, 'ipsum', 'justo eu massa', 'fusce posuere felis sed lacus', '2016-02-12', '11:15:00', 'Nulla mollis molestie lorem.', 'https://robohash.org/consequunturpossimusomnis.jpg?size=300x300&set=set1', ''),
(33, 8, 'arcu libero', 'tristique tortor eu', 'id massa id nisl venenatis lacinia aenean sit amet', '2016-08-19', '07:14:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.', 'https://robohash.org/maioresnamporro.png?size=300x300&set=set1', ''),
(34, 9, 'pretium quis', 'nulla mollis molestie', 'purus aliquet at feugiat non pretium quis', '2016-09-29', '06:41:00', 'Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.', 'https://robohash.org/voluptasfugitasperiores.bmp?size=300x300&set=set1', ''),
(35, 11, 'justo in', 'et commodo vulputate justo', 'donec vitae nisi nam ultrices libero non mattis pulvinar nulla', '2015-11-14', '04:32:00', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 'https://robohash.org/eiusarchitectorepellat.bmp?size=300x300&set=set1', ''),
(36, 8, 'nisi nam ultrices', 'tortor id nulla ultrices', 'justo pellentesque viverra pede ac diam', '2016-08-04', '06:05:00', 'Duis aliquam convallis nunc.', 'https://robohash.org/eafugitaliquam.jpg?size=300x300&set=set1', ''),
(37, 5, 'hac', 'ipsum primis in faucibus orci', 'gravida sem praesent id massa id nisl venenatis lacinia', '2016-05-24', '09:21:00', 'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 'https://robohash.org/temporerepellatratione.png?size=300x300&set=set1', ''),
(38, 7, 'sit', 'in hac habitasse platea', 'amet nunc viverra dapibus nulla suscipit ligula in', '2016-07-01', '02:49:00', 'Etiam faucibus cursus urna. Ut tellus.', 'https://robohash.org/reiciendiscommodiofficia.jpg?size=300x300&set=set1', ''),
(39, 11, 'nisi nam ultrices', 'libero nullam', 'sem fusce consequat', '2015-11-14', '06:02:00', 'Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.', 'https://robohash.org/nesciuntaccusantiumtotam.png?size=300x300&set=set1', ''),
(40, 11, 'vivamus vestibulum sagittis', 'cras in purus eu', 'nulla', '2015-11-12', '09:12:00', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.', 'https://robohash.org/excepturipraesentiumexplicabo.bmp?size=300x300&set=set1', ''),
(41, 2, 'lacus morbi', 'montes nascetur ridiculus mus', 'quis', '2016-02-23', '03:57:00', 'Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.', 'https://robohash.org/porrodoloriure.jpg?size=300x300&set=set1', ''),
(42, 2, 'elementum pellentesque quisque', 'nulla neque', 'nisi at nibh in', '2016-02-26', '02:32:00', 'Pellentesque ultrices mattis odio. Donec vitae nisi.', 'https://robohash.org/etimpeditharum.bmp?size=300x300&set=set1', ''),
(43, 5, 'pede lobortis', 'ligula nec sem duis aliquam', 'pede venenatis non sodales', '2016-05-03', '05:31:00', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 'https://robohash.org/nesciuntquasipsa.png?size=300x300&set=set1', ''),
(44, 6, 'dictumst etiam', 'justo in', 'nulla suspendisse potenti cras in purus eu magna vulputate', '2016-06-16', '03:37:00', 'Suspendisse potenti.', 'https://robohash.org/facereblanditiisharum.jpg?size=300x300&set=set1', ''),
(45, 10, 'nonummy maecenas', 'in quis justo', 'luctus nec molestie sed justo pellentesque', '2016-10-13', '11:34:00', 'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.', 'https://robohash.org/nisiaccusamusdolorum.bmp?size=300x300&set=set1', ''),
(46, 2, 'sodales scelerisque', 'porta volutpat quam pede lobortis', 'ut blandit non interdum', '2016-02-19', '04:50:00', 'Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'https://robohash.org/explicaboquamsaepe.png?size=300x300&set=set1', ''),
(47, 1, 'quis', 'erat vestibulum sed magna', 'erat id mauris vulputate', '2016-01-28', '12:49:00', 'Phasellus in felis.', 'https://robohash.org/inciduntdeseruntminus.png?size=300x300&set=set1', ''),
(48, 4, 'scelerisque quam', 'lorem vitae mattis nibh', 'sapien sapien non mi integer ac', '2016-04-19', '08:54:00', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'https://robohash.org/corporiscumoptio.bmp?size=300x300&set=set1', ''),
(49, 6, 'in magna bibendum', 'metus vitae', 'ut dolor morbi', '2016-06-03', '01:37:00', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 'https://robohash.org/estiustosequi.png?size=300x300&set=set1', ''),
(50, 2, 'ut erat id', 'a libero nam', 'justo sit amet sapien', '2016-02-09', '09:55:00', 'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.', 'https://robohash.org/rerumteneturmodi.bmp?size=300x300&set=set1', ''),
(51, 12, 'ac enim in', 'ultrices', 'ante ipsum primis in faucibus orci luctus et ultrices posuere', '2015-12-02', '06:54:00', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 'https://robohash.org/perferendismolestiaequis.jpg?size=300x300&set=set1', ''),
(52, 7, 'commodo', 'nam', 'quam sollicitudin', '2016-07-24', '11:15:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/quieaquevoluptatem.jpg?size=300x300&set=set1', ''),
(53, 5, 'amet', 'odio', 'tellus nisi eu orci mauris', '2016-05-04', '03:46:00', 'Etiam justo. Etiam pretium iaculis justo.', 'https://robohash.org/doloremquasiut.jpg?size=300x300&set=set1', ''),
(54, 4, 'et ultrices posuere', 'dui vel sem', 'sapien arcu sed augue aliquam', '2016-04-09', '11:53:00', 'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.', 'https://robohash.org/quiasimiliquevoluptate.bmp?size=300x300&set=set1', ''),
(55, 10, 'lorem ipsum dolor', 'non mi integer ac', 'a suscipit nulla elit ac nulla sed vel', '2016-10-02', '08:37:00', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis.', 'https://robohash.org/accusantiumtemporeullam.bmp?size=300x300&set=set1', ''),
(56, 11, 'nullam porttitor', 'nisi eu orci', 'mauris morbi non', '2015-11-26', '12:59:00', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.', 'https://robohash.org/delenitinisisaepe.png?size=300x300&set=set1', ''),
(57, 4, 'dui nec', 'quis odio consequat varius integer', 'vestibulum ante', '2016-04-24', '03:02:00', 'Suspendisse potenti.', 'https://robohash.org/laboriosamquiomnis.bmp?size=300x300&set=set1', ''),
(58, 8, 'vel nisl duis', 'hendrerit at vulputate', 'habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam', '2016-08-21', '07:13:00', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.', 'https://robohash.org/doloremvoluptatesdicta.bmp?size=300x300&set=set1', ''),
(59, 10, 'libero rutrum ac', 'aenean', 'vehicula', '2016-10-05', '04:03:00', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'https://robohash.org/quoabsequi.jpg?size=300x300&set=set1', ''),
(60, 8, 'platea dictumst', 'natoque penatibus', 'volutpat convallis morbi odio odio elementum eu', '2016-08-10', '10:31:00', 'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.', 'https://robohash.org/quoetanimi.jpg?size=300x300&set=set1', ''),
(61, 8, 'praesent blandit lacinia', 'montes', 'et ultrices posuere cubilia curae duis faucibus accumsan', '2016-08-21', '09:38:00', 'Pellentesque at nulla. Suspendisse potenti.', 'https://robohash.org/nihildolorumsapiente.bmp?size=300x300&set=set1', ''),
(62, 12, 'ipsum', 'potenti in eleifend quam a', 'morbi odio odio', '2015-12-21', '03:24:00', 'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.', 'https://robohash.org/quiaadipiscideserunt.bmp?size=300x300&set=set1', ''),
(63, 11, 'a odio', 'vulputate nonummy maecenas tincidunt', 'ultrices mattis odio donec vitae', '2015-11-20', '09:21:00', 'Suspendisse potenti.', 'https://robohash.org/idetdeleniti.jpg?size=300x300&set=set1', ''),
(64, 11, 'molestie lorem quisque', 'tincidunt nulla mollis molestie', 'etiam vel augue vestibulum', '2015-11-29', '08:03:00', 'Donec ut mauris eget massa tempor convallis.', 'https://robohash.org/laborevelitconsequatur.jpg?size=300x300&set=set1', ''),
(65, 11, 'morbi quis tortor', 'faucibus orci luctus', 'maecenas pulvinar lobortis', '2015-11-23', '06:47:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.', 'https://robohash.org/dolorumminusmagni.png?size=300x300&set=set1', ''),
(66, 5, 'elementum', 'amet justo morbi ut', 'lacus morbi quis tortor id nulla ultrices aliquet maecenas', '2016-05-04', '08:39:00', 'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'https://robohash.org/quibusdamcommodirerum.jpg?size=300x300&set=set1', ''),
(67, 5, 'erat', 'vivamus tortor', 'lectus pellentesque eget nunc donec quis orci eget orci', '2016-05-25', '11:51:00', 'Nunc nisl.', 'https://robohash.org/nonperferendisut.bmp?size=300x300&set=set1', ''),
(68, 12, 'luctus et', 'neque libero convallis', 'vestibulum eget vulputate ut ultrices vel augue vestibulum', '2015-12-17', '07:35:00', 'Pellentesque ultrices mattis odio. Donec vitae nisi.', 'https://robohash.org/accusamusporroaspernatur.png?size=300x300&set=set1', ''),
(69, 2, 'dis', 'lorem vitae mattis', 'lacinia aenean sit amet', '2016-02-27', '08:37:00', 'Praesent blandit lacinia erat.', 'https://robohash.org/cumqueenimquo.jpg?size=300x300&set=set1', ''),
(70, 10, 'lorem id ligula', 'eget', 'vestibulum quam sapien varius ut blandit', '2016-10-01', '08:52:00', 'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.', 'https://robohash.org/abeataein.bmp?size=300x300&set=set1', ''),
(71, 11, 'amet nunc', 'feugiat non pretium', 'amet nulla quisque', '2015-11-02', '03:03:00', 'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.', 'https://robohash.org/evenietsapientecupiditate.jpg?size=300x300&set=set1', ''),
(72, 10, 'nulla neque', 'felis eu sapien', 'pellentesque volutpat dui maecenas tristique est', '2015-10-24', '04:17:00', 'Aliquam sit amet diam in magna bibendum imperdiet.', 'https://robohash.org/molestiasdictaaccusamus.png?size=300x300&set=set1', ''),
(73, 12, 'orci', 'ultrices mattis odio donec', 'accumsan odio curabitur convallis duis', '2015-12-01', '07:27:00', 'Curabitur convallis.', 'https://robohash.org/porroearumeaque.png?size=300x300&set=set1', ''),
(74, 1, 'lacinia nisi venenatis', 'diam id ornare imperdiet', 'morbi odio odio elementum eu interdum', '2016-01-09', '09:27:00', 'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.', 'https://robohash.org/ullamperferendisblanditiis.jpg?size=300x300&set=set1', ''),
(75, 3, 'tristique fusce congue', 'a ipsum integer a', 'sit amet erat', '2016-03-21', '07:10:00', 'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/etiustoquas.jpg?size=300x300&set=set1', ''),
(76, 5, 'turpis eget', 'rutrum nulla nunc', 'etiam vel augue vestibulum rutrum', '2016-05-05', '07:20:00', 'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.', 'https://robohash.org/nonilloquis.png?size=300x300&set=set1', ''),
(77, 8, 'dui', 'eleifend', 'sollicitudin', '2016-08-23', '07:34:00', 'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.', 'https://robohash.org/repudiandaemagniet.bmp?size=300x300&set=set1', ''),
(78, 8, 'posuere felis sed', 'lobortis', 'nulla tempus vivamus in felis eu', '2016-08-12', '01:27:00', 'Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 'https://robohash.org/esthicanimi.png?size=300x300&set=set1', ''),
(79, 5, 'magnis dis', 'donec pharetra magna', 'eget tincidunt eget tempus vel pede morbi porttitor lorem', '2016-05-02', '09:45:00', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.', 'https://robohash.org/consequaturadistinctio.jpg?size=300x300&set=set1', ''),
(80, 1, 'lacus', 'congue etiam justo', 'ac nulla sed vel enim sit', '2016-01-26', '04:45:00', 'Integer ac leo.', 'https://robohash.org/laudantiumfacereeaque.png?size=300x300&set=set1', ''),
(81, 7, 'accumsan', 'nulla sed vel', 'turpis', '2016-07-03', '11:55:00', 'Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 'https://robohash.org/totamdoloremvoluptas.bmp?size=300x300&set=set1', ''),
(82, 4, 'ultrices posuere cubilia', 'nulla tellus', 'at feugiat non pretium quis lectus suspendisse potenti', '2016-04-25', '08:21:00', 'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.', 'https://robohash.org/velitnonquod.jpg?size=300x300&set=set1', ''),
(83, 8, 'at ipsum ac', 'nunc', 'nisl aenean', '2016-08-21', '09:29:00', 'Integer ac neque. Duis bibendum.', 'https://robohash.org/nesciuntquibusdamea.jpg?size=300x300&set=set1', ''),
(84, 5, 'vestibulum', 'neque', 'curae nulla dapibus dolor vel est donec odio justo', '2016-05-17', '07:22:00', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'https://robohash.org/verosintexpedita.png?size=300x300&set=set1', ''),
(85, 2, 'a', 'nisl nunc', 'semper est quam pharetra magna ac consequat metus', '2016-02-05', '11:46:00', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'https://robohash.org/cupiditatenullaeligendi.jpg?size=300x300&set=set1', ''),
(86, 10, 'sed magna at', 'cras pellentesque volutpat dui', 'erat', '2016-10-04', '11:49:00', 'Maecenas tincidunt lacus at velit.', 'https://robohash.org/eaerrormodi.png?size=300x300&set=set1', ''),
(87, 12, 'massa', 'vel augue vestibulum ante', 'diam cras pellentesque volutpat dui maecenas tristique est et', '2015-12-25', '12:16:00', 'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.', 'https://robohash.org/culpamagnamqui.png?size=300x300&set=set1', ''),
(88, 10, 'sapien a libero', 'enim blandit mi in', 'vivamus vestibulum sagittis sapien cum sociis natoque penatibus et', '2016-10-06', '10:11:00', 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.', 'https://robohash.org/earumquasiminus.png?size=300x300&set=set1', ''),
(89, 7, 'elit ac nulla', 'ac diam cras pellentesque', 'eu tincidunt in leo', '2016-07-12', '01:15:00', 'Nulla ut erat id mauris vulputate elementum.', 'https://robohash.org/velitexpeditaest.bmp?size=300x300&set=set1', ''),
(90, 12, 'sit amet', 'erat fermentum', 'dapibus nulla suscipit ligula', '2015-12-27', '12:08:00', 'Nunc rhoncus dui vel sem.', 'https://robohash.org/providentetvoluptas.jpg?size=300x300&set=set1', ''),
(91, 10, 'lobortis ligula sit', 'primis in faucibus orci luctus', 'neque', '2016-10-18', '03:35:00', 'In eleifend quam a odio.', 'https://robohash.org/euminesse.jpg?size=300x300&set=set1', ''),
(92, 4, 'lacinia eget tincidunt', 'amet turpis elementum', 'sapien urna', '2016-04-22', '09:53:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/recusandaeaccusamusiste.jpg?size=300x300&set=set1', ''),
(93, 7, 'nulla ut erat', 'justo', 'parturient montes nascetur ridiculus mus vivamus vestibulum sagittis', '2016-07-04', '02:39:00', 'Integer tincidunt ante vel ipsum.', 'https://robohash.org/etharumet.png?size=300x300&set=set1', ''),
(94, 6, 'rutrum', 'convallis nunc proin at', 'quis augue', '2016-06-30', '01:03:00', 'Duis at velit eu est congue elementum.', 'https://robohash.org/utsaepeconsequatur.jpg?size=300x300&set=set1', ''),
(95, 3, 'dui maecenas tristique', 'primis in faucibus orci', 'viverra pede ac', '2016-03-29', '05:40:00', 'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.', 'https://robohash.org/teneturvoluptatumdoloremque.jpg?size=300x300&set=set1', ''),
(96, 2, 'vivamus vel', 'purus eu magna', 'ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean', '2016-02-29', '05:45:00', 'Nulla suscipit ligula in lacus.', 'https://robohash.org/facerenequelaudantium.bmp?size=300x300&set=set1', ''),
(97, 5, 'eget eleifend', 'gravida nisi', 'nulla ac enim in tempor turpis nec euismod scelerisque quam', '2016-05-04', '11:13:00', 'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 'https://robohash.org/ipsaconsequunturmagni.png?size=300x300&set=set1', ''),
(98, 8, 'luctus', 'primis in faucibus', 'proin at turpis a pede', '2016-08-29', '04:20:00', 'Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.', 'https://robohash.org/inventoreadipisciqui.png?size=300x300&set=set1', ''),
(99, 5, 'placerat praesent blandit', 'amet diam in', 'suspendisse potenti nullam porttitor lacus at', '2016-05-03', '08:05:00', 'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 'https://robohash.org/eamagnicorrupti.png?size=300x300&set=set1', ''),
(100, 9, 'lectus', 'morbi', 'diam in magna bibendum imperdiet', '2016-09-01', '05:32:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/sinteiuslaboriosam.bmp?size=300x300&set=set1', ''),
(101, 12, 'eu', 'turpis nec euismod scelerisque quam', 'vel augue vestibulum ante ipsum primis', '2015-12-26', '09:11:00', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.', 'https://robohash.org/numquamprovidentut.bmp?size=300x300&set=set1', ''),
(102, 10, 'nisl venenatis', 'venenatis lacinia aenean', 'augue', '2015-10-22', '07:47:00', 'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 'https://robohash.org/quiavoluptatemveritatis.bmp?size=300x300&set=set1', ''),
(103, 8, 'consequat', 'velit', 'justo maecenas rhoncus aliquam lacus', '2016-08-10', '02:37:00', 'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.', 'https://robohash.org/utvoluptatemdolorem.jpg?size=300x300&set=set1', ''),
(104, 12, 'vel nisl', 'convallis tortor risus dapibus', 'cubilia curae duis faucibus accumsan odio curabitur convallis duis', '2015-12-03', '02:23:00', 'In hac habitasse platea dictumst.', 'https://robohash.org/distinctioeosdeleniti.png?size=300x300&set=set1', ''),
(105, 12, 'dolor', 'id nulla ultrices aliquet maecenas', 'ultrices phasellus id sapien in sapien iaculis congue', '2015-12-13', '11:48:00', 'Praesent blandit. Nam nulla.', 'https://robohash.org/nihilsuscipitquidem.png?size=300x300&set=set1', ''),
(106, 2, 'eu', 'aliquam convallis nunc proin', 'suspendisse potenti nullam porttitor lacus at', '2016-02-09', '05:32:00', 'Etiam faucibus cursus urna.', 'https://robohash.org/temporibusvoluptatemest.png?size=300x300&set=set1', ''),
(107, 5, 'lacus at', 'sapien a libero nam', 'lorem integer tincidunt ante vel ipsum praesent', '2016-05-31', '11:16:00', 'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.', 'https://robohash.org/debitismaximererum.jpg?size=300x300&set=set1', ''),
(108, 8, 'orci luctus et', 'aliquet', 'blandit mi in porttitor', '2016-08-10', '11:16:00', 'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.', 'https://robohash.org/nobisadinventore.jpg?size=300x300&set=set1', ''),
(109, 7, 'quis libero nullam', 'pede ullamcorper', 'augue aliquam erat volutpat in congue', '2016-07-12', '05:57:00', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis.', 'https://robohash.org/iddignissimosperspiciatis.jpg?size=300x300&set=set1', ''),
(110, 3, 'dictumst maecenas ut', 'sem sed sagittis', 'mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus', '2016-03-04', '05:12:00', 'Suspendisse accumsan tortor quis turpis. Sed ante.', 'https://robohash.org/porronesciuntquibusdam.jpg?size=300x300&set=set1', ''),
(111, 10, 'amet sapien dignissim', 'rutrum neque aenean', 'eu mi nulla ac enim in tempor turpis nec', '2016-10-20', '01:08:00', 'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.', 'https://robohash.org/suscipitveniamaccusamus.jpg?size=300x300&set=set1', ''),
(112, 11, 'libero nullam', 'magna ac consequat', 'maecenas rhoncus aliquam lacus morbi quis tortor id nulla', '2015-11-09', '06:37:00', 'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.', 'https://robohash.org/abmodiet.jpg?size=300x300&set=set1', ''),
(113, 8, 'aliquet pulvinar sed', 'accumsan tortor quis turpis', 'luctus et ultrices posuere cubilia curae nulla dapibus', '2016-08-19', '06:19:00', 'Donec dapibus.', 'https://robohash.org/suscipitquiaqui.bmp?size=300x300&set=set1', ''),
(114, 5, 'dui', 'nec nisi volutpat eleifend donec', 'quam pharetra magna ac consequat metus sapien ut', '2016-05-28', '05:13:00', 'Fusce posuere felis sed lacus.', 'https://robohash.org/nequeestquia.png?size=300x300&set=set1', ''),
(115, 7, 'ac', 'odio curabitur convallis duis', 'dolor sit amet consectetuer adipiscing elit', '2016-07-06', '08:45:00', 'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.', 'https://robohash.org/itaqueerroreveniet.jpg?size=300x300&set=set1', ''),
(116, 11, 'pede venenatis', 'et commodo vulputate justo in', 'id mauris vulputate', '2015-11-02', '05:06:00', 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.', 'https://robohash.org/remquaeratassumenda.bmp?size=300x300&set=set1', ''),
(117, 12, 'montes nascetur', 'nec nisi volutpat', 'aliquet at feugiat non pretium quis lectus', '2015-12-31', '09:48:00', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.', 'https://robohash.org/eligendieaquo.bmp?size=300x300&set=set1', ''),
(118, 8, 'pede', 'sollicitudin ut suscipit a feugiat', 'blandit lacinia erat vestibulum sed magna at', '2016-08-15', '04:55:00', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.', 'https://robohash.org/corruptietex.jpg?size=300x300&set=set1', ''),
(119, 1, 'odio', 'amet lobortis sapien', 'non velit donec diam neque vestibulum eget vulputate ut ultrices', '2016-01-27', '10:27:00', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.', 'https://robohash.org/quaedoloremquidem.bmp?size=300x300&set=set1', ''),
(120, 9, 'nunc rhoncus', 'sit', 'ut massa volutpat convallis morbi odio odio elementum', '2016-09-26', '06:55:00', 'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.', 'https://robohash.org/sedreprehenderiteos.png?size=300x300&set=set1', ''),
(121, 8, 'in congue', 'adipiscing molestie hendrerit at', 'id', '2016-08-01', '02:56:00', 'Morbi a ipsum. Integer a nibh. In quis justo.', 'https://robohash.org/corporissuntconsectetur.jpg?size=300x300&set=set1', ''),
(122, 11, 'elementum', 'magna vestibulum aliquet', 'condimentum id luctus nec molestie', '2015-11-01', '10:26:00', 'Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 'https://robohash.org/noneiusquo.bmp?size=300x300&set=set1', ''),
(123, 6, 'quam', 'lectus suspendisse potenti', 'sit amet nulla quisque arcu libero rutrum ac', '2016-06-27', '07:30:00', 'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 'https://robohash.org/temporeassumendain.png?size=300x300&set=set1', ''),
(124, 8, 'nulla', 'luctus ultricies eu', 'et eros vestibulum ac est lacinia', '2016-08-28', '06:57:00', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 'https://robohash.org/consequatureosnulla.bmp?size=300x300&set=set1', ''),
(125, 10, 'sollicitudin vitae consectetuer', 'nec sem duis aliquam', 'amet cursus', '2016-10-03', '01:48:00', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 'https://robohash.org/abeumaut.bmp?size=300x300&set=set1', ''),
(126, 4, 'cras', 'felis donec semper sapien a', 'non quam nec dui luctus rutrum nulla tellus', '2016-04-24', '02:14:00', 'Aenean lectus. Pellentesque eget nunc.', 'https://robohash.org/utoccaecatisaepe.bmp?size=300x300&set=set1', ''),
(127, 8, 'suscipit', 'volutpat in congue etiam justo', 'consectetuer adipiscing elit proin interdum mauris non ligula pellentesque', '2016-08-01', '11:06:00', 'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/voluptatemliberocommodi.png?size=300x300&set=set1', ''),
(128, 12, 'vestibulum', 'praesent blandit', 'sit amet consectetuer adipiscing elit', '2015-12-11', '12:45:00', 'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.', 'https://robohash.org/utvelet.jpg?size=300x300&set=set1', ''),
(129, 9, 'duis', 'lectus aliquam', 'accumsan tortor', '2016-09-09', '02:04:00', 'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.', 'https://robohash.org/sedsintab.bmp?size=300x300&set=set1', ''),
(130, 2, 'diam', 'at velit vivamus', 'enim blandit mi in porttitor pede justo eu massa donec', '2016-02-23', '05:39:00', 'Ut tellus.', 'https://robohash.org/quiseaqui.png?size=300x300&set=set1', ''),
(131, 11, 'id', 'blandit mi', 'cubilia curae donec pharetra magna', '2015-11-02', '03:53:00', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 'https://robohash.org/pariaturatquevoluptatibus.bmp?size=300x300&set=set1', ''),
(132, 3, 'maecenas tincidunt', 'ullamcorper', 'parturient montes nascetur ridiculus mus etiam vel', '2016-03-27', '06:46:00', 'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.', 'https://robohash.org/autemculpadeserunt.jpg?size=300x300&set=set1', ''),
(133, 3, 'at velit', 'eleifend luctus', 'ipsum', '2016-03-26', '03:56:00', 'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.', 'https://robohash.org/utsimiliqueut.bmp?size=300x300&set=set1', ''),
(134, 12, 'odio odio elementum', 'sagittis nam congue risus', 'ultrices posuere cubilia curae duis faucibus accumsan', '2015-12-20', '05:35:00', 'Proin at turpis a pede posuere nonummy. Integer non velit.', 'https://robohash.org/placeatliberosit.png?size=300x300&set=set1', ''),
(135, 1, 'semper sapien', 'bibendum imperdiet nullam', 'nisl venenatis lacinia aenean sit amet', '2016-01-22', '05:39:00', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'https://robohash.org/illumdoloremaspernatur.jpg?size=300x300&set=set1', ''),
(136, 6, 'integer ac neque', 'nullam porttitor lacus at turpis', 'sem mauris laoreet ut', '2016-06-12', '03:52:00', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', 'https://robohash.org/iustoillumfugiat.bmp?size=300x300&set=set1', ''),
(137, 12, 'donec ut mauris', 'augue quam', 'vulputate nonummy maecenas tincidunt lacus at velit vivamus', '2015-12-29', '08:16:00', 'Praesent blandit. Nam nulla.', 'https://robohash.org/rerumaliquamnecessitatibus.jpg?size=300x300&set=set1', ''),
(138, 4, 'amet', 'eu sapien cursus vestibulum proin', 'ante nulla justo aliquam quis turpis', '2016-04-03', '08:50:00', 'In quis justo.', 'https://robohash.org/innullaeum.jpg?size=300x300&set=set1', ''),
(139, 7, 'magna', 'ante ipsum primis in', 'amet', '2016-07-20', '03:16:00', 'Vivamus vestibulum sagittis sapien.', 'https://robohash.org/excepturiexpeditaquo.bmp?size=300x300&set=set1', ''),
(140, 4, 'iaculis', 'nunc vestibulum', 'proin eu mi nulla ac enim in', '2016-04-29', '04:50:00', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.', 'https://robohash.org/officiisseddolorem.jpg?size=300x300&set=set1', ''),
(141, 4, 'tincidunt nulla mollis', 'lectus suspendisse potenti', 'eleifend quam', '2016-04-20', '09:39:00', 'Nulla justo.', 'https://robohash.org/iustovoluptatumnihil.jpg?size=300x300&set=set1', ''),
(142, 3, 'sapien iaculis', 'ultrices erat', 'proin eu mi nulla ac', '2016-03-16', '07:46:00', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 'https://robohash.org/similiqueetsint.bmp?size=300x300&set=set1', ''),
(143, 10, 'cum', 'duis at velit eu est', 'cubilia curae mauris viverra diam', '2016-10-06', '11:31:00', 'Proin risus.', 'https://robohash.org/sedessevoluptatibus.bmp?size=300x300&set=set1', ''),
(144, 5, 'vulputate elementum', 'ultrices mattis odio', 'vulputate justo', '2016-05-25', '01:26:00', 'Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'https://robohash.org/natuslaborumdignissimos.jpg?size=300x300&set=set1', ''),
(145, 4, 'vestibulum', 'gravida sem praesent id massa', 'adipiscing lorem', '2016-04-04', '05:23:00', 'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.', 'https://robohash.org/quietad.bmp?size=300x300&set=set1', ''),
(146, 9, 'non', 'ante vestibulum ante ipsum', 'integer ac neque duis bibendum morbi non quam', '2016-09-23', '08:12:00', 'In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.', 'https://robohash.org/hicbeataeest.bmp?size=300x300&set=set1', ''),
(147, 6, 'nibh', 'eu orci mauris', 'ultrices posuere cubilia', '2016-06-07', '09:40:00', 'Nunc purus. Phasellus in felis. Donec semper sapien a libero.', 'https://robohash.org/aspernaturnisirerum.png?size=300x300&set=set1', ''),
(148, 7, 'sapien ut', 'neque libero convallis eget eleifend', 'interdum mauris ullamcorper', '2016-07-03', '05:12:00', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 'https://robohash.org/quaeinet.png?size=300x300&set=set1', ''),
(149, 2, 'ante vivamus tortor', 'amet', 'egestas metus aenean fermentum donec ut mauris eget', '2016-02-11', '01:02:00', 'Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 'https://robohash.org/velminimaquia.bmp?size=300x300&set=set1', ''),
(150, 9, 'nulla', 'vestibulum rutrum rutrum', 'penatibus et magnis dis parturient', '2016-09-19', '11:19:00', 'In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'https://robohash.org/distinctioeaquealiquid.png?size=300x300&set=set1', ''),
(151, 11, 'habitasse platea', 'eget semper rutrum nulla', 'porttitor pede justo eu massa donec', '2015-11-25', '04:48:00', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 'https://robohash.org/etvoluptatumet.png?size=300x300&set=set1', ''),
(152, 7, 'vestibulum', 'risus', 'donec ut mauris', '2016-07-10', '11:55:00', 'Etiam pretium iaculis justo.', 'https://robohash.org/explicaboessesit.png?size=300x300&set=set1', ''),
(153, 9, 'sit amet nunc', 'viverra', 'venenatis non sodales sed tincidunt eu felis fusce posuere', '2016-09-01', '05:00:00', 'In congue.', 'https://robohash.org/dolorumipsumneque.png?size=300x300&set=set1', ''),
(154, 3, 'proin', 'ligula in lacus curabitur at', 'neque vestibulum eget vulputate', '2016-03-01', '12:24:00', 'Aenean lectus.', 'https://robohash.org/debitisliberoeius.jpg?size=300x300&set=set1', ''),
(155, 12, 'in', 'in est risus auctor sed', 'nullam molestie', '2015-12-02', '08:44:00', 'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.', 'https://robohash.org/inciduntetblanditiis.bmp?size=300x300&set=set1', ''),
(156, 12, 'nibh', 'dapibus', 'lectus pellentesque eget nunc donec quis orci eget orci vehicula', '2015-12-07', '12:57:00', 'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.', 'https://robohash.org/aliasaliquamesse.bmp?size=300x300&set=set1', ''),
(157, 4, 'amet consectetuer adipiscing', 'in imperdiet et', 'tincidunt lacus at velit vivamus vel nulla eget', '2016-04-26', '03:12:00', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.', 'https://robohash.org/voluptastemporibusperferendis.bmp?size=300x300&set=set1', ''),
(158, 4, 'porttitor', 'metus', 'massa id lobortis convallis tortor risus', '2016-04-22', '07:42:00', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 'https://robohash.org/perferendiscumquequo.bmp?size=300x300&set=set1', ''),
(159, 4, 'sapien sapien', 'viverra eget congue eget', 'vivamus vel nulla eget eros elementum pellentesque', '2016-04-16', '10:46:00', 'Morbi a ipsum. Integer a nibh.', 'https://robohash.org/quodenimhic.png?size=300x300&set=set1', ''),
(160, 12, 'lacus at', 'quisque id justo sit amet', 'purus phasellus in', '2015-12-14', '04:19:00', 'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.', 'https://robohash.org/animinostrumdolor.bmp?size=300x300&set=set1', ''),
(161, 12, 'elementum pellentesque quisque', 'morbi porttitor', 'elementum in hac habitasse platea dictumst morbi', '2015-12-07', '02:52:00', 'Phasellus in felis. Donec semper sapien a libero.', 'https://robohash.org/nullautexplicabo.png?size=300x300&set=set1', ''),
(162, 8, 'lectus pellentesque', 'gravida sem praesent id', 'faucibus orci luctus', '2016-08-24', '01:45:00', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 'https://robohash.org/uttemporaerror.png?size=300x300&set=set1', ''),
(163, 3, 'eget nunc donec', 'sodales', 'pulvinar lobortis est phasellus sit amet erat nulla tempus', '2016-03-09', '06:35:00', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'https://robohash.org/atestodit.jpg?size=300x300&set=set1', ''),
(164, 1, 'duis faucibus', 'etiam faucibus cursus', 'massa id nisl venenatis lacinia', '2016-01-19', '05:10:00', 'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio.', 'https://robohash.org/omnisutest.png?size=300x300&set=set1', ''),
(165, 9, 'lacus morbi', 'euismod scelerisque quam turpis adipiscing', 'imperdiet sapien urna pretium nisl ut volutpat', '2016-09-22', '08:05:00', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.', 'https://robohash.org/distinctioabut.jpg?size=300x300&set=set1', ''),
(166, 9, 'a libero nam', 'augue vestibulum', 'ut nulla sed accumsan felis', '2016-09-03', '12:41:00', 'Maecenas pulvinar lobortis est.', 'https://robohash.org/magnamaliasenim.png?size=300x300&set=set1', ''),
(167, 2, 'donec pharetra magna', 'augue', 'pharetra', '2016-02-29', '02:51:00', 'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 'https://robohash.org/blanditiissuntomnis.png?size=300x300&set=set1', ''),
(168, 8, 'ultrices', 'sed magna at nunc', 'iaculis justo in hac', '2016-08-22', '04:26:00', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.', 'https://robohash.org/explicabohicet.bmp?size=300x300&set=set1', ''),
(169, 8, 'est', 'ultrices posuere', 'tristique fusce congue diam', '2016-08-18', '11:41:00', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 'https://robohash.org/quoquihic.png?size=300x300&set=set1', ''),
(170, 5, 'maecenas', 'eros vestibulum ac', 'diam id ornare imperdiet sapien urna pretium nisl', '2016-05-04', '02:37:00', 'Nulla ut erat id mauris vulputate elementum. Nullam varius.', 'https://robohash.org/autemquiminima.png?size=300x300&set=set1', ''),
(171, 4, 'pretium', 'platea dictumst maecenas ut massa', 'mi pede malesuada in imperdiet et commodo vulputate justo in', '2016-04-30', '07:26:00', 'Integer ac leo.', 'https://robohash.org/architectoestqui.png?size=300x300&set=set1', ''),
(172, 9, 'vestibulum ante', 'faucibus orci', 'diam id ornare imperdiet sapien urna pretium', '2016-09-11', '04:12:00', 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'https://robohash.org/sintbeataeodio.jpg?size=300x300&set=set1', ''),
(173, 2, 'quam nec', 'curabitur in libero ut massa', 'mauris ullamcorper purus', '2016-02-12', '03:06:00', 'Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc.', 'https://robohash.org/voluptassednihil.png?size=300x300&set=set1', ''),
(174, 5, 'nec', 'amet diam in magna bibendum', 'posuere felis sed lacus morbi sem mauris laoreet', '2016-05-23', '01:23:00', 'Vestibulum sed magna at nunc commodo placerat. Praesent blandit.', 'https://robohash.org/idrepellatsint.png?size=300x300&set=set1', ''),
(175, 2, 'in congue etiam', 'leo odio porttitor', 'vivamus tortor duis mattis', '2016-02-11', '01:53:00', 'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.', 'https://robohash.org/iniureeveniet.png?size=300x300&set=set1', ''),
(176, 8, 'eu', 'turpis integer aliquet massa', 'erat', '2016-08-28', '01:30:00', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 'https://robohash.org/etmodivoluptas.bmp?size=300x300&set=set1', ''),
(177, 10, 'nulla quisque', 'aliquet maecenas leo odio condimentum', 'odio cras', '2016-10-14', '11:42:00', 'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.', 'https://robohash.org/utautnobis.png?size=300x300&set=set1', ''),
(178, 9, 'amet', 'curae donec', 'et magnis dis parturient montes', '2016-09-24', '08:29:00', 'Pellentesque at nulla.', 'https://robohash.org/nullaasperioresminima.png?size=300x300&set=set1', ''),
(179, 6, 'eleifend luctus ultricies', 'in felis eu', 'diam cras', '2016-06-27', '05:50:00', 'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.', 'https://robohash.org/suscipitautemea.png?size=300x300&set=set1', ''),
(180, 11, 'integer ac neque', 'placerat', 'sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit', '2015-11-18', '08:40:00', 'Quisque id justo sit amet sapien dignissim vestibulum.', 'https://robohash.org/laborumsintconsequatur.png?size=300x300&set=set1', ''),
(181, 9, 'sed', 'platea dictumst', 'orci nullam molestie nibh in lectus pellentesque at nulla suspendisse', '2016-09-19', '08:33:00', 'Vivamus vel nulla eget eros elementum pellentesque.', 'https://robohash.org/placeattemporenihil.png?size=300x300&set=set1', ''),
(182, 11, 'vulputate ut', 'libero nam dui proin leo', 'pretium nisl ut volutpat', '2015-11-17', '05:37:00', 'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.', 'https://robohash.org/omnisvoluptasconsequatur.bmp?size=300x300&set=set1', ''),
(183, 4, 'integer a nibh', 'viverra eget', 'cras pellentesque volutpat dui maecenas tristique est et tempus', '2016-04-07', '01:05:00', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'https://robohash.org/quaenisiinventore.bmp?size=300x300&set=set1', '');
INSERT INTO `events` (`id`, `month_id`, `title`, `sub_title`, `location`, `date`, `time`, `description`, `thumbnail`, `weblink`) VALUES
(184, 5, 'auctor sed', 'nulla elit ac nulla sed', 'sit amet lobortis', '2016-05-20', '08:26:00', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'https://robohash.org/corrupticonsecteturet.bmp?size=300x300&set=set1', ''),
(185, 7, 'platea dictumst', 'congue eget semper rutrum nulla', 'non ligula pellentesque ultrices phasellus id', '2016-07-17', '06:40:00', 'Etiam faucibus cursus urna. Ut tellus.', 'https://robohash.org/odiorerumporro.jpg?size=300x300&set=set1', ''),
(186, 3, 'at turpis', 'non mauris morbi non lectus', 'vivamus vestibulum', '2016-03-03', '12:37:00', 'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.', 'https://robohash.org/temporibusassumendaerror.png?size=300x300&set=set1', ''),
(187, 11, 'sit amet eleifend', 'massa id nisl venenatis', 'enim lorem ipsum dolor sit amet consectetuer adipiscing elit', '2015-11-21', '04:22:00', 'Proin at turpis a pede posuere nonummy. Integer non velit.', 'https://robohash.org/utsapienteet.jpg?size=300x300&set=set1', ''),
(188, 11, 'primis', 'dolor sit amet', 'porttitor id consequat in consequat ut nulla sed', '2015-11-03', '07:08:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.', 'https://robohash.org/harumestnostrum.jpg?size=300x300&set=set1', ''),
(189, 4, 'suspendisse ornare consequat', 'elementum ligula vehicula consequat', 'nisl aenean lectus pellentesque eget nunc donec', '2016-04-22', '07:57:00', 'Quisque id justo sit amet sapien dignissim vestibulum.', 'https://robohash.org/architectomolestiasrerum.bmp?size=300x300&set=set1', ''),
(190, 12, 'nunc vestibulum', 'turpis elementum ligula vehicula', 'volutpat erat quisque erat eros viverra eget congue eget', '2015-12-25', '11:26:00', 'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.', 'https://robohash.org/itaquevellabore.jpg?size=300x300&set=set1', ''),
(191, 4, 'pellentesque', 'in hac habitasse platea', 'non sodales sed tincidunt', '2016-04-30', '04:07:00', 'Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.', 'https://robohash.org/quiautex.png?size=300x300&set=set1', ''),
(192, 10, 'parturient', 'lorem ipsum dolor sit amet', 'eu nibh quisque id justo sit amet', '2016-10-13', '08:38:00', 'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.', 'https://robohash.org/laboresedaut.bmp?size=300x300&set=set1', ''),
(193, 4, 'potenti in eleifend', 'lacinia eget', 'curae mauris', '2016-04-11', '04:54:00', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.', 'https://robohash.org/voluptasautnon.jpg?size=300x300&set=set1', ''),
(194, 11, 'orci', 'vulputate elementum', 'posuere felis sed lacus morbi sem mauris laoreet', '2015-11-01', '08:44:00', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.', 'https://robohash.org/consequaturperspiciatisab.jpg?size=300x300&set=set1', ''),
(195, 12, 'sed vel', 'sit amet', 'donec odio', '2015-12-14', '04:37:00', 'Nulla facilisi. Cras non velit nec nisi vulputate nonummy.', 'https://robohash.org/providentaccusantiumquod.png?size=300x300&set=set1', ''),
(196, 10, 'posuere cubilia', 'dapibus dolor vel est donec', 'eget rutrum at lorem integer tincidunt ante', '2016-10-06', '05:32:00', 'Duis at velit eu est congue elementum. In hac habitasse platea dictumst.', 'https://robohash.org/evenietoditex.jpg?size=300x300&set=set1', ''),
(197, 2, 'porttitor lorem id', 'curabitur gravida nisi at', 'a suscipit nulla elit ac nulla sed vel enim', '2016-02-29', '05:50:00', 'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.', 'https://robohash.org/quiatemporevoluptatum.bmp?size=300x300&set=set1', ''),
(198, 12, 'elit ac', 'lacus morbi sem mauris', 'elit proin risus praesent lectus vestibulum quam', '2015-12-09', '10:13:00', 'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.', 'https://robohash.org/rerumsitofficiis.png?size=300x300&set=set1', ''),
(199, 1, 'purus', 'sapien a', 'integer pede justo', '2016-01-16', '10:44:00', 'Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.', 'https://robohash.org/excepturiineum.png?size=300x300&set=set1', ''),
(200, 8, 'in leo maecenas', 'vulputate luctus cum', 'adipiscing elit proin interdum mauris non ligula pellentesque', '2016-08-26', '08:55:00', 'Integer ac neque. Duis bibendum.', 'https://robohash.org/providentdoloresut.jpg?size=300x300&set=set1', ''),
(201, 9, 'natoque penatibus', 'iaculis diam erat', 'vulputate elementum nullam varius nulla facilisi cras non velit nec', '2016-09-22', '07:47:00', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla.', 'https://robohash.org/minimaautreiciendis.jpg?size=300x300&set=set1', ''),
(202, 4, 'elementum eu', 'nec condimentum neque sapien', 'et ultrices posuere cubilia curae mauris', '2016-04-17', '07:48:00', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.', 'https://robohash.org/molestiasrepudiandaeet.bmp?size=300x300&set=set1', ''),
(203, 11, 'venenatis non', 'non ligula pellentesque ultrices', 'curae duis faucibus accumsan', '2015-11-11', '07:36:00', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.', 'https://robohash.org/doloribussuscipitnisi.jpg?size=300x300&set=set1', ''),
(204, 10, 'blandit', 'at diam nam tristique tortor', 'id luctus nec molestie sed justo pellentesque', '2016-10-08', '11:43:00', 'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'https://robohash.org/estcorporisvel.bmp?size=300x300&set=set1', ''),
(205, 10, 'justo', 'quis orci eget orci', 'libero ut massa volutpat', '2016-10-14', '09:20:00', 'In quis justo. Maecenas rhoncus aliquam lacus.', 'https://robohash.org/quaeaperiamneque.bmp?size=300x300&set=set1', ''),
(206, 2, 'ac', 'pede morbi', 'vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet', '2016-02-13', '06:16:00', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', 'https://robohash.org/accusamusnostrumdicta.bmp?size=300x300&set=set1', ''),
(207, 8, 'cum sociis natoque', 'in porttitor pede justo', 'ac nulla', '2016-08-26', '05:49:00', 'In congue. Etiam justo.', 'https://robohash.org/eligendisuntquidem.bmp?size=300x300&set=set1', ''),
(208, 10, 'tempor convallis', 'a ipsum integer a nibh', 'ac leo pellentesque ultrices mattis odio donec vitae', '2016-10-02', '09:59:00', 'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', 'https://robohash.org/eosullamillo.jpg?size=300x300&set=set1', ''),
(209, 5, 'felis sed interdum', 'praesent lectus vestibulum quam', 'velit eu est congue elementum in hac habitasse platea dictumst', '2016-05-07', '01:05:00', 'Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.', 'https://robohash.org/culpaaliquideius.png?size=300x300&set=set1', ''),
(210, 7, 'luctus', 'in est', 'ullamcorper augue a', '2016-07-17', '07:03:00', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.', 'https://robohash.org/autsintdolorem.png?size=300x300&set=set1', ''),
(211, 7, 'nascetur ridiculus mus', 'ut erat id mauris', 'dolor morbi vel lectus in', '2016-07-21', '01:42:00', 'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.', 'https://robohash.org/praesentiumevenieteum.jpg?size=300x300&set=set1', ''),
(212, 9, 'eleifend donec', 'id mauris vulputate elementum nullam', 'mauris enim leo rhoncus sed vestibulum sit amet cursus', '2016-09-01', '07:10:00', 'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.', 'https://robohash.org/nihilplaceatvitae.jpg?size=300x300&set=set1', ''),
(213, 9, 'ultrices aliquet', 'neque libero convallis eget eleifend', 'amet sapien dignissim vestibulum vestibulum ante ipsum primis', '2016-09-06', '04:54:00', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 'https://robohash.org/undequivel.png?size=300x300&set=set1', ''),
(214, 7, 'pellentesque', 'dapibus at diam', 'tristique est et tempus semper est quam pharetra', '2016-07-25', '05:12:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.', 'https://robohash.org/voluptatemmaioresaut.bmp?size=300x300&set=set1', ''),
(215, 1, 'posuere cubilia curae', 'pharetra magna ac consequat', 'velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit', '2016-01-16', '08:25:00', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 'https://robohash.org/repellatsimiliquefugiat.bmp?size=300x300&set=set1', ''),
(216, 1, 'non quam nec', 'eget eros elementum', 'dolor vel est donec', '2016-01-26', '05:52:00', 'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla.', 'https://robohash.org/cumvoluptatemnesciunt.jpg?size=300x300&set=set1', ''),
(217, 5, 'arcu libero', 'congue vivamus metus', 'volutpat convallis morbi odio odio elementum eu interdum', '2016-05-13', '08:30:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/quidemcupiditatemollitia.bmp?size=300x300&set=set1', ''),
(218, 10, 'nisl', 'nibh fusce lacus', 'sit amet turpis', '2016-10-06', '02:37:00', 'Morbi quis tortor id nulla ultrices aliquet.', 'https://robohash.org/quononsed.png?size=300x300&set=set1', ''),
(219, 3, 'diam', 'dolor quis', 'turpis a', '2016-03-14', '03:38:00', 'Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.', 'https://robohash.org/totamhicquae.png?size=300x300&set=set1', ''),
(220, 3, 'nunc', 'purus eu magna vulputate', 'tincidunt lacus at velit vivamus vel nulla eget', '2016-03-12', '01:32:00', 'Ut tellus. Nulla ut erat id mauris vulputate elementum.', 'https://robohash.org/consequaturmaximesoluta.jpg?size=300x300&set=set1', ''),
(221, 2, 'nullam', 'molestie lorem quisque ut', 'nunc commodo placerat praesent blandit', '2016-02-02', '07:50:00', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.', 'https://robohash.org/asperioresdictareprehenderit.jpg?size=300x300&set=set1', ''),
(222, 1, 'lectus suspendisse', 'id', 'vivamus vel nulla eget', '2016-01-19', '11:26:00', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', 'https://robohash.org/assumendadoloribusipsa.png?size=300x300&set=set1', ''),
(223, 2, 'id', 'tristique in tempus sit amet', 'et magnis dis parturient', '2016-02-27', '04:50:00', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.', 'https://robohash.org/oditautdolore.bmp?size=300x300&set=set1', ''),
(224, 8, 'ligula suspendisse ornare', 'scelerisque quam turpis adipiscing', 'cras in purus', '2016-08-24', '10:54:00', 'Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.', 'https://robohash.org/necessitatibusconsequunturmaiores.bmp?size=300x300&set=set1', ''),
(225, 3, 'in lectus pellentesque', 'cubilia', 'integer a nibh in quis justo maecenas rhoncus', '2016-03-21', '05:19:00', 'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.', 'https://robohash.org/atlaboriosamaut.bmp?size=300x300&set=set1', ''),
(226, 1, 'sit', 'non quam', 'sapien ut nunc vestibulum ante', '2016-01-21', '02:51:00', 'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 'https://robohash.org/aperiamdistinctioiste.jpg?size=300x300&set=set1', ''),
(227, 4, 'nibh in lectus', 'aliquam convallis nunc proin', 'nec nisi vulputate nonummy maecenas tincidunt lacus', '2016-04-13', '01:58:00', 'Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.', 'https://robohash.org/nonquaeratquasi.png?size=300x300&set=set1', ''),
(228, 6, 'at', 'a ipsum', 'eget nunc', '2016-06-24', '03:18:00', 'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.', 'https://robohash.org/doloresinventoresequi.jpg?size=300x300&set=set1', ''),
(229, 7, 'at dolor quis', 'primis in faucibus orci luctus', 'et tempus', '2016-07-14', '02:20:00', 'Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.', 'https://robohash.org/minuspraesentiumillum.bmp?size=300x300&set=set1', ''),
(230, 4, 'accumsan tellus nisi', 'lobortis convallis tortor', 'vitae consectetuer eget rutrum at lorem integer', '2016-04-07', '10:26:00', 'Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/nemoexercitationemex.png?size=300x300&set=set1', ''),
(231, 5, 'curabitur', 'nunc purus', 'eget massa tempor convallis nulla neque libero convallis eget', '2016-05-04', '04:13:00', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 'https://robohash.org/officiisipsadistinctio.bmp?size=300x300&set=set1', ''),
(232, 1, 'pede justo lacinia', 'malesuada in imperdiet', 'suscipit nulla elit ac nulla sed vel', '2016-01-20', '08:20:00', 'Proin interdum mauris non ligula pellentesque ultrices.', 'https://robohash.org/dolorlaboreut.png?size=300x300&set=set1', ''),
(233, 11, 'imperdiet', 'donec ut mauris eget massa', 'iaculis diam erat fermentum justo', '2015-11-03', '01:10:00', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.', 'https://robohash.org/ducimusetcupiditate.png?size=300x300&set=set1', ''),
(234, 4, 'tempus', 'faucibus orci', 'proin leo odio porttitor id consequat in consequat ut nulla', '2016-04-15', '06:42:00', 'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', 'https://robohash.org/nonnobisneque.jpg?size=300x300&set=set1', ''),
(235, 1, 'ultrices aliquet maecenas', 'nulla tempus vivamus in felis', 'nisi at nibh in hac habitasse', '2016-01-31', '09:25:00', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 'https://robohash.org/remadodit.jpg?size=300x300&set=set1', ''),
(236, 11, 'ultrices', 'ipsum', 'nonummy maecenas tincidunt lacus at velit vivamus', '2015-11-05', '03:41:00', 'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.', 'https://robohash.org/quiaeteum.png?size=300x300&set=set1', ''),
(237, 9, 'id mauris', 'congue diam', 'enim in tempor turpis nec euismod', '2016-09-11', '12:49:00', 'Nulla ac enim.', 'https://robohash.org/quissolutavoluptates.png?size=300x300&set=set1', ''),
(238, 1, 'ut', 'ligula suspendisse ornare', 'ligula pellentesque ultrices phasellus id', '2016-01-24', '03:29:00', 'Suspendisse potenti.', 'https://robohash.org/utvelvero.png?size=300x300&set=set1', ''),
(239, 8, 'aliquet massa id', 'curae nulla', 'nisl duis ac nibh fusce lacus purus aliquet at', '2016-08-25', '02:06:00', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 'https://robohash.org/inrecusandaeut.bmp?size=300x300&set=set1', ''),
(240, 6, 'turpis', 'fusce', 'id sapien in sapien iaculis', '2016-06-08', '06:55:00', 'Sed accumsan felis. Ut at dolor quis odio consequat varius.', 'https://robohash.org/etvoluptatemautem.jpg?size=300x300&set=set1', ''),
(241, 10, 'non mattis pulvinar', 'dapibus augue vel accumsan tellus', 'facilisi cras non velit nec nisi vulputate', '2015-10-27', '03:36:00', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'https://robohash.org/fugitquasisit.png?size=300x300&set=set1', ''),
(242, 9, 'ac', 'nulla justo', 'lobortis ligula sit amet eleifend', '2016-09-21', '11:10:00', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 'https://robohash.org/sequiautest.bmp?size=300x300&set=set1', ''),
(243, 10, 'magna', 'nisi vulputate nonummy maecenas', 'vel augue vestibulum ante ipsum primis in faucibus', '2015-10-22', '11:23:00', 'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 'https://robohash.org/eumdoloreset.png?size=300x300&set=set1', ''),
(244, 5, 'fusce', 'donec', 'tempus sit amet sem', '2016-05-26', '06:55:00', 'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', 'https://robohash.org/dictaautnatus.bmp?size=300x300&set=set1', ''),
(245, 6, 'morbi', 'eros elementum pellentesque', 'rhoncus', '2016-06-07', '12:17:00', 'Suspendisse accumsan tortor quis turpis. Sed ante.', 'https://robohash.org/evenietfacilisqui.bmp?size=300x300&set=set1', ''),
(246, 4, 'vel nisl duis', 'nisl aenean lectus pellentesque', 'at feugiat non pretium quis lectus suspendisse', '2016-04-28', '01:21:00', 'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst.', 'https://robohash.org/necessitatibusiurenulla.bmp?size=300x300&set=set1', ''),
(247, 5, 'massa volutpat', 'nascetur ridiculus mus', 'orci pede venenatis non sodales sed tincidunt', '2016-05-22', '09:38:00', 'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'https://robohash.org/quiquinihil.png?size=300x300&set=set1', ''),
(248, 4, 'at', 'in quam', 'nulla suspendisse potenti cras in purus eu', '2016-04-06', '05:24:00', 'Aliquam sit amet diam in magna bibendum imperdiet.', 'https://robohash.org/nondolorererum.jpg?size=300x300&set=set1', ''),
(249, 4, 'eu massa donec', 'lorem integer tincidunt ante', 'natoque penatibus et magnis dis parturient montes nascetur ridiculus', '2016-04-27', '07:52:00', 'Duis consequat dui nec nisi volutpat eleifend.', 'https://robohash.org/earemvoluptatem.jpg?size=300x300&set=set1', ''),
(250, 10, 'et', 'id ligula suspendisse ornare', 'pharetra', '2015-10-26', '07:31:00', 'Maecenas pulvinar lobortis est. Phasellus sit amet erat.', 'https://robohash.org/dolorenimea.jpg?size=300x300&set=set1', ''),
(251, 3, 'eget', 'erat eros viverra eget congue', 'nisl duis ac nibh fusce lacus purus aliquet', '2016-03-31', '02:21:00', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.', 'https://robohash.org/etexcepturiquia.jpg?size=300x300&set=set1', ''),
(252, 10, 'ultrices erat', 'sapien cum sociis natoque penatibus', 'proin interdum mauris non ligula pellentesque', '2016-10-20', '12:08:00', 'Morbi ut odio.', 'https://robohash.org/illofaceredolorum.png?size=300x300&set=set1', ''),
(253, 11, 'consequat ut nulla', 'nullam porttitor', 'orci pede venenatis non sodales', '2015-11-07', '11:38:00', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'https://robohash.org/voluptatibusenimsit.jpg?size=300x300&set=set1', ''),
(254, 12, 'non', 'aliquet pulvinar sed nisl', 'morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl', '2015-12-14', '09:58:00', 'Praesent blandit. Nam nulla.', 'https://robohash.org/doloreseaqueut.bmp?size=300x300&set=set1', 'http://asdasdas2');

-- --------------------------------------------------------

--
-- Table structure for table `gifts`
--

CREATE TABLE `gifts` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `sub_title` text NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `thumbnail` text NOT NULL,
  `weblink` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gifts`
--

INSERT INTO `gifts` (`id`, `title`, `sub_title`, `description`, `start_date`, `end_date`, `thumbnail`, `weblink`) VALUES
(1, 'Free tickets for Lorem Ipsum', 'lorem ipsum dolor', 'This is a lorem ipsum dolor description', '2016-09-01', '2016-09-03', 'http://betaprojex.com/loveradio-app/uploads/gift_thumbnail_1.jpg', ''),
(2, 'Free admission for Green day concert', 'grumpy wizard brews', 'This is another description the quick brown fox', '2016-10-26', '2016-10-29', 'http://betaprojex.com/loveradio-app/uploads/gift_thumbnail_2.jpg', ''),
(3, 'justo eu massa donec dapibus duis at', 'ac enim in tempor turpis nec euismod scelerisque quam', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2016-05-06', '2016-02-08', 'https://robohash.org/aspernatursolutaillo.jpg?size=250x250&set=set1', ''),
(4, 'nulla justo aliquam quis turpis eget elit', 'sed justo pellentesque viverra pede ac diam', 'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.', '2015-12-31', '2015-12-16', 'https://robohash.org/fugaetatque.jpg?size=250x250&set=set1', ''),
(5, 'duis faucibus accumsan odio curabitur convallis', 'sapien placerat ante nulla justo aliquam', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2015-12-04', '2016-02-03', 'https://robohash.org/nobisaccusantiumnisi.jpg?size=250x250&set=set1', ''),
(6, 'pretium iaculis diam erat fermentum justo nec', 'nisi vulputate nonummy maecenas', 'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla.', '2015-12-01', '2016-02-18', 'https://robohash.org/enimtemporebeatae.jpg?size=250x250&set=set1', ''),
(7, 'quis lectus suspendisse potenti in eleifend quam a odio in', 'justo eu massa donec dapibus duis', 'Ut tellus.', '2016-05-09', '2015-12-20', 'https://robohash.org/autfaciliscum.jpg?size=250x250&set=set1', ''),
(8, 'sed justo pellentesque viverra pede', 'et magnis dis parturient montes nascetur', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl.', '2016-01-10', '2016-08-21', 'https://robohash.org/natusquisaspernatur.jpg?size=250x250&set=set1', ''),
(9, 'morbi ut odio cras mi pede malesuada in imperdiet', 'adipiscing elit proin interdum mauris non ligula pellentesque', 'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.', '2016-07-01', '2016-03-20', 'https://robohash.org/cumquelaudantiumdelectus.jpg?size=250x250&set=set1', ''),
(10, 'nulla dapibus dolor vel est donec', 'mauris ullamcorper purus', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.', '2015-11-22', '2016-09-13', 'https://robohash.org/ipsaharumvoluptas.jpg?size=250x250&set=set1', ''),
(11, 'quis orci eget orci vehicula', 'fusce', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.', '2015-11-27', '2016-05-17', 'https://robohash.org/totamquomagni.jpg?size=250x250&set=set1', ''),
(12, 'vel nulla eget eros elementum pellentesque quisque', 'at lorem integer', 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.', '2016-09-11', '2015-12-20', 'https://robohash.org/nemocupiditatequibusdam.jpg?size=250x250&set=set1', ''),
(13, 'quam pede lobortis ligula sit amet eleifend pede', 'in', 'Duis aliquam convallis nunc.', '2016-01-22', '2016-03-05', 'https://robohash.org/utfacilisconsectetur.jpg?size=250x250&set=set1', ''),
(14, 'pharetra magna vestibulum aliquet ultrices erat', 'nibh in quis justo maecenas rhoncus aliquam', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2016-08-27', '2016-02-19', 'https://robohash.org/repudiandaevoluptatumnobis.jpg?size=250x250&set=set1', ''),
(15, 'quam pede lobortis ligula sit amet eleifend pede', 'enim blandit', 'Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.', '2016-09-30', '2016-08-13', 'https://robohash.org/utillosimilique.jpg?size=250x250&set=set1', ''),
(16, 'neque', 'commodo placerat praesent blandit nam nulla integer pede', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2016-08-19', '2016-02-19', 'https://robohash.org/laboreetaut.jpg?size=250x250&set=set1', ''),
(17, 'lacus purus aliquet', 'fusce posuere felis sed lacus morbi sem mauris laoreet', 'Vestibulum sed magna at nunc commodo placerat.', '2015-12-01', '2016-08-21', 'https://robohash.org/doloremquedelenitinatus.jpg?size=250x250&set=set1', ''),
(18, 'suspendisse accumsan tortor', 'ut rhoncus aliquet pulvinar sed nisl', 'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.', '2016-04-15', '2015-12-13', 'https://robohash.org/voluptatemaccusantiumrerum.jpg?size=250x250&set=set1', ''),
(19, 'ante vel ipsum', 'pede justo lacinia eget tincidunt eget tempus vel', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.', '2016-04-24', '2016-09-24', 'https://robohash.org/doloremvelitut.jpg?size=250x250&set=set1', ''),
(20, 'purus aliquet at feugiat', 'at lorem integer tincidunt', 'In quis justo. Maecenas rhoncus aliquam lacus.', '2016-05-04', '2016-09-28', 'https://robohash.org/blanditiismolestiaeeius.jpg?size=250x250&set=set1', ''),
(21, 'justo eu massa donec dapibus duis', 'nulla facilisi cras non velit nec nisi', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.', '2015-12-31', '2015-11-20', 'https://robohash.org/eaqueconsequunturrem.jpg?size=250x250&set=set1', ''),
(22, 'praesent blandit', 'fusce congue diam id ornare imperdiet sapien urna', 'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus.', '2016-02-12', '2015-11-24', 'https://robohash.org/perspiciatislaborevoluptates.jpg?size=250x250&set=set1', ''),
(23, 'sollicitudin ut suscipit a feugiat', 'ipsum praesent blandit lacinia erat vestibulum sed magna', 'Aliquam erat volutpat. In congue. Etiam justo.', '2016-07-20', '2015-10-28', 'https://robohash.org/quisconsequaturalias.jpg?size=250x250&set=set1', ''),
(24, 'in ante vestibulum ante', 'amet eros suspendisse accumsan tortor quis turpis', 'Nunc rhoncus dui vel sem.', '2016-09-22', '2016-07-15', 'https://robohash.org/facilisblanditiisexcepturi.jpg?size=250x250&set=set1', ''),
(25, 'at velit eu est congue elementum', 'mauris ullamcorper purus sit amet nulla quisque arcu libero', 'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2016-10-16', '2016-04-03', 'https://robohash.org/veniamplaceatharum.jpg?size=250x250&set=set1', ''),
(26, 'orci pede venenatis non', 'imperdiet nullam orci pede venenatis non', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.', '2016-05-31', '2015-11-13', 'https://robohash.org/aspernaturnamest.jpg?size=250x250&set=set1', ''),
(27, 'sodales scelerisque mauris sit amet eros', 'posuere cubilia curae nulla dapibus dolor vel', 'Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2016-02-13', '2016-03-10', 'https://robohash.org/velitsintplaceat.jpg?size=250x250&set=set1', ''),
(28, 'justo sit amet sapien dignissim', 'aliquet ultrices', 'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2016-08-12', '2016-03-26', 'https://robohash.org/voluptatibusvelmagnam.jpg?size=250x250&set=set1', ''),
(29, 'at', 'id', 'Aenean fermentum.', '2016-10-05', '2016-04-10', 'https://robohash.org/sitsaepeeum.jpg?size=250x250&set=set1', ''),
(30, 'faucibus cursus urna ut tellus nulla', 'amet eleifend pede libero', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.', '2016-10-12', '2016-04-19', 'https://robohash.org/autmollitiacorrupti.jpg?size=250x250&set=set1', ''),
(31, 'proin at turpis a pede posuere nonummy integer', 'et ultrices posuere', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2016-05-24', '2015-11-22', 'https://robohash.org/voluptatesetanimi.jpg?size=250x250&set=set1', ''),
(32, 'luctus nec molestie sed', 'leo maecenas pulvinar lobortis est phasellus sit amet erat nulla', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.', '2016-03-25', '2015-11-30', 'https://robohash.org/magnipariatura.jpg?size=250x250&set=set1', ''),
(33, 'vel sem sed', 'libero non mattis', 'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.', '2016-01-22', '2016-08-17', 'https://robohash.org/laborevoluptatibushic.jpg?size=250x250&set=set1', ''),
(34, 'consequat in consequat ut nulla sed accumsan felis ut at', 'nam dui proin leo odio porttitor id consequat in consequat', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.', '2016-09-06', '2016-06-27', 'https://robohash.org/reiciendiseosvitae.jpg?size=250x250&set=set1', ''),
(35, 'quis orci nullam molestie nibh in lectus pellentesque at nulla', 'cubilia curae', 'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2016-08-06', '2015-10-21', 'https://robohash.org/hicatqueeum.jpg?size=250x250&set=set1', ''),
(36, 'orci luctus et', 'imperdiet sapien urna pretium nisl ut volutpat sapien arcu', 'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.', '2016-05-16', '2016-09-09', 'https://robohash.org/aperiamnisiillum.jpg?size=250x250&set=set1', ''),
(37, 'erat vestibulum sed magna at nunc commodo placerat praesent blandit', 'odio condimentum id luctus nec molestie sed', 'Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.', '2016-09-11', '2016-07-30', 'https://robohash.org/utsuntsuscipit.jpg?size=250x250&set=set1', ''),
(38, 'aliquam non mauris morbi non lectus aliquam', 'nulla nisl nunc nisl duis bibendum felis sed interdum venenatis', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.', '2015-11-21', '2016-05-19', 'https://robohash.org/estadipiscieaque.jpg?size=250x250&set=set1', ''),
(39, 'tortor duis mattis egestas metus aenean fermentum donec', 'elit sodales scelerisque mauris sit amet eros suspendisse', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2016-08-12', '2016-05-24', 'https://robohash.org/explicaboametveritatis.jpg?size=250x250&set=set1', ''),
(40, 'nulla ultrices', 'sapien sapien non mi integer ac', 'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.', '2015-12-14', '2016-01-29', 'https://robohash.org/suntveniamvelit.jpg?size=250x250&set=set1', ''),
(41, 'libero ut massa volutpat convallis morbi odio odio elementum eu', 'leo maecenas pulvinar lobortis est phasellus sit amet', 'Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.', '2016-04-05', '2016-08-02', 'https://robohash.org/perspiciatisitaquedicta.jpg?size=250x250&set=set1', ''),
(42, 'imperdiet nullam', 'vitae mattis nibh ligula nec sem duis aliquam convallis', 'In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.', '2016-01-07', '2016-05-27', 'https://robohash.org/veritatisutquod.jpg?size=250x250&set=set1', ''),
(43, 'nunc purus phasellus', 'posuere cubilia curae nulla dapibus', 'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2016-09-22', '2016-03-23', 'https://robohash.org/sitatquedolorum.jpg?size=250x250&set=set1', ''),
(44, 'nullam porttitor lacus at turpis', 'diam neque vestibulum eget vulputate ut ultrices vel', 'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.', '2016-04-30', '2016-06-28', 'https://robohash.org/atcommodiaut.jpg?size=250x250&set=set1', ''),
(45, 'pulvinar nulla pede ullamcorper augue a suscipit nulla elit', 'habitasse platea dictumst', 'In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.', '2016-04-13', '2016-04-29', 'https://robohash.org/voluptasvelitcum.jpg?size=250x250&set=set1', ''),
(46, 'ante vivamus tortor duis mattis egestas metus aenean fermentum donec', 'maecenas ut massa quis augue luctus tincidunt nulla mollis', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.', '2016-01-10', '2015-11-05', 'https://robohash.org/dolorblanditiissint.jpg?size=250x250&set=set1', ''),
(47, 'proin eu mi nulla ac enim in tempor turpis', 'rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.', '2016-07-16', '2016-06-01', 'https://robohash.org/natusidut.jpg?size=250x250&set=set1', ''),
(48, 'semper sapien a libero nam dui proin leo odio porttitor', 'accumsan tortor quis', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.', '2016-09-23', '2016-02-10', 'https://robohash.org/etporrovoluptatum.jpg?size=250x250&set=set1', ''),
(49, 'non mi integer ac', 'odio justo', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', '2016-10-14', '2016-08-03', 'https://robohash.org/quamquineque.jpg?size=250x250&set=set1', ''),
(50, 'diam cras pellentesque volutpat dui maecenas', 'aenean lectus', 'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2016-10-12', '2016-07-08', 'https://robohash.org/doloremitaqueet.jpg?size=250x250&set=set1', 'http://asdasdasd');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `thumbnail` text NOT NULL,
  `welcome_message` text NOT NULL,
  `greeting` text NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `thumbnail`, `welcome_message`, `greeting`, `title`) VALUES
(1, 'http://betaprojex.com/loveradio-app/uploads/home_thumbnail_1.jpg', 'The quick brown fox jumps over the lazy dog', 'Happy halloweenz', 'Some title');

-- --------------------------------------------------------

--
-- Table structure for table `livestream_radio`
--

CREATE TABLE `livestream_radio` (
  `id` int(11) NOT NULL,
  `link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `livestream_radio`
--

INSERT INTO `livestream_radio` (`id`, `link`) VALUES
(1, 'http://hyades.shoutca.st:8146/');

-- --------------------------------------------------------

--
-- Table structure for table `livestream_video`
--

CREATE TABLE `livestream_video` (
  `id` int(11) NOT NULL,
  `link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `livestream_video`
--

INSERT INTO `livestream_video` (`id`, `link`) VALUES
(1, 'http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8');

-- --------------------------------------------------------

--
-- Table structure for table `months`
--

CREATE TABLE `months` (
  `id` int(11) NOT NULL,
  `month_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `months`
--

INSERT INTO `months` (`id`, `month_name`) VALUES
(1, 'january'),
(2, 'february'),
(3, 'march'),
(4, 'april'),
(5, 'may'),
(6, 'june'),
(7, 'july'),
(8, 'august'),
(9, 'september'),
(10, 'october'),
(11, 'november'),
(12, 'december');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy`
--

CREATE TABLE `privacy_policy` (
  `id` int(11) NOT NULL,
  `pp_desc` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacy_policy`
--

INSERT INTO `privacy_policy` (`id`, `pp_desc`) VALUES
(1, 'Sample Privacy Policy\n\nThis privacy policy discloses the privacy practices for (website address). This privacy policy applies solely to information collected by this web site. It will notify you of the following:\n\nWhat personally identifiable information is collected from you through the web site, how it is used and with whom it may be shared.\nWhat choices are available to you regarding the use of your data.\nThe security procedures in place to protect the misuse of your information.\nHow you can correct any inaccuracies in the information.\nInformation Collection, Use, and Sharing \nWe are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.\n\nWe will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order.\n\nUnless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.\n\nYour Access to and Control Over Information \nYou may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:\n\n   â€¢ See what data we have about you, if any.\n\n   â€¢ Change/correct any data we have about you.\n\n   â€¢ Have us delete any data we have about you.\n\n   â€¢ Express any concern you have about our use of your data.\n\nSecurity \nWe take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.\n\nWherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a closed lock icon at the bottom of your web browser, or looking for "https" at the beginning of the address of the web page.\n\nWhile we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.\n\nUpdates\n\nOur Privacy Policy may change from time to time and all updates will be posted on this page.\n\nIf you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at XXX YYY-ZZZZ or via email.');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `terms` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `terms`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dapibus pulvinar lacus ac imperdiet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer leo lacus, tristique nec ex ut, tincidunt efficitur dui. Nam pulvinar ligula in rutrum finibus. Donec auctor est non est faucibus iaculis. Praesent eget tristique tortor. Praesent diam ligula, ultricies ut volutpat sed, laoreet at eros. Vivamus quis massa pulvinar, consequat orci sit amet, tristique ante. Sed fermentum libero non tristique posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi id fringilla magna. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum fringilla mi vitae lacus pellentesque, sodales consequat mauris sagittis. Nulla sed aliquet est. Quisque nec semper ligula. Phasellus aliquam turpis nec lobortis placerat.\n\nPellentesque nec vulputate eros. Suspendisse potenti. Quisque sit amet dignissim tortor. Sed condimentum sapien dolor, non tincidunt magna mattis et. Nullam suscipit turpis a tellus efficitur, vel consequat tortor condimentum. Morbi ullamcorper nec dolor nec accumsan. Ut at pharetra tellus. Suspendisse euismod, risus vitae porttitor semper, turpis orci aliquet enim, ut commodo diam risus eu justo. Quisque in sapien pharetra, tempus erat nec, dignissim ante.\n\nEtiam in eleifend nisi, scelerisque gravida ex. Pellentesque accumsan nunc quis vehicula tincidunt. Proin et hendrerit quam, quis facilisis mi. Praesent ullamcorper neque id arcu sagittis egestas. Sed id purus sem. Nullam turpis dui, malesuada nec quam vitae, dignissim ultrices velit. Sed et arcu enim. Vivamus ut sem vel urna egestas aliquam.\n\nSuspendisse faucibus, turpis non euismod luctus, erat ex facilisis neque, non rhoncus ligula libero eu justo. Vivamus pulvinar luctus justo eget commodo. Integer pharetra faucibus suscipit. Aenean vel posuere eros. Mauris pellentesque lorem elit. Cras mollis dolor mauris, eu ultricies ante tincidunt nec. Quisque sit amet diam mi. Vivamus suscipit ligula at leo ultrices, in auctor metus dapibus.\n\nProin convallis dignissim purus ac fermentum. Donec porta facilisis turpis ac dignissim. Curabitur nec ultricies tellus. Sed cursus dui nisi, ut pharetra felis ullamcorper quis. Aenean vitae ligula sit amet turpis pulvinar porttitor. Ut volutpat ex sed nisi sagittis sagittis. Integer ante elit, consequat non nulla nec, lobortis congue ipsum. Etiam auctor ipsum in tellus accumsan, non elementum diam aliquam. Nam fermentum eros quis dapibus pellentesque. Suspendisse volutpat, ligula non egestas tincidunt, orci velit tincidunt augue, nec porttitor nunc est vel magna. Fusce blandit lobortis sagittis. Maecenas sed laoreet libero. Pellentesque diam ante, faucibus eget ex et, eleifend iaculis purus. Nam varius condimentum ex, nec mattis purus. Pellentesque at risus maximus, tristique ex at, ultricies nulla.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(225) NOT NULL,
  `mname` varchar(225) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_num` varchar(225) NOT NULL,
  `password` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `sex` varchar(20) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `interests` text NOT NULL,
  `avatar` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `mname`, `email`, `mobile_num`, `password`, `birthdate`, `sex`, `country`, `city`, `interests`, `avatar`, `status`) VALUES
(1, 'Jane Doe', '', '', 'doe@john.com', '', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '1994-12-11', 'Female', 'Philippines', 'Paranaque', 'Fashion,Games', 'http://betaprojex.com/loveradio-app/uploads/avatar_1.jpg', 1),
(3, 'wendy ang', '', '', 'wsang@myoptimind.com', '', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '1980-08-25', 'Female', 'Philippines', 'Manila', 'Food,Fashion,Art & Music,Travel,Lifestyle,Photography', 'http://betaprojex.com/loveradio-app/uploads/avatar_3.jpg', 1),
(4, 'Lawrence', '', '', 'jlmarcelo@myoptimind.com', '', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '1999-12-23', 'Male', 'Anguilla', 'George Hill', 'Travel', 'http://betaprojex.com/loveradio-app/uploads/avatar_4.jpg', 1),
(5, 'The Jacob Chan', '', '', 'jacobchan_19@yahoo.com', '', 'd8a207792e297e5602b67ec68ba412abfc6a7dbc', '1992-04-05', 'Male', 'Philippines', 'Marikina City', 'Sport,Travel,Photography,Food,Fashion,Games,Movies', 'http://betaprojex.com/loveradio-app/uploads/avatar_5.jpg', 1),
(12, 'Lorenzo', 'Dante', '', 'lsalamante@myoptimind.com', '', '345d37fe046538829504c0ed4a806d17eb4d181c', '0000-00-00', 'Male', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vods`
--

CREATE TABLE `vods` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `uploader` text NOT NULL,
  `link` text NOT NULL,
  `thumbnail` text NOT NULL,
  `duration` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vods`
--

INSERT INTO `vods` (`id`, `category_id`, `title`, `uploader`, `link`, `thumbnail`, `duration`) VALUES
(1, 1, 'Doraemon', 'staff', 'http://betaprojex.com/loveradio-app/uploads/vod_1.mp4', 'http://betaprojex.com/loveradio-app/uploads/vod_thumbnail_1.jpg', '0:03'),
(2, 5, 'One punch man!!', 'staff', 'http://example.com/videos/123456789/player?!!!asdasdasdasd', 'http://localhost/mbc-love-radio/uploads/vod_thumbnail_2.jpg', '00:14:51');

-- --------------------------------------------------------

--
-- Table structure for table `vod_category`
--

CREATE TABLE `vod_category` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `thumbnail` text NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vod_category`
--

INSERT INTO `vod_category` (`id`, `title`, `thumbnail`, `status`) VALUES
(1, 'Clubzxccccc', 'http://localhost/mbc-love-radio/uploads/vod_category_1.jpg', 1),
(2, 'EDM', 'http://betaprojex.com/loveradio-app/uploads/vod_category_2.jpg', 1),
(3, 'Disco', 'http://betaprojex.com/loveradio-app/uploads/vod_category_3.jpg', 1),
(4, 'Sentimental', 'http://localhost/mbc-love-radio/uploads/vod_category_4.jpg', 1),
(5, 'Hype', 'http://localhost/mbc-love-radio/uploads/vod_category_5.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gifts`
--
ALTER TABLE `gifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livestream_radio`
--
ALTER TABLE `livestream_radio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livestream_video`
--
ALTER TABLE `livestream_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `months`
--
ALTER TABLE `months`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `vods`
--
ALTER TABLE `vods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vod_category`
--
ALTER TABLE `vod_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=255;
--
-- AUTO_INCREMENT for table `gifts`
--
ALTER TABLE `gifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `livestream_radio`
--
ALTER TABLE `livestream_radio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `livestream_video`
--
ALTER TABLE `livestream_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `months`
--
ALTER TABLE `months`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `vods`
--
ALTER TABLE `vods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vod_category`
--
ALTER TABLE `vod_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
