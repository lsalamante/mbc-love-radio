<!DOCTYPE html>
<html lang="en">

<!-- Define Charset -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Responsive Meta Tag -->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">

<title>Registration successful!</title>

<!-- Responsive and Valid Styles -->



</head>
<center>
<body style="margin: 30px 0; background: #F1F2F7">

<table border="0" cellpadding="0" cellspacing="0" style="max-width: 560px; width: 100%;">
    <tbody>
        <tr>
            <td style="padding: 10px; background: #152632">
                <img src="../img/email-img/loveicon.png" style="max-width: 60px;width: 100%;">
            </td>
        </tr>
        <tr>
            <td style="padding: 20px; background: white;">
                <table width="100%">
                    <tr>
                        <td style="width: 70%; padding: 15px; box-sizing: border-box;">
                            <h2 style="font: 20px/26px  Helvetica, Arial, sans-serif; color: black; font-weight: bold; margin-bottom: 15px;">Registration successful!</h2>
                            <p style="font: 14px/20px  Helvetica, Arial, sans-serif; margin-bottom: 10px;">You can now log in to the Love Radio TV app using your email address and password.</p>
                            <p style="font: 14px/20px  Helvetica, Arial, sans-serif; margin-bottom: 10px;"><a href="https://zjz4y.app.goo.gl/jTpt" style="background-color: #FF9800; font-size: 14px; padding: 10px 15px; color: #fff; text-decoration: none">Open App</a></p>
                        </td>
                        <td style="padding: 15px; box-sizing: border-box;"><img  style="display: block; display:none" src="../img/email-img/success.png" alt="image4" height="auto" width="100%"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px; background: #4CAF50; font: 12px/18px  Helvetica, Arial, sans-serif; color: white; text-align:center"><?php echo $SITE_NAME ?> © Copyright 2017 . All Rights Reserved.</td>
        </tr>
    </tbody>
</table>



</body>
</html>
