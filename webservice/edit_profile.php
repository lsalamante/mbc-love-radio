<?php
#INCLUDES
include('../jp_library/jp_lib.php');


if (
    isset($_POST['id']) &&
    isset($_POST['fname']) &&
    isset($_POST['lname']) &&
    isset($_POST['birthdate']) &&
    isset($_POST['sex']) &&
    isset($_POST['country']) &&
    isset($_POST['city']) &&
    isset($_POST['mobile_num']) &&
    isset($_POST['interests'])
) {
    #Uncomment when reactivating string explosion
//    $interests = "";
//    foreach($_POST['interests'] as $row){
//       $interests .= $row . ",";
//    }
    #trim trailing comma
//    $interests = rtrim($interests, ',');

    $params['table'] = 'users';
    $params['where'] = "id = '" . $_POST['id'] . "'";
//    $params['debug'] = 1;
    #OVERWRITTEN POST VARIABLES
//    $_POST['interests'] = $interests;

    $params['data'] = array(
        "fname" => $_POST['fname'],
        "lname" => $_POST['lname'],
        "birthdate" => $_POST['birthdate'],
        "sex" => $_POST['sex'],
        "country" => $_POST['country'],
        "city" => $_POST['city'],
        "mobile_num" => $_POST['mobile_num'],
        "interests" => $_POST['interests']
    );

    #UPDATE HAPPENS HERE
    $result = jp_update($params);
    if ($result) #IF QUERY IS SUCCESSFUL
    {
        $i['status'] = true;
        $i['message'] = "Profile updated successfully";
//        $i['debug'] = $result;
    } else {
        $i['status'] = false;
        $i['message'] = "Failed to update profile";
    }
    echo json_encode($i);
} else {
    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);

}
