<?php
include('../jp_library/jp_lib.php');

if(
    isset($_POST['month_id']) &&
    isset($_POST['year']) &&
    isset($_POST['counter']))
{

    $counter = $_POST['counter'];

    if ($counter == 1) {
        $limit = "LIMIT 10";
    } else {
        $counter = ($counter - 1) * 10;
        $limit = "LIMIT 10 OFFSET $counter";
    }

    $params['table'] = "events";
    $params['where'] = "month_id = '" . $_POST['month_id'] . "' AND date LIKE '" . $_POST['year'] . "%'";
    $params['filters'] = "ORDER BY date ASC $limit";

    $result = jp_get($params);

    if ($result->num_rows > 0) {

        $i['status'] = true;

        $x = 0;
        foreach ($result as $row) {

            $i['event'][$x]["id"] = $row["id"];
            $i['event'][$x]["title"] = $row["title"];
            $i['event'][$x]["sub_title"] = $row["sub_title"];
            $i['event'][$x]["location"] = $row["location"];
            $i['event'][$x]["date"] = $row["date"];
            $i['event'][$x]["time"] = $row["time"];
            $i['event'][$x]["description"] = $row["description"];
            $i['event'][$x]["thumbnail"] = $row["thumbnail"];
            $i['event'][$x]["weblink"] = $row["weblink"];

            $x = $x + 1;
        }
    } else {
        $i['status'] = true;
        $i['event'] = [];
    }

    echo json_encode($i);
}else{

    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);
}
