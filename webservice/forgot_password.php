<?php
#INCLUDES
include('../jp_library/jp_lib.php');
require('../PHPMailer-master/PHPMailerAutoload.php');

#random string generator
function generateRandomString($length = 8)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

#TODO possible to be exploited random resetting of password?
if (isset($_POST['email'])) {

    $params['table'] = "users";
    $params['where'] = "email = '" . $_POST['email'] . "'";
    $query = jp_get($params);
    $result = mysqli_fetch_assoc($query);

    #generate new password
    #replace old password with a new one
    unset($params);
    $params['table'] = 'users';
    $params['where'] = "id = '" . $result['id'] . "'";
    $new_password = generateRandomString();
    $hashed_new_password = sha1($new_password);

    $params['data'] = array(
        "password" => $hashed_new_password,
    );

    $result2 = jp_update($params);
    if ($result2) {
        $password_updated = 1;
    } else {
        $password_updated = 0;
    }

    #TODO change mail configurations SMTP
    if ($query->num_rows > 0) {
        $email_to = $_POST['email'];
        $email_toName = $result['fname'];

        $email_from = 'no-reply@loveradio.com.ph';
        $subject = "Password Reset";
        $message = "
                <html>
                <body>
                    <p>Hello $email_toName!</p>
                    <p>This is your new password:<b> $new_password </b></p>
                    <p>Please don't forget to change your password in the app.<br>Thank you for patronizing our services.</p>
                </body>
                </html>";

        $message = "<table class=\"container-middle\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"560\" bgcolor=\"F1F2F7\">
            <tr >
                <td>
                    <table class=\"mainContent\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"528\">
                        <tbody><tr><td height=\"20\"></td></tr>
                        <tr>
                            <td>

                                <table class=\"section-item\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"360\">
                                    <tbody><tr>
                                        <td  style=\"color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;\">

                                            Kabisyo, $email_toName!

                                        </td>
                                    </tr>
                                    <tr><td height=\"15\"></td></tr>
                                    <tr>
                                        <td  style=\"color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;\">

                                           This is your new password:<b> $new_password</b><br>
                                           <span style=\"color: #a4a4a4;\">Please don't forget to change your password in the app.</span>

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>


                                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                                    <tbody><tr><td height=\"30\" width=\"30\"></td></tr>
                                    </tbody>
                                </table>


                                <table class=\"section-item\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                                    <tbody><tr><td height=\"6\"></td></tr>
                                    <tr>
                                        <td><a href=\"\" style=\"width: 128px; display: none;\"><img  style=\"display: block;\" src=\"\" alt=\"image4\" class=\"section-img\" height=\"auto\" width=\"128\"></a></td>
                                    </tr>
                                    <tr><td height=\"10\"></td></tr>
                                    </tbody>
                                </table>


                            </td>
                        </tr>

                        <tr><td height=\"20\"></td></tr>

                        </tbody></table>
                </td>
            </tr>


            </table>";

        $mail = new PHPMailer;
        $mail->isSMTP();                                                                       // Set mailer to use SMTP
        $mail->Host = 'mail.smtp2go.com';                                                      // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                                                // Enable SMTP authentication
        $mail->Username = 'smtp@loveradio.com.ph';                                    // SMTP username
        $mail->Password = 'm7AGLzxRPRet';                                                   // SMTP password
        $mail->SMTPSecure = 'tls';                                                             // Enable TLS encryption, `ssl` also accepted
        $mail->Port = "80";                                                                    // TCP port to connect to
        $mail->setFrom($email_from, "Love Radio App");
        $mail->addAddress($email_to, $email_toName);
        $mail->addReplyTo('no-reply@loveradio.com.ph', 'Love Radio App');
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        // $mail->AddEmbeddedImage('../img/email-img/image1.png', 'my-photo', 'image1.jpg ');
        $mail->Body = $message;
        $email_sent = 0;
        if (!$mail->send()) {

            $i['status'] = false;
            $i['message'] = "Failed to send email. " . $mail->ErrorInfo . ".";
        } else {
            $email_sent = 1;
        }

        #check if all is good
        if ($email_sent && $password_updated) {
            $i['status'] = true;
            $i['message'] = "Password was reset successfully. Please check your email: $email_to";
        } else {
            $i['status'] = false;
            $i['message'] = "There is something wrong with updating your password. Please try again.";
        }

    } else {
        $i['status'] = false;
        $i['message'] = "Email does not exist in the database.";
    }
    echo json_encode($i);

} else {
    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);

}
