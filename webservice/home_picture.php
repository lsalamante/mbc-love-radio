<?php

include('../jp_library/jp_lib.php');

$params['table'] = "home";

$row = mysqli_fetch_assoc(jp_get($params));

$i['status'] = true;
$i['thumbnail'] = $row['thumbnail'];
$i['greeting'] = $row['greeting'];
$i['welcome_message'] = $row['welcome_message'];

echo json_encode($i);