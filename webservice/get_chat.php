<?php
include('../jp_library/jp_lib.php');


if(isset($_POST['counter']) #page counter for pagination
){

  $counter = $_POST['counter'];

  $params['table'] = 'chat';

  $params['filters'] = "ORDER BY id DESC LIMIT $counter";

  $result = jp_get($params);

  $x = 0;

  if($result->num_rows > 0){
    foreach($result as $row){

      $i['status'] = true;


      unset($result);
      unset($params);

      $params['table'] = 'users';
      $params['where'] = "id ='".$row['user_id']."'";
      $result = mysqli_fetch_assoc(jp_get($params));

      $i['chat'][$x] = array(
        "id" => $row['id'],
        "user_id" => $row['user_id'],
        "nickname" => $row['nickname'],
        "chat_message" => $row['chat_message'],
        "profile_picture" => $result['avatar'],
        "timestamp" => $newDateTime = date('h:i A', strtotime($row['timestamp'])),
      );

      $x++;
    }
    $i['chat'] = array_reverse($i['chat']);
  }else{
    $i['status'] = true;
    $i['chat'] = [];
  }

  echo json_encode($i);

}
else{
  $i['status'] = false;
  $i['message'] = "Failed to read expected parameters";
  echo json_encode($i);
}
