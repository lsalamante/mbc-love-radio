<?php
    #INCLUDES
    include('../jp_library/jp_lib.php');


if(isset($_POST['device_id'])){
    
    $device_id = $_POST['device_id'];
    
    if(isset($_POST['user_id']) == false)
    $_POST['user_id'] = 0;
    
    if($_POST['user_id'] == "")
    $_POST['user_id'] = 0;
    
    $id = $_POST['user_id'];
    
    $params['table'] = "devices";

//    $params['where'] = "user_id = $id AND device_id = $device_id";
    
    $result = jp_get($params);

    $existing = false;
    
    foreach($result as $row){

        if($id == 0){
            if($row['device_id'] == $device_id){
                $existing = true;
            }
        }else{
            if($row['user_id'] == $id && $row['device_id'] == $device_id){
                $existing = true;
            }
        }
        
    }
    
    if($existing){
        $i['status'] = false;  
        $i['message'] = "Duplicate device ID";
    }else{
        
        unset($params); #unset our favorite variable

        $params['table'] = 'devices';

        $params['data'] = array(
            "user_id" => $id,
            "device_id" =>  $device_id,
        );

        $result = jp_add($params);
        
        $i['status'] = true;  
        $i['message'] = "Successfully added device ID";
    }

    echo json_encode($i);
    
}
else{
    $i['message'] = "Failed to read expected parameters.";

    echo json_encode($i);
}
?>