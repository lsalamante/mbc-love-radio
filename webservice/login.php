<?php
include('../jp_library/jp_lib.php');


if (isset($_POST['email']) && isset($_POST['password'])) {
    #Hash the password with SHA1
    $_POST['password'] = sha1($_POST['password']);

    $params['table'] = 'users';
    $params['where'] = "email = '" . $_POST['email'] . "' AND " . "password = '" . $_POST['password'] . "'";

    $result = jp_get($params);

    $row = mysqli_fetch_assoc(jp_get($params));

    $interests = explode(",", $row['interests']);

    if ($result->num_rows > 0) {
        #If user's account is already activated
        if ($row['status'] == 1) {

            $id = $row['id'];
            $i['id'] = $id;

            $i['fname'] = $row['fname'];
            $i['lname'] = $row['lname'];
            $i['email'] = $row['email'];
            $i['mobile_num'] = $row['mobile_num'];
            $i['birthdate'] = $row['birthdate'];
            $i['sex'] = $row['sex'];
            $i['country'] = $row['country'];
            $i['city'] = $row['city'];
            $i['interests'] = $interests;
            $i['avatar'] = $row['avatar'];

            $i['status'] = true;
            $i['message'] = "Login success";

        } else if ($row['status'] == 0){
            $i['status'] = false;
            $i['message'] = "Failed to login. Please activate your account at: " . $row['email'] . " first.";
        }
        else if ($row['status'] == -1){
            $i['status'] = false;
            $i['message'] = "Sorry, you have been banned by the administrator.";
        }
    } else {
        $i['status'] = false;
        $i['message'] = "Wrong email or password";
    }
    echo json_encode($i);
} else {
    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);

}
