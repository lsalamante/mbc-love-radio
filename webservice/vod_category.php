<?php

include('../jp_library/jp_lib.php');

$params['table'] = "vod_category";
$params['where'] = "status = '1' AND is_featured = '1'";
$params['filters'] = "ORDER BY featured_at DESC";

$resultFeatured = jp_get($params);

unset($params);

$params['table'] = "vod_category";
$params['where'] = "status = '1' AND is_featured = '0'";

$result = jp_get($params);

$i['status'] = true;

$x = 0;
foreach ($resultFeatured as $row) {

    $i['vod_category'][$x]["id"] = $row["id"];
    $i['vod_category'][$x]["title"] = $row["title"];
    $i['vod_category'][$x]["thumbnail"] = $row["thumbnail"];
    $i['vod_category'][$x]["is_featured"] = $row["is_featured"];

    $x = $x + 1;
}


foreach ($result as $row) {

    $i['vod_category'][$x]["id"] = $row["id"];
    $i['vod_category'][$x]["title"] = $row["title"];
    $i['vod_category'][$x]["thumbnail"] = $row["thumbnail"];
    $i['vod_category'][$x]["is_featured"] = $row["is_featured"];

    $x = $x + 1;
}

if(!array_key_exists('vod_category', $i)){
    $i['vod_category'] = [];
} 

echo json_encode($i);
