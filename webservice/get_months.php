<?php
include('../jp_library/jp_lib.php');


$params["table"] = "events";
$params["filters"] = "ORDER BY date DESC LIMIT 1";

$result = mysqli_fetch_assoc(jp_get($params));

$date = explode("-", $result['date']);

$latest_year = $date[0];
$latest_month = $date[1];

unset($params);
unset($result);

$params['table'] = "months";
$params['filters'] = "ORDER BY id ASC";
$result = jp_get($params);


$i['status'] = true;
$i['months'] = array();
$current_month = date('m');
$current_year = date('Y');

//echo date("Y-m-d H:i:s");

foreach ($result as $row) {

    if ($row["id"] >= $current_month) {

        array_push($i['months'],
            array(
                "month_name" => $row['month_name'],
                "month_id" => $row['id'],
                "year" => $current_year,
            ));
    }
}

if ($current_year != $latest_year) {

    foreach ($result as $row) {

        if ($latest_month        > $row["id"]) {

            array_push($i['months'],
                array(
                    "month_name" => $row['month_name'],
                    "month_id" => $row['id'],
                    "year" => $latest_year,
                ));
        }
    }

}

echo json_encode($i);
