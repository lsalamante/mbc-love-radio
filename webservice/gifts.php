<?php
include('../jp_library/jp_lib.php');


    if(isset($_POST['counter']) #page counter for pagination
    ){

        $counter = $_POST['counter'];

        if($counter == 1){
            $limit = "LIMIT 10";
        }else{
            $counter = ($counter - 1 ) * 10;
            $limit = "LIMIT 10 OFFSET $counter";
        }

        $params['table'] = 'gifts';

        $params['filters'] = "ORDER BY id DESC $limit";

        $result = jp_get($params);


        $x = 0;

        if($result->num_rows > 0){
            foreach($result as $row){

                $i['status'] = true;
                $i['gifts'][$x] = array(
                    "id" => $row['id'],
                    "title" => $row['title'],
                    "sub_title" => $row['sub_title'],
                    "description" => $row['description'],
                    "start_date" => $row['start_date'],
                    "end_date" => $row['end_date'],
                    "thumbnail" => $row['thumbnail'],
                    "weblink" => $row['weblink'],
                );

                $x++;
            }
        }else{
            $i['status'] = true;
            $i['gifts'] = [];
        }

        echo json_encode($i);

    }
    else{
        $i['status'] = false;
        $i['message'] = "Failed to read expected parameters";
        echo json_encode($i);
    }
