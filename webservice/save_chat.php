<?php
include('../jp_library/jp_lib.php');

if(
  isset($_POST['user_id']) &&
  isset($_POST['nickname']) &&
  isset($_POST['chat_message']))
  {

    $params['table'] = "chat";
    $params['data'] = $_POST;

    $result = jp_add($params);

    if ($result) {

      $i['status'] = true;

      echo json_encode($i);
    }
  }
  else{

    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);

  }
