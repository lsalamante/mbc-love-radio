<?php
include('../jp_library/jp_lib.php');

if (isset($_POST['email'])) {
    $params['table'] = 'users';
    $result = jp_get($params);
    $existing = false;

    if ($result) {
        foreach ($result as $row) {
            if ($row['email'] == $_POST['email']) {
                $existing = true;
                break;
            }

        }
    }

    $domain = explode('@', $_POST['email']);

    #checks if domain is INVALID
    if (!filter_var(gethostbyname($domain[1]), FILTER_VALIDATE_IP)) {
        $i['status'] = false;
        $i['message'] = $domain[1] . " is not a valid domain";
    } else if (!$existing) {
        $i['status'] = true;
        $i['message'] = "Email is available";
    } else if ($existing) {
        if($row['status'] == 0){

        $i['status'] = false;
        $i['message'] = "That email already exists in our database. Please activate your email to complete your registration.";
      }else if($row['status'] == 1){
        $i['status'] = false;
        $i['message'] = "Email is already in use.";
      }
    }

    echo json_encode($i);
} else {
    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);
}
