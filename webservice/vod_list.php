<?php
include('../jp_library/jp_lib.php');

if (isset($_POST['category_id']) && isset($_POST['counter'])) {

    $counter = $_POST['counter'];

    if ($counter == 1) {
        $limit = "LIMIT 10";
    } else {
        $counter = ($counter - 1) * 10;
        $limit = "LIMIT 10 OFFSET $counter";
    }

    $params['table'] = "vods";
    $params['filters'] = "ORDER BY id DESC $limit";
    $params['where'] = "category_id = '" . $_POST['category_id'] . "'";

    $result = jp_get($params);
    #INITIALIZE

    #STATIC
    if ($result->num_rows > 0) {

        $i['status'] = true;

        $x = 0;
        foreach ($result as $row) {

            $i['vod_videos'][$x]["id"] = $row["id"];
            //        $i['vod_videos'][$x]["category_id"] = $row["category_id"];
            $i['vod_videos'][$x]["title"] = $row["title"];
            $i['vod_videos'][$x]["uploader"] = $row["uploader"];
            $i['vod_videos'][$x]["link"] = $row["link"];
            $i['vod_videos'][$x]["duration"] = $row["duration"];
            $i['vod_videos'][$x]["thumbnail"] = $row["thumbnail"];

            $x = $x + 1;
        }
    } else {
        $i['status'] = true;
        $i['vod_videos'] = [];
    }

    echo json_encode($i);
} else {
    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);

}

