<?php
include('../jp_library/jp_lib.php');
$json = file_get_contents('../world.json');
$world = json_decode($json, true);
$countries = array_keys($world);
sort($countries);

$i['status'] = true;

if (isset($_POST['country'])) {
    #SHOW ALL CITIES OF A SPECIFIC COUNTRY
    $i['cities'] = array();

    $cities = $world[$_POST['country']];


    for ($x = 0; $x < count($cities); $x++) {
        array_push($i['cities'], $cities[$x]);
    }

    sort($i['cities']);

    if (count($i['cities']) < 1) {
        $i['status'] = false;
        $i['message'] = "No such country exists. Please check your spelling.";
        unset($i['cities']);
    }

} else {
    #SHOW ALL COUNTRIES not CITIES!
    $i['countries'] = array();

    #ADD VALUES TO COUNTRIES ARRAY
    for ($x = 0; $x < count($countries); $x++) {
        if (!($countries[$x] == ""))
            array_push($i['countries'], $countries[$x]);
    }


}

echo json_encode($i);