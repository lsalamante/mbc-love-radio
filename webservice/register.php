<?php
include('../jp_library/jp_lib.php');
require('../PHPMailer-master/PHPMailerAutoload.php');

if (isset($_POST['fname']) &&
    isset($_POST['lname']) &&
    isset($_POST['email']) &&
    isset($_POST['password']) &&
    isset($_POST['birthdate']) &&
    isset($_POST['sex']) &&
    isset($_POST['country']) &&
    isset($_POST['city']) &&
    isset($_POST['mobile_num']) &&
    isset($_POST['interests'])
) {
    #variable inits
    $email = $_POST['email'];
    $_POST['password'] = sha1($_POST['password']);

    #query db with the given email
    $params['select'] = "COUNT(*)";
    $params['table'] = "users";
    $params['where'] = "email = '$email''";
    $result = jp_get($params);

    #what to do if email is existing
    if ($result > 0) {
        $i['status'] = false;
        $i['message'] = $_POST['email'] . " already exists in our database. Please activate your email to complete your registration.";
    } else {
        unset($params);
        unset($result);

        #Email stuffs start
        $email_to = $_POST['email'];
        $email_toName = $_POST['fname'];

        $email_from = 'no-reply@loveradio.com.ph';
        $subject = "Confirm Registration";
//        $message = "
//            <html>
//            <body>
//                <p>Hello $email_toName!</p>
//                <p>Please confirm your registration by clicking this link or copy-pasting it in your browser:<br><a href='" . $BASE_URL . "webservice/confirm_register.php?email=$email_to'>http://" . $BASE_URL . "webservice/confirm_register.php?email=$email_to</a></p>
//            </body>
//            </html>";

        $message = "<table class=\"container-middle\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"560\" bgcolor=\"F1F2F7\">
            <tr >
                <td>
                    <table class=\"mainContent\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"528\">
                        <tbody><tr><td height=\"20\"></td></tr>
                        <tr>
                            <td>

                                <table class=\"section-item\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"360\">
                                    <tbody><tr>
                                        <td  style=\"color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;\">

                                            Kabisyo, $email_toName!

                                        </td>
                                    </tr>
                                    <tr><td height=\"15\"></td></tr>
                                    <tr>
                                        <td  style=\"color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;\">

                                            Please confirm your registration by clicking the button below or copying the link on your browser.

                                        </td>
                                    </tr>
                                    <tr><td height=\"20\"></td></tr>
                                    <tr>
                                        <td>
                                            <a href=\" " .$BASE_URL . "webservice/confirm_register.php?email=$email_to\" style=\"background-color: #FF9800; font-size: 12px; padding: 10px 15px; color: #fff; text-decoration: none\"> Confirm Registration</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>


                                <table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                                    <tbody><tr><td height=\"30\" width=\"30\"></td></tr>
                                    </tbody>
                                </table>


                                <table class=\"section-item\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                                    <tbody><tr><td height=\"6\"></td></tr>
                                    <tr>
                                        <td><a href=\"\" style=\"width: 128px; display: none;\"><img  style=\"display: block;\" src=\"\" alt=\"image4\" class=\"section-img\" height=\"auto\" width=\"128\"></a></td>
                                    </tr>
                                    <tr><td height=\"25\"></td></tr>
                                    </tbody>
                                </table>

                                <table class=\"section-item\" align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-top:35px\">
                                    <tbody>
                                    <tr>
                                        <td>
                                            " .$BASE_URL . "webservice/confirm_register.php?email=$email_to
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>


                            </td>
                        </tr>

                        <tr><td height=\"20\"></td></tr>

                        </tbody></table>
                </td>
            </tr>


            </table>";


        $mail = new PHPMailer;
        $mail->isSMTP();                                                                       // Set mailer to use SMTP
        $mail->Host = 'mail.smtp2go.com';                                                      // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                                                // Enable SMTP authentication
        $mail->Username = 'smtp@loveradio.com.ph';                                    // SMTP username
        $mail->Password = 'm7AGLzxRPRet';                                                  // SMTP password
        $mail->SMTPSecure = 'tls';                                                             // Enable TLS encryption, `ssl` also accepted
        $mail->Port = "80";                                                                    // TCP port to connect to
        $mail->setFrom($email_from, "Love Radio TV App");
        $mail->addAddress($email_to, $email_toName);
        #TODO: Change this according to the SMTP host
        $mail->addReplyTo('no-reply@loveradio.com.ph', 'Love Radio TV App');
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        // $mail->AddEmbeddedImage('../img/email-img/image3.png', 'my-photo', 'image3.jpg ');
        $mail->Body = $message;
        $email_sent = 0;
        if (!$mail->send()) {
            $i['status'] = false;
            $i['message'] = "Failed to send email. Please try again.";
        } else {

            #insert to DB
            $params['table'] = 'users';
            $params['data'] = $_POST;
            $result = jp_add($params);

            $email_sent = 1;
        }

        if ($email_sent) {
            $i['status'] = true;
            $i['message'] = "Please check your email: $email_to to confirm your registration.";
        }

    }

    echo json_encode($i);
} else {
    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);

}
