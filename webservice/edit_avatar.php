<?php
include('../jp_library/jp_lib.php');
include('../jp_library/s_upload.php');

if (isset($_FILES['avatar']) && isset($_POST['id'])) {

    $s_upload = s_upload("../uploads/", "avatar_", $_FILES["avatar"], $_POST['id']);

    $upload_msg = $s_upload['msg'];

    $s_upload['path'] = ltrim($s_upload['path'], '\.\./'); #USE THIS ONLY WHEN BACKTRACKING FROM DIRECTORIES ex. -> "../uploads"
    $avatar = $BASE_URL . $s_upload['path'];

    $params['table'] = 'users';
    $params['where'] = "id = '" . $_POST['id'] . "'";
    #OVERWRITTEN POST VARIABLES
    $params['data'] = array(
        "avatar" => $avatar
    );

    $result2 = jp_update($params);
    #CHECK IF BOTH UPDATE AND UPLOAD IS SUCCESSFUL
    if ($s_upload['status'] && $result2) {
        $i['status'] = true;
        $i['message'] = "Avatar updated successfully";
        $i['path'] = $avatar;
    } else {
        $i['status'] = false;
        $i['message'] = "Avatar update failed horribly";
    }
    echo json_encode($i);
} else {
    $i['status'] = false;
    $i['message'] = "Failed to read expected parameters";
    echo json_encode($i);

}

