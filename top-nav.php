<div class="top-nav ">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <li>
            <?php if($SEARCH) { ?> <input type="text" class="form-control search" placeholder="Search"> </li>
        <?php } ?>
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <?php if($AVATAR) echo '<img alt="" src="img/admin.png">'; ?>
                                <span class="username">
<!--                               Welcome back, -->
                                <?php
                                    if (isset($_SESSION['full_name']))
                                        echo $_SESSION['full_name'];
                                ?>
                                </span> <b class="caret"></b> </a>
            <ul class="dropdown-menu extended logout">
                <div class="log-arrow-up"></div>
<!--                <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>-->
<!--                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>-->
<!--                <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>-->
                <li><a href="logout.php"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <?php if($RIGHT_SIDEBAR)
                    {
                       echo '<li class="sb-toggle-right"> <i class="fa  fa-align-right"></i> </li>';
                    }
 ?>
        <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
</div>
