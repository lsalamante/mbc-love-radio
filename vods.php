<?php
#INCLUDES
include('jp_library/jp_lib.php');
include('jp_library/s_upload.php');
include('jp_library/s_video_upload.php');
// include getID3() library (can be in a different directory if full path is specified)
require_once('getid3/getid3.php');

// Initialize getID3 engine
$getID3 = new getID3;
if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}

$DYNAMIC_TABLE = true;
$PICKERS = true;
$LOADER = true;

if (isset($_POST['title']) &&
    isset($_POST['category_id']) &&
    isset($_POST['duration']) &&
    isset($_POST['link']) &&
    isset($_FILES['thumbnail'])
) {

    if (isset($_POST['notify']) == false) {
        $_POST['notify'] = 0;
    }

    #for notification purposes only
    $category_id = $_POST['category_id'];

    $params['select'] = "title";
    $params['table'] = "vod_category";
    $params['where'] = "id = $category_id";
    $cat = mysqli_fetch_assoc(jp_get($params));
    unset($params);

    $all_ok = 0;

    #HACK

    $_POST['uploader'] = $_SESSION['full_name'];
    $params['table'] = "vods";
    $params['data'] = $_POST;
    $result = jp_add($params);

    $last_vod_id = jp_last_added(); #get our last ID

    if ($result) {
        $s_upload = s_upload("uploads/", "vod_thumbnail_", $_FILES["thumbnail"], $last_vod_id);
       

        if ($s_upload['status']) {
            $thumbnail = $BASE_URL . $s_upload['path']; #get file URL

            unset($params); #unset our favourite variable right here
            unset($result); #unset our favourite variable right here

            $params['table'] = 'vods';
            $params['where'] = "id = $last_vod_id";

            $params['data'] = array(
                "thumbnail" => $thumbnail,
            );

            $result = jp_update($params);

            if ($result) {
                $all_ok = 1;
            }

        } else { #delete last id
            #HACK only!
            #todo make it so that it never inserts at all
            #possible or no?
            unset($params); #unset our favourite variable right here
            unset($result); #unset our favourite variable right here

            #delete from DB
            $delete_id = $last_vod_id;
            $params['table'] = "vods";
            $params['where'] = "id = $delete_id";
            $result = jp_delete($params);

        }
    }

    if ($all_ok) {

        $status_msg = "Successfully added new VOD.";
        if ($_POST['notify'] == 1) {
            ###########FIREBASE HERE
            require_once __DIR__ . '/firebase.php';
            require_once __DIR__ . '/vod.php';

            $firebase = new Firebase();
            $vod = new Vod();

            $title = $_POST['title'];
            $uploader = $_POST['uploader'];

            $vod->setId($last_vod_id);
            $vod->setTitle($title);
            $vod->setVodCategory($cat['title']);
            $vod->setUploader($uploader);
            $vod->setVodUrl($_POST['link']);
            $vod->setThumbnailUrl($thumbnail);
            $vod->setDuration($_POST['duration']);

            $json = '';
            $response = '';

            #XXXX
            #TOPIC TITLE IS HERE!!!
            $json = $vod->getVod();
            
            $notification['title'] = "New VOD available!";
            $notification['body'] = $title . " uploaded by " . $uploader;
            
            $response = $firebase->sendToTopic('exclusive_content', $json, $notification);
            ###########/ FIREBASE HERE

            $status_msg .= " All users notified.";
        }

    } else {

        if ($s_video_upload['status']) {

            #video from DISK
            $unlinked_mp4 = 0;
            $mp4 = "uploads/vod_" . $last_vod_id . ".mp4";
            if (unlink($mp4)) {
                $unlinked_mp4 = 1;
            }

        }
        if ($s_upload['status']) {

            #image delete from DISK
            $unlinked_jpg = 0;
            $jpg = "uploads/vod_thumbnail_" . $last_vod_id . ".jpg";
            if (unlink($jpg)) {
                $unlinked_jpg = 1;
            }

        }

        $status_msg = "Failed to add new VOD. " . $s_video_upload['msg'];
    }

}

if (isset($_POST['delete_id'])) {

    #delete from DB
    $delete_id = $_POST['delete_id'];

     $d['select'] = "thumbnail";
      $d['table'] = "vods";
      $d['where'] = "id = $delete_id";

      $fileUrl = mysqli_fetch_assoc(jp_get($d));
      $fileUrl = array_pop(explode("/", $fileUrl['thumbnail']));

    $params['table'] = "vods";
    $params['where'] = "id = $delete_id";
    $result = jp_delete($params);
 

    #image delete from DISK
    $unlinked_jpg = 0;
    $jpg = "uploads/". $fileUrl;
    if (unlink($jpg)) {
        $unlinked_jpg = 1;
    }

    if ($result || $unlinked_jpg) {
        $status_msg = "Row deleted.";
        $all_ok = 1;
        unset($params);
        unset($result);
    }
}

#Refresh our variables right here
unset($params);
#VIEWING
$params['table'] = "vods";
$params['filters'] = "ORDER BY id DESC";
$vods = jp_get($params);

?>
    <!DOCTYPE html>
    <html lang="en">
    <?php include('header.php'); ?>

        <body>
            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
                        <!--logo start-->
                        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
                            <!--logo end-->
                            <div class="nav notify-row" id="top_menu">
                                <!--  notification start -->
                                <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
                                    <!--  notification end -->
                            </div>
                            <?php include('top-nav.php'); ?>
                </header>
                <!--header end-->
                <!--sidebar start-->
                <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
                    <!--sidebar end-->
                    <!--main content start-->
                    <section id="main-content">
                        <section class="wrapper site-min-height">
                            <!-- page start-->
                            <div class="row">
                                <div class="col-lg-12">
                                    <section class="panel">
                                        <header class="panel-heading"> Add new VOD
                                            <br> <sub <?php if (isset($all_ok)) { if ($all_ok) { echo "class='status-ok'"; } else { echo "class='status-not-ok'"; } ?>
                                <?php } ?>
                            ><?php echo isset($status_msg) ? $status_msg : ''; ?></sub>
                                        </header>
                                        <div class="panel-body">
                                            <form class="form-horizontal" role="form" action=<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]); ?> method="post" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label for="title" class="col-lg-2 col-sm-2 control-label">VOD title</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="title" name="title" required placeholder="VOD title" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="title" class="col-lg-2 col-sm-2 control-label">VOD Category</label>
                                                    <div class="col-lg-10">
                                                        <select class="form-control" name="category_id" required>
                                                            <?php
                                            unset($params);
                                            $params['table'] = "vod_category";
                                            $params['filters'] = "ORDER BY id DESC";
                                            $params['where'] = "status = '1'";
                                            $categories = jp_get($params);
                                            foreach ($categories as $row) {
                                                $id = $row['id'];
                                                $name = $row['title'];

                                                echo "<option value='$id'>$name</option>";
                                            }
                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="link" class="col-lg-2 col-sm-2 control-label">VOD link</label>
                                                    <div class="col-lg-10">
                                                        <input type="url" class="form-control" id="link" name="link" required placeholder="http://example.com/videos/123456789/player?" required>
                                                        <span class="help-block">Please enter a valid VOD URL. Example:  <em> http://example.com/videos/123456789/player?</em></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="title" class="col-lg-2 col-sm-2 control-label">Duration</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="duration" name="duration" required placeholder="hh:mm:ss" pattern="([0-5][0-9]):([0-5][0-9]):([0-5][0-9])" required title="hh:mm:ss" data-mask="99:99:99">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="home_file" class="col-lg-2 col-sm-2 control-label">Thumbnail</label>
                                                    <div class="col-lg-10">
                                                        <input type="file" id="thumbnail" name="thumbnail" required>
                                                        <p class="help-block">Image files only. (jpeg, jpg, png, gif)</p>
                                                        <div class="loader-div hidden"><img class="loader" src="img/hourglass.gif" /> Uploading...</div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="home_file" class="col-lg-2 col-sm-2 control-label">Notify all users for adding this item?</label>
                                                    <div class="col-lg-10">
                                                        <input type="checkbox" value="1" style="width:20px" class="checkbox form-control" name="notify">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button type="submit" class="btn btn-info">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                </div>
                                <div class="col-lg-12">
                                    <section class="panel">
                                        <header class="panel-heading"> VODs<span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span></header>
                                        <div class="panel-body">
                                            <div class="adv-table">
                                                <table class="display table table-bordered table-striped" id="dynamic-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Title</th>
                                                            <th>Category</th>
                                                            <th>Uploader</th>
                                                            <th>VOD URL</th>
                                                            <th>Thumbnail URL</th>
                                                            <th>Duration</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                    foreach ($vods as $row) {
                                        #variables
                                        $category_id = $row['category_id'];

                                        echo '<tr>';
                                        echo '<td>' . $row["id"] . '</td>';
                                        echo '<td>' . $row["title"] . '</td>';

                                        #Get category name
                                        unset($params);
                                        $params['select'] = "title";
                                        $params['table'] = "vod_category";
                                        $params['where'] = "id = $category_id";
                                        $cat = mysqli_fetch_assoc(jp_get($params));

                                        echo '<td>' . $cat['title'] . '</td>';
                                        echo '<td>' . $row["uploader"] . '</td>';
                                        echo '<td><a href="' . $row["link"] . '" target="_blank">' . $row["link"] . '</a></td>';
                                        echo '<td><a href="' . $row["thumbnail"] . '" target="_blank">' . $row["thumbnail"] . '</a></td>';
                                        echo '<td>' . $row["duration"] . '</td>';
                                        echo '<td>'; ?>

                                                            <form style='display:inline;' onsubmit="return confirm('Edit row #<?= $row['id'] ?>?');" action="edit_vod.php" method="post" class="no-loader">
                                                                <input type="hidden" name="edit_id" value="<?= $row["id"]; ?>">
                                                                <input type="hidden" name="edit_page" value="<?= $PAGE_NAME ?>">
                                                                <button class="btn btn-primary btn-xs" type="submit"><i class="fa fa-pencil "></i></button>
                                                            </form>

                                                            <form style='display:inline;' onsubmit="return confirm('Are you sure you want to delete that?');" action=<?php echo htmlspecialchars($_SERVER[ "PHP_SELF"]); ?> method="post" class="no-loader">
                                                                <input type="hidden" name="delete_id" value="<?php echo $row["id"]; ?>">
                                                                <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-o "></i></button>
                                                            </form>
                                                            <?php
                                        echo '</td>';
                                        echo '</tr>';
                                    }

                                    ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Title</th>
                                                            <th>Category</th>
                                                            <th>Uploader</th>
                                                            <th>VOD URL</th>
                                                            <th>Thumbnail URL</th>
                                                            <th>Duration</th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </section>
                                </div>

                            </div>
                            <!-- page end-->
                            </section>
                        </section>
                        <!--main content end-->
                        <!-- Right Slidebar start -->
                        <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
                            <!-- Right Slidebar end -->
                            <!--footer start-->
                            <?php include('footer.php'); ?>
                                <!--footer end-->
                    </section>
                    <?php include('scripts.php'); ?>
        </body>

    </html>