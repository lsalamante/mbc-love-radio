<?php
class Event {

    private $id;
    private $title;
    private $sub_title;
    private $location;
    private $description;
    private $event_date;
    private $event_time;
    private $short_desc;
    private $full_desc;
    private $thumbnail;
    private $month_name;
    private $weblink;

    function __construct() {

    }
    public function setId($arg) {
        $this->id = $arg;
    }
    public function setTitle($arg) {
        $this->title = $arg;
    }
    public function setSubTitle($arg) {
        $this->sub_title = $arg;
    }
    public function setLocation($arg) {
        $this->location = $arg;
    }
    public function setDate($arg) {
        $this->event_date = $arg;
    }
    public function setTime($arg) {
        $this->event_time = $arg;
    }
    public function setShortDesc($arg) {
        $this->short_desc = $arg;
    }
    public function setFullDesc($arg) {
        $this->full_desc = $arg;
    }
    public function setThumbnailUrl($arg) {
        $this->thumbnail = $arg;
    }
    public function setMonth($arg) {
        $this->month_name = $arg;
    }
    public function setDescription($arg) {
        $this->description = $arg;
    }
    public function setWeblink($arg) {
        $this->weblink = $arg;
    }

    public function getEvent() {
        $res = array();
        $res['data']['status'] = true;
        $res['data']['category'] = "event";

        $res['data']['id'] = $this->id;
        $res['data']['title'] = $this->title;
        $res['data']['sub_title'] = $this->sub_title;
        $res['data']['location'] = $this->location;
        $res['data']['date'] = $this->event_date;
        $res['data']['time'] = $this->event_time;
        $res['data']['month'] = $this->month_name;
        $res['data']['description'] = $this->description;
        $res['data']['thumbnail'] = $this->thumbnail;
        $res['data']['weblink'] = $this->weblink;

        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }

}
