<?php
$debug = 0;
#INCLUDES
include('jp_library/jp_lib.php');

if(!isset($_SESSION['is_logged_in'])){
    header("Location: " . "login.php");
    die();
}

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
    <section id="container">
        <!--header start-->
        <header class="header white-bg">
            <?php
            if($LEFT_SIDEBAR)
                {
                    echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
                }
            ?>
                <!--logo start-->
                <?php if($LOGO)
                {
                    include('logo.php');
                }
            ?>
                <!--logo end-->
                <div class="nav notify-row" id="top_menu">
                    <!--  notification start -->
                    <?php if($NOTIFICATION) {
                include('notification.php');
                } ?>
                    <!--  notification end -->
                </div>
                <?php include('top-nav.php'); ?>
        </header>
        <!--header end-->
        <!--sidebar start-->
        <?php
            if($LEFT_SIDEBAR){
            include('left-sidebar.php');
           }
        ?>
        <!--sidebar end-->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <!-- page start-->

        <?php
        // Enabling error reporting
        error_reporting(-1);
        ini_set('display_errors', 'On');

        require_once __DIR__ . '/firebase.php';
        require_once __DIR__ . '/announcement.php';

        $firebase = new Firebase();
        $announcement = new Announcement();

        $title = isset($_GET['title']) ? $_GET['title'] : '';

        $message = isset($_GET['message']) ? $_GET['message'] : '';

        $push_type = isset($_GET['push_type']) ? $_GET['push_type'] : '';

        $announcement->setTitle($title);
        $announcement->setMessage($message);

        $json = '';
        $response = '';

                #XXXX
        #TOPIC TITLE IS HERE!!!
        if ($push_type == 'topic') {
            $json = $announcement->getAnnouncement();

            $notification['title'] = $title;
            $notification['body'] = $message;

            $response = $firebase->sendToTopic('public', $json, $notification);
        }
        ?>

        <div class="col-lg-6">
            <section class="panel">
                <header class="panel-heading">Announcement<br>
                   <sub style="<?php if ($debug == 0) echo 'display:none' ?>">

                     <?php if ($json != '') { ?>
                    <br><label><b>Debug info:</b></label>
                    <br><label><b>Request:</b></label>
                    <div class="json_preview">
                        <pre><?php echo json_encode($json) ?></pre>
                    </div>
                <?php } ?>
                <br/>
                <?php if ($response != '') { ?>
                    <label><b>Response:</b></label>
                    <div class="json_preview">
                        <pre><?php echo json_encode($response) ?></pre>
                    </div>
                <?php } ?>

                    </sub>
                   <sub class='status-ok'>
                    <?php if ($response != '') { ?>
                    Announcement successful
                <?php } ?>
                    </sub>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="get">
                        <div class="form-group">
                            <label for="title" class="col-lg-2 col-sm-2 control-label">Title</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="title" name="title" placeholder="Notification title" required> </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-lg-2 col-sm-2 control-label">Message</label>
                            <div class="col-lg-10">
                                <textarea style="resize:vertical" class="form-control" placeholder="Notification message" name="message" id="message" required></textarea>
                                </div>
                        </div>

                        <input type="hidden" name="push_type" value="topic"/>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

                <!-- page end-->
            </section>
        </section>
        <!--main content end-->
        <!-- Right Slidebar start -->
        <?php
            if($RIGHT_SIDEBAR){
     include('right-sidebar.php');
            }
    ?>
        <!-- Right Slidebar end -->
        <!--footer start-->
        <?php include('footer.php'); ?>
        <!--footer end-->
    </section>
    <?php include('scripts.php'); ?>
</body>

</html>
