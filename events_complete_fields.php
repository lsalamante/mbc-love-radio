<?php
#INCLUDES
include('jp_library/jp_lib.php');
include('jp_library/s_upload.php');

if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}


$json = '';
$response = '';
$debug = 1;


$DYNAMIC_TABLE = true;
$PICKERS = true;

if (isset($_POST['title']) &&
    isset($_POST['sub_title']) &&
    isset($_POST['location']) &&
    isset($_POST['date']) &&
    isset($_POST['time']) &&
    isset($_POST['short_desc']) &&
    isset($_POST['full_desc']) &&
    isset($_FILES['thumbnail'])
) {

    if (isset($_POST['notify']) == false) {
        $_POST['notify'] = 0;
    }

    $all_ok = 0;

    #convert time format
    $_POST['time'] = date('H:i:s', strtotime($_POST['time']));
    #HACK
    $date = $_POST['date'];
    $date = explode("-", $date);
    $_POST['date'] = date('Y-m-d', strtotime("$date[2]-$date[0]-$date[1]"));
    #This hack turned out to be useful, I guess
    $_POST['month_id'] = $date[0];

    $params['table'] = "events";
    $params['data'] = $_POST;
    $result = jp_add($params);

    $last_event_id = jp_last_added(); #get our last ID

    if ($result) {
        $s_upload = s_upload("uploads/", "event_thumbnail_", $_FILES["thumbnail"], $last_event_id);

        if ($s_upload['status']) {
            $thumbnail = $BASE_URL . $s_upload['path']; #get file URL

            unset($params); #unset our favourite variable right here
            unset($result); #unset our favourite variable right here

            $params['table'] = 'events';
            $params['where'] = "id = $last_event_id";

            $params['data'] = array(
                "thumbnail" => $thumbnail,
            );

            $result = jp_update($params);

            if ($_POST['notify'] == 1) {
                ###########FIREBASE HERE
                require_once __DIR__ . '/firebase.php';
                require_once __DIR__ . '/event.php';

                $firebase = new Firebase();
                $event = new Event();

                $event->setTitle($_POST['title']);
                $event->setSubTitle($_POST['sub_title']);
                $event->setLocation($_POST['location']);
                $event->setDate($_POST['date']);
                $event->setTime($_POST['time']);
                $event->setShortDesc($_POST['short_desc']);
                $event->setFullDesc($_POST['full_desc']);
                $event->setThumbnailUrl($thumbnail);

                $json = $event->getEvent();
                $response = $firebase->sendToTopic('exclusive_content', $json);
                ###########/ FIREBASE HERE
            }


            if ($result) {
                $all_ok = 1;
            }
        }
    }

    if ($all_ok) {
        $status_msg = "Successfully added new event.";
    } else {
        $status_msg = "Failed to add new event.";
    }

} else {
    $status_msg = "Fill out the form below to add a new event.";
}


if (isset($_POST['delete_id'])) {
    #delete from DB
    $delete_id = $_POST['delete_id'];
    $params['table'] = "events";
    $params['where'] = "id = $delete_id";
    $result = jp_delete($params);

    #delete from DISK
    $unlinked = 0;
    $file = "uploads/event_thumbnail_" . $delete_id . ".jpg";
    if (unlink($file)) {
        $unlinked = 1;
    }

    if ($result && $unlinked) {
        $status_msg = "Row deleted.";
    }
}

unset($params);
unset($result);
#VIEWING
$params['table'] = "events";
$params['filters'] = "ORDER BY id DESC";
$events = jp_get($params);

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
<section id="container">
    <!--header start-->
    <header class="header white-bg">
        <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
        <!--logo start-->
        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
            <!--  notification end -->
        </div>
        <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading"> Events<span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
                            <br> <sub><?php echo isset($status_msg) ? $status_msg : ''; ?></sub>

                            <sub style="<?php if ($debug == 0) echo 'display:none' ?>">

                                <?php if ($json != '') { ?>
                                    <br><label><b>Debug info:</b></label>
                                    <br><label><b>Request:</b></label>
                                    <div class="json_preview">
                                        <pre><?php echo json_encode($json) ?></pre>
                                    </div>
                                <?php } ?>
                                <br/>
                                <?php if ($response != '') { ?>
                                    <label><b>Response:</b></label>
                                    <div class="json_preview">
                                        <pre><?php echo json_encode($response) ?></pre>
                                    </div>
                                <?php } ?>

                            </sub>
                            <sub>
                                <?php if ($response != '') { ?>
                                    Announcement successful
                                <?php } ?>
                            </sub>


                        </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Sub title</th>
                                        <th>Location</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Short description</th>
                                        <th>Full description</th>
                                        <th>Thumbnail</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($events as $row) {
                                        echo '<tr>';
                                        echo '<td>' . $row["id"] . '</td>';
                                        echo '<td>' . $row["title"] . '</td>';
                                        echo '<td>' . $row["sub_title"] . '</td>';
                                        echo '<td>' . $row["location"] . '</td>';

                                        $row['date'] = date('M d, Y', strtotime($row['date']));
                                        echo '<td>' . $row["date"] . '</td>';

                                        $row['time'] = date('h:i A', strtotime($row['time']));

                                        echo '<td>' . $row["time"] . '</td>';
                                        echo '<td>' . $row["short_desc"] . '</td>';
                                        echo '<td>' . $row["full_desc"] . '</td>';
                                        echo '<td>' . $row["thumbnail"] . '</td>';
                                        echo '<td>'; ?>
                                        <form style='display:inline;'
                                              onsubmit="return confirm('Are you sure you want to delete that?');"
                                              action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">
                                            <input type="hidden" name="delete_id" value="<?php echo $row["id"]; ?>">
                                            <button class="btn btn-danger btn-xs" type="submit"><i
                                                    class="fa fa-trash-o "></i></button>
                                        </form>
                                        <?php
                                        echo '</td>';
                                        echo '</tr>';
                                    }

                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Sub title</th>
                                        <th>Location</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Short description</th>
                                        <th>Full description</th>
                                        <th>Thumbnail</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading"> Add new event</header>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form"
                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                                  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="title" class="col-lg-2 col-sm-2 control-label">Title</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Event title" required></div>
                                </div>
                                <div class="form-group">
                                    <label for="sub_title" class="col-lg-2 col-sm-2 control-label">Sub title</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="sub_title" name="sub_title" required
                                               placeholder="Sub title"></div>
                                </div>
                                <div class="form-group">
                                    <label for="location" class="col-lg-2 col-sm-2 control-label">Location</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="location" name="location" required
                                               placeholder="Event location"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Date</label>
                                    <div class="col-lg-10">
                                        <input class="form-control form-control-inline input-medium default-date-picker"
                                               required
                                               name="date" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Time</label>
                                    <div class="col-lg-10">
                                        <div class="input-group bootstrap-timepicker">
                                            <input type="text" class="form-control timepicker-default" name="time"
                                                   required>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button"><i
                                                        class="fa fa-clock-o"></i></button>
                                                </span></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="short_desc" class="col-lg-2 col-sm-2 control-label">Short
                                        description</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" id="short_desc" name="short_desc"
                                               required
                                               placeholder="Short description here"></div>
                                </div>
                                <div class="form-group">
                                    <label for="full_desc" class="col-lg-2 col-sm-2 control-label">Full
                                        description</label>
                                    <div class="col-lg-10">
                                        <textarea name="full_desc" id="full_desc" placeholder="Full description here"
                                                  required
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="home_file" class="col-lg-2 col-sm-2 control-label">Thumbnail</label>
                                    <div class="col-lg-10">
                                        <input type="file" id="thumbnail" name="thumbnail" required>
                                        <p class="help-block">Image files only. (jpeg, jpg, png, gif)</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="home_file" class="col-lg-2 col-sm-2 control-label">Notify all users for
                                        adding this item?</label>
                                    <div class="col-lg-10">
                                        <input type="checkbox" value="1" style="width:20px"
                                               class="checkbox form-control " name="notify">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
</section>
<?php include('scripts.php'); ?>
</body>

</html>