# Love Radio Web App
* * *
#Table of contents
1. [Project description](#markdown-header-project-description)
1. [Technical specifications](#markdown-header-technical-specifications)
    + [Technologies used](#markdown-header-technologies-used)
    + [Dependencies](#markdown-header-dependencies)
    + [Third-party dependencies](#markdown-header-third-party-dependencies)
1. [Installation](#markdown-header-installation)
1. [Notes](#markdown-header-notes)
1. [CMS](#markdown-header-cms)
1. [Web services / API](#markdown-header-web-services)
    + [confirm_register](#markdown-header-confirm_register)
    + [edit_avatar](#markdown-header-edit_avatar)
    + [edit_profile](#markdown-header-edit_profile)
    + [events_list](#markdown-header-events_list)
    + [forgot_password](#markdown-header-forgot_password)
    + [get_months](#markdown-header-get_months)
    + [get_world](#markdown-header-get_world)
    + [gifts](#markdown-header-gifts)
    + [home_picture](#markdown-header-home_picture)
    + [live_radio](#markdown-header-live_radio)
    + [live_video](#markdown-header-live_video)
    + [login](#markdown-header-login)
    + [register](#markdown-header-register)
    + [vod_category](#markdown-header-vod_category)
    + [vod_list](#markdown-header-vod_list)

* * *
# Project description
Love Radio Web App is a CMS (Content-Management System) for the Love Radio Mobile App (Available in both Android and iOS). It also hosts the web services used by both mobile apps to interact with a common database.
* * *
# Technical specifications
## Technologies used
* Front-end: HTML5 and CSS3 using [Bootstrap FlatLab Theme](http://thevectorlab.net/flatlab/)
* Back-end: PHP, JavaScript
* Database: MySQL
* Design pattern: Procedural
## Dependencies
1. JP library
1. [PHP Mailer](https://github.com/PHPMailer/PHPMailer) for mail functions
1. [GetID3](http://getid3.sourceforge.net/) for getting media meta data. (eg. To get the duration of a specific MP4 file)
1. [Socket.io](http://socket.io/) for the peer to peer chat in mobile application
1. [Node.js](https://nodejs.org/en/) for setting up the chat server
1. [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging/) for sending notifications to the mobile apps
## Third-party Dependencies
* Winmedia for sending radio meta-data
* * *
# Installation  
This portion covers how to configure this project properly. You should expect the following in this section:  

* Database configuration
* Website routes
* Module activation / controls / settings
* Site information (eg. Site name, etc.)
* App constants

## jp_library/globals.php  

* This is where you activate and deactivate certain modules native to FlatLab theme  
* You should **NEVER** set the `$LOCAL_MODE` variable to `true` on production / test site!!!  
* You should always initialize `$PICKERS` to `true` variable on the page that you want to use them  
* Change your site name here
* Change your default site URL here
* You can find most of the constants here

## jp_library/con-define.php  
* Configure database connections here

* * *
# CMS
* * *
# Web services
This can be found in the `/webservice` directory  

###### Example usage
To use `login` webservice, use `http://domain.com/webservice/login.php` to make a request

### login
#### Request: POST | Response: JSON  
Use this web service to validate the user's credentials and return the user's information upon success
###### Expects
| Parameter name | Type      | Example                           |
| -------------- | --------- | --------------------------------- |
| `email`        | text      | john@doe.com                      |
| `password`     | text      | password                          |

###### On success
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `fname`        | text      | John Doe                                      |
| `email`        | text      | john@doe.com                                  |
| `mobile_num`   | text      | 09XX XXX XXXX                                 |
| `birthdate`    | text      | 1994-12-11                                    |
| `sex`          | text      | Male                                          |
| `country`      | text      | Philippines                                   |
| `city`         | text      | Marikina City                                 |
| `interests`    | array     | `["Fashion","Games"]`                         |
| `avatar`       | url       | `http://example.com/uploads/avatar_1.jpg`     |
| `status`       | boolean   | `true`                                        |
| `message`      | text      | Relevant success message                      |

### register
#### Request: POST | Response: JSON  
Use this web service to enlist the user (as an inactive user) to the database whilst sending a mail confirmation to the his/her email
###### Expects
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `fname`        | text      | John Doe                                      |
| `email`        | text      | john@doe.com                                  |
| `password`     | text      | mysupersecretpassword                         |
| `mobile_num`   | text      | 09XX XXX XXXX                                 |
| `birthdate`    | text      | 1994-12-11                                    |
| `sex`          | text      | Male                                          |
| `country`      | text      | Philippines                                   |
| `city`         | text      | Marikina City                                 |
| `interests`    | text      | Fashion,Games                                 |

###### On success
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `status`       | boolean   | `true`                                        |
| `message`      | text      | Relevant success message                      |


### confirm_register
#### Request: POST | Response: JSON | Requires SMTP configuration from the client  
Requests to this webservice always comes from the user's email via hypertext link.
###### Expects
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `email`        | text      | john@doe.com                                  |

###### On success
Displays a static confirmation page

### live_radio  
#### Response: JSON  
Use this to get the livestream radio link from the database
###### Expects
Nothing

###### On success
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `status`       | boolean   | `true`                                        |
| `link`         | url       | `http://hyades.shoutca.st:8146/`              |

### live_video
#### Response: JSON  
Use this to get the livestream video link from the database
###### Expects
Nothing

###### On success
| Parameter name | Type      | Example                                                        |
| -------------- | --------- | -------------------------------------------------------------- |
| `status`       | boolean   | `true`                                                         |
| `link`         | url       | `http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8` |

### change_password
#### Request: POST | Response: JSON  
Use this to change the user's password
###### Expects
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `id`           | text      | 1                                             |
| `password`     | text      | mysupersecretpassword                         |

###### On success
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `status`       | boolean   | `true`                                        |
| `message`      | text      | Relevant success message                      |

### check_email
#### Request: POST | Response: JSON  
Use this to check if the user's email is not yet existing in the database
###### Expects
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `email`        | text      | john@doe.com                                  |

###### On success
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `status`       | boolean   | `true`                                        |
| `message`      | text      | Relevant success message                      |

### forgot_password
#### Request: POST | Response: JSON | Requires SMTP configuration from the client
Use this function to replace the user's old password with a new one via email.
###### Expects
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `email`        | text      | john@doe.com                                  |

###### On success
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `status`       | boolean   | `true`                                        |
| `message`      | text      | Relevant success message                      |

### get_months
#### Response: JSON  
Use this to get the list of months, their associated year, and their integer equivalent from the database.
###### Expects
Nothing

###### On success
| Parameter name | Type                     | Example                                       |
| -------------- | ------------------------ | --------------------------------------------- |
| `status`       | boolean                  | `true`                                        |
| `months`       | array[object,object,...] | `{"month_name":"january","month_id":"1", "year":"2016"}, {"month_name":"february","month_id":"2","year":"2016"},...`      |

### home_picture
#### Response: JSON  
Use this to get the app homescreen information from the database
###### Expects
Nothing

###### On success
| Parameter name    | Type      | Example                                       |
| ----------------- | --------- | --------------------------------------------- |
| `status`          | boolean   | `true`                                        |
| `thumbnail`       | url       | `http://example.com/uploads/thumbnail.jpg`    |
| `greeting`        | text      | Some greeting                                 |
| `welcome_message` | text      | Some very cool welcome message                |

### vod_category
#### Response: JSON  
Use this to get the list of VOD categories from the database
###### Expects
Nothing

###### On success
| Parameter name | Type                     | Example                                       |
| -------------- | ------------------------ | --------------------------------------------- |
| `status`       | boolean                  | `true`                                        |
| `vod_category`       | array[object,object,...] | `{"id":"1","title":"Club","thumbnail":"http://example.com/uploads/image.jpg"},{"id":"2","title":"EDM","thumbnail":"http://example.com/uploads/image.jpg"},...`      |

### edit_avatar
#### Request: POST | Response: JSON
Use this function to upload or replace the user's old avatar 
###### Expects
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `id`           | text      | 21                                            |
| `avatar`       | file      | `some_actual_file.jpg`                        |

###### On success
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `status`       | boolean   | `true`                                        |
| `message`      | text      | Relevant success message                      |
| `path`         | url       | `http://example.com/uploads/image.jpg`        |

### edit_profile
#### Request: POST | Response: JSON  
Use this web service to update information about the user
###### Expects
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `id`           | text      | 21                                            |
| `fname`        | text      | John Doe                                      |
| `email`        | text      | john@doe.com                                  |
| `password`     | text      | mysupersecretpassword                         |
| `mobile_num`   | text      | 09XX XXX XXXX                                 |
| `birthdate`    | text      | 1994-12-11                                    |
| `sex`          | text      | Male                                          |
| `country`      | text      | Philippines                                   |
| `city`         | text      | Marikina City                                 |
| `interests`    | text      | Fashion,Games                                 |

###### On success
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `status`       | boolean   | `true`                                        |
| `message`      | text      | Relevant success message                      |

### get_world
#### Response: JSON  
Use this to get the list of all countries
###### Expects
Nothing

###### On success
| Parameter name | Type                     | Example                                       |
| -------------- | ------------------------ | --------------------------------------------- |
| `status`       | boolean                  | `true`                                        |
| `countries`    | array                    | `["Afghanistan", "Albania", "Algeria", ...]`  |


### get_world
#### Request: POST | Response: JSON  
Use this to get the list of all cities of a certain country
###### Expects
| Parameter name | Type      | Example                                       |
| -------------- | --------- | --------------------------------------------- |
| `country`      | text      | Afghanistan                                   |

###### On success
| Parameter name | Type                     | Example                                       |
| -------------- | ------------------------ | --------------------------------------------- |
| `status`       | boolean                  | `true`                                        |
| `cities`       | array                    | `["Herat","Kabul","Kandahar", ...]`            |

### vod_list  
#### Request: POST | Response: JSON  
Use this to get an array of VOD objects from the database. Also provide a positive `counter` integer for the pagination. (eg. `1` is equal to page 1, `2` is for page 2, and so on...)  
**Note:** Returns 10 items per page
###### Expects
| Parameter name | Type      | Example                        |
| -------------- | --------- | ------------------------------ |
| `category_id`  | text      | 1                              |
| `counter`      | text      | 1                              |

###### On success
| Parameter name | Type                     | Example                                       |
| -------------- | ------------------------ | --------------------------------------------- |
| `status`       | boolean                  | `true`                                        |
| `vod_videos`   | array[object,object,...] | `{"id":"1", "title":"One Punch  Man", "uploader":"Saitama", "link":"http://example.com/uploads.opm.mp4", "duration":"0:03", "thumbnail":"http://example.com/uploads/image.jpg"}, ...` |

### gifts  
#### Request: POST | Response: JSON  
Use this to get an array of gift objects from the database. Also provide a positive `counter` integer for the pagination. (eg. `1` is equal to page 1, `2` is for page 2, and so on...)  
**Note:** Returns 10 items per page
###### Expects
| Parameter name | Type      | Example                        |
| -------------- | --------- | ------------------------------ |
| `counter`      | text      | 1                              |

###### On success
| Parameter name | Type                     | Example                                       |
| -------------- | ------------------------ | --------------------------------------------- |
| `status`       | boolean                  | `true`                                        |
| `gifts     `   | array[object,object,...] | `{"id":"1", "title":"Some title", "sub_title":"Some sub title", "description":"Grumpy wizards make toxic brew for evil queen and jack", "start_date":"2016-10-12", "end_date":"2016-10-15", "thumbnail":"http://example.com/uploads/image.jpg"}, ...` |

### events_list  
#### Request: POST | Response: JSON  
Use this to get an array of event objects from the database. Also provide a positive `counter` integer for the pagination. (eg. `1` is equal to page 1, `2` is for page 2, and so on...)  
**Note:** Returns 10 items per page
###### Expects
| Parameter name | Type      | Example                        |
| -------------- | --------- | ------------------------------ |
| `month_id`     | text      | 10                             |
| `year`         | text      | 8                              |
| `counter`      | text      | 1                              |

###### On success
| Parameter name | Type                     | Example                                       |
| -------------- | ------------------------ | --------------------------------------------- |
| `status`       | boolean                  | `true`                                        |
| `gifts     `   | array[object,object,...] | `{"id":"1", "title":"Some title", "sub_title":"Some sub title", "location":"SM Mall of Asia", "date":"2016-10-12", "time":"15:00:01", "description":"Grumpy wizards make toxic brew for evil queen and jack", "thumbnail":"http://example.com/uploads/image.jpg"}, ...` |