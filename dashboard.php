<?php 
#INCLUDES
include('jp_library/jp_lib.php');

if(isset($_POST['email']) && isset($_POST['password'])){
    
 
    #GET THE ID AND FULL NAME
    $params['table'] = "admin";
    $params['where'] = "email = '" . $_POST['email'] . "'";
    
    $row = mysqli_fetch_assoc(jp_get($params));
    
    $id = $row['id'];
    $full_name = $row['fname'] . ' ' . $row['lname'];
    
    unset($params);
    
    $params['table'] = "admin";
    $params['where'] = "id = '" . $id . "'";
    
    $row = mysqli_fetch_assoc(jp_get($params));
    
    if(sha1($_POST['password']) == $row['password']){
        
        $_SESSION['full_name'] = $full_name;
        
        $_SESSION['is_logged_in'] = 1;     
        
        $_SESSION['my_id'] = $id;

    }
    else{
        $_SESSION['err_msg'] = "Wrong email or password.";
        header("Location: " . "login.php");
        die();    
    }
    
    
}
else if(!isset($_SESSION['is_logged_in'])){
    header("Location: " . "login.php");
    die();
}

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
    <section id="container">
        <!--header start-->
        <header class="header white-bg">
            <?php
            if($LEFT_SIDEBAR)
                {
                    echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
                }
            ?>
                <!--logo start-->
                <?php if($LOGO)
                {
                    include('logo.php');
                }
            ?>
                <!--logo end-->
                <div class="nav notify-row" id="top_menu">
                    <!--  notification start -->
                    <?php if($NOTIFICATION) { 
                include('notification.php'); 
                } ?>
                    <!--  notification end -->
                </div>
                <?php include('top-nav.php'); ?>
        </header>
        <!--header end-->
        <!--sidebar start-->
        <?php 
            if($LEFT_SIDEBAR){ 
            include('left-sidebar.php');
           }
        ?>
        <!--sidebar end-->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <!-- page start-->Your content here.

                <!-- page end-->
            </section>
        </section>
        <!--main content end-->
        <!-- Right Slidebar start -->
        <?php 
            if($RIGHT_SIDEBAR){ 
     include('right-sidebar.php');
            }
    ?>
        <!-- Right Slidebar end -->
        <!--footer start-->
        <?php include('footer.php'); ?>
        <!--footer end-->
    </section>
    <?php include('scripts.php'); ?>
</body>

</html>
