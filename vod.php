<?php
class Vod {

    private $title;
    private $vod_category;
    private $uploader;
    private $vod_url;
    private $thumbnail_url;
    private $duration;
    private $id;

    function __construct() {
        
    }
    public function setTitle($arg) {
        $this->title = $arg;
    }
    public function setVodCategory($arg) {
        $this->vod_category = $arg;
    }
    public function setUploader($arg) {
        $this->uploader = $arg;
    }
    public function setVodUrl($arg) {
        $this->vod_url = $arg;
    }
    public function setThumbnailUrl($arg) {
        $this->thumbnail_url = $arg;
    }
    public function setDuration($arg) {
        $this->duration = $arg;
    }
    public function setId($arg) {
        $this->id = $arg;
    }

    public function getVod() {
        $res = array();
        $res['data']['status'] = true;
        $res['data']['category'] = "vod";
        
        $res['data']['id'] = $this->id;
        $res['data']['title'] = $this->title;
        $res['data']['vod_category'] = $this->vod_category;
        $res['data']['uploader'] = $this->uploader;
        $res['data']['link'] = $this->vod_url;
        $res['data']['thumbnail'] = $this->thumbnail_url;
        $res['data']['duration'] = $this->duration;
        
        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }

}
