<?php
#INCLUDES
include('jp_library/jp_lib.php');
include('jp_library/s_upload.php');

if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}

if (isset($_POST['pp_desc'])) {

    //    $status_msg = $s_upload['msg'];

    $params['table'] = 'privacy_policy';
    $params['where'] = "id = 1";
    $params['data'] = $_POST;
    $result = jp_update($params);

    if ($result) {
        $status_msg = ' Update successful.';
        $all_ok = 1;
    }


}

unset($params);

#VIEWING
$params['table'] = "privacy_policy";
$privacy = mysqli_fetch_assoc(jp_get($params));

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
    <section id="container">
        <!--header start-->
        <header class="header white-bg">
            <?php
            if ($LEFT_SIDEBAR) {
                echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
            }
            ?>
            <!--logo start-->
            <?php if ($LOGO) {
                include('logo.php');
            }
            ?>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <?php if ($NOTIFICATION) {
                    include('notification.php');
                } ?>
                <!--  notification end -->
            </div>
            <?php include('top-nav.php'); ?>
        </header>
        <!--header end-->
        <!--sidebar start-->
        <?php
        if ($LEFT_SIDEBAR) {
            include('left-sidebar.php');
        }
        ?>
        <!--sidebar end-->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <!-- page start-->
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading"> Privacy policy
                            <br> <sub
                            <?php if (isset($all_ok)) {
                                if ($all_ok) {
                                    echo "class='status-ok'";
                                } else {
                                    echo "class='status-not-ok'";
                                }
                                ?>
                                <?php } ?>
                                ><?php echo isset($status_msg) ? $status_msg : ''; ?></sub>
                            </header>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form"
                                action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                                enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="welcome_message" class="col-lg-2 col-sm-2 control-label">Privacy policy
                                    </label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" id="pp_desc" name="pp_desc" style="resize:vertical; height:20em"
                                        placeholder="Privacy policy"><?php echo $privacy['pp_desc']?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button type="submit" class="btn btn-info">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--                                     page end-->
                    </section>
                </section>
                <!--main content end
                <!-- Right Slidebar start -->
                <?php
                if ($RIGHT_SIDEBAR) {
                    include('right-sidebar.php');
                }
                ?>
                <!-- Right Slidebar end -->
                <!--footer start-->
                <?php include('footer.php'); ?>
                <!--footer end-->
            </section>
            <?php include('scripts.php'); ?>

        </body>

        </html>
