<?php
#INCLUDES
include('jp_library/jp_lib.php');

if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}

$DYNAMIC_TABLE = true;

if (isset($_POST['ban_id'])) {
    $ban_id = $_POST['ban_id'];
    $params['table'] = "users";
    $params['where'] = "id = '$ban_id'";
    $params['data'] = array(
        "status" => '-1',
    );

    $result = jp_update($params);
    if ($result) {
        #TODO: fix this
        $status_msg = "User banned";
        $all_ok = 1;
    }
}

if (isset($_POST['unban_id'])) {
    $unban_id = $_POST['unban_id'];
    $params['table'] = "users";
    $params['where'] = "id = '$unban_id'";
    $params['data'] = array(
        "status" => '1',
    );

    $result = jp_update($params);
    if ($result) {
        #TODO: fix this
        $status_msg = "User set to active";
        $all_ok = 1;
    }
}

unset($params);
unset($result);
#VIEWING
$params['table'] = "users";
$admin = jp_get($params);

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
<section id="container" class="sidebar-closed">
    <!--header start-->
    <header class="header white-bg">
        <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
        <!--logo start-->
        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
            <!--  notification end -->
        </div>
        <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- page start-->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading"> Users Table <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
                            <br> <sub
                                <?php if (isset($all_ok)) {
                                    if ($all_ok) {
                                        echo "class='status-ok'";
                                    } else {
                                        echo "class='status-not-ok'";
                                    }
                                    ?>
                                <?php } ?>
                            ><?php echo isset($status_msg) ? $status_msg : ''; ?></sub>
                        </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Birthdate</th>
                                        <th>Sex</th>
                                        <th>Country</th>
                                        <th>City</th>
                                        <th>Interests</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($admin as $row) {
                                        echo '<tr>';
                                        echo '<td>' . $row["id"] . '</td>';
                                        echo '<td>' . $row["fname"] . " " . $row["lname"] . '</td>';
                                        echo '<td>' . $row["email"] . '</td>';
                                        echo '<td>' . $row["mobile_num"] . '</td>';
                                        echo '<td>' . $row["birthdate"] . '</td>';
                                        echo '<td>' . $row["sex"] . '</td>';
                                        echo '<td>' . $row["country"] . '</td>';
                                        echo '<td>' . $row["city"] . '</td>';
                                        echo '<td>' . $row["interests"] . '</td>';
                                        echo '<td>';
                                        if ($row['status'] == 1) {
                                            echo "active";
                                        } elseif ($row['status'] == 0) {
                                            echo "inactive";
                                        } elseif ($row['status'] == -1) {
                                            echo "banned";
                                        }

                                        echo '</td>';
                                        echo '<td>'; ?>
                                        <?php

                                        if ($row['status'] == 1) {
                                            ?>
                                            <form style='display:inline;'
                                                  onsubmit="return confirm('Are you sure you want to ban that user?');"
                                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">
                                                <input type="hidden" name="ban_id" value="<?php echo $row["id"]; ?>">
                                                <button class="btn btn-danger btn-xs" type="submit"><i
                                                        class="fa fa-ban"></i></button>
                                            </form>
                                            <?php
                                        } else if ($row['status'] == 0){
                                            ?>
                                            <form style='display:inline;'
                                                  onsubmit="return confirm('Are you sure you want to activate that user?');"
                                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">
                                                <input type="hidden" name="unban_id" value="<?php echo $row["id"]; ?>">
                                                <button class="btn btn-info btn-xs" type="submit"><i
                                                        class="fa fa-unlock-alt"></i></button>
                                            </form>
                                            <?php
                                        }else if ($row['status'] == -1){
                                            ?>
                                            <form style='display:inline;'
                                                  onsubmit="return confirm('Are you sure you want to unban that user?');"
                                                  action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">
                                                <input type="hidden" name="unban_id" value="<?php echo $row["id"]; ?>">
                                                <button class="btn btn-primary btn-xs" type="submit"><i
                                                        class="fa fa-check-circle"></i></button>
                                            </form>
                                            <?php
                                        }

                                        ?>

                                        <?php
                                        echo '</td>';
                                        echo '</tr>';
                                    }

                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Birthdate</th>
                                        <th>Sex</th>
                                        <th>Country</th>
                                        <th>City</th>
                                        <th>Interests</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
</section>
<?php include('scripts.php'); ?>
</body>

</html>
