<?php

$LOCAL_MODE = false; //SET TO TRUE ONLY IN LOCAL COMPUTER

#MODULE CONTROLS
$AVATAR = true;
$LEFT_SIDEBAR = true;
$RIGHT_SIDEBAR = false;
$SEARCH = false;
$NOTIFICATION = false;
$LOGO = true;

#DATETIME SETTINGS
date_default_timezone_set('Asia/Manila');

#DEFAULT MODULE INITIALIZATIONS
#OVERRIDE ON YOUR PAGE IF NECESSARY
$DYNAMIC_TABLE = false;
$PICKERS = false; #FOR TIMEPICKERS, DATEPICKERS, ETC
$LOADER = false; #FOR LOADER DUH

#SITE CONFIG
$SITE_NAME = '90.7 Love Radio';

#URL CONTROLS
$PAGE_NAME = basename($_SERVER['PHP_SELF']);

if ($LOCAL_MODE)
    $BASE_URL = "http://localhost/mbc-love-radio/";
else
    $BASE_URL = "http://loveradio.com.ph/cms/";
    // $BASE_URL = "http://betaprojex.com/loveradio-app/";

error_reporting(E_ERROR);
