<?php
/*
* s_upload means Static Upload
* I don't want to spend so much time thinking
* of a name of a PHP file, hence s_upload
*/

function s_upload($target_dir, $file_prefix, $file_to_upload, $id)
{
    #ADD USING STATIC PREFIXES
    if ($_POST) {

        #EXAMPLE VARIABLES
        //    $target_dir = "uploads/";
        //    $file_prefix = 'home_';
        //    $file_to_upload = $_FILES['fileToUpload'];

        $path = $file_to_upload['name'];
        $original_ext = pathinfo($path, PATHINFO_EXTENSION); #GET FILE EXTENSION
        $ext = 'jpg';

        $target_file = $target_dir . $file_prefix . $id . time() . "." . $ext;
        $uploadOk = 1;
        // Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($file_to_upload["tmp_name"]);
            if ($check !== false) {
                $update_msg = "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                $update_msg = "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check file size
        if ($file_to_upload["size"] > 500000000) {
            $update_msg = "File is too large.";
            $uploadOk = 0;
        }

        // Allow certain file formats
        if ($original_ext != "jpg" && $original_ext != "png" && $original_ext != "jpeg"
            && $original_ext != "gif"
        ) {
            $s_upload['status'] = false;
            $s_upload['msg'] = "Only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
//            $update_msg .= " Sorry, your file was not uploaded";
            // if everything is ok, try to upload file
            $s_upload['status'] = false;
            $s_upload['msg'] = " Sorry, your file was not uploaded";
        } else {

            if (file_exists($target_file)) {
                #overwrite file with same name
                move_uploaded_file($file_to_upload["tmp_name"], $target_file);
                $s_upload['status'] = true;
                $s_upload['msg'] = "File overwritten.";
                $s_upload['path'] = $target_file;
            } else if (move_uploaded_file($file_to_upload["tmp_name"], $target_file)) {
                #upload file for the first time
                $s_upload['status'] = true;
//                $s_upload['msg'] = "The file " . basename($file_to_upload["name"]) . " has been uploaded";
                $s_upload['msg'] = "";
                $s_upload['path'] = $target_file;
            } else {
                $s_upload['status'] = false;
                $s_upload['msg'] = "Sorry, there was an error uploading your file";
            }
        }
        return $s_upload;

    }
}
