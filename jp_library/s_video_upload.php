<?php
/*
* s_upload means Static Upload
* I don't want to spend so much time thinking
* of a name of a PHP file, hence s_upload
* TODO fix this shit
*/

function s_video_upload($target_dir, $file_prefix, $file_to_upload, $id)
{
    #ADD USING STATIC PREFIXES
    if ($_POST) {

        #EXAMPLE VARIABLES
        //    $target_dir = "uploads/";
        //    $file_prefix = 'home_';
        //    $file_to_upload = $_FILES['fileToUpload'];

        $path = $file_to_upload['name'];
        $original_ext = pathinfo($path, PATHINFO_EXTENSION); #GET FILE EXTENSION
        $ext = 'mp4';

        $target_file = $target_dir . $file_prefix . $id . time() . "." . $ext;
        $uploadOk = 1;
        // Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($file_to_upload["tmp_name"]);
            if ($check !== false) {
                $s_video_upload['msg'] = "File is an image - " . $check["mime"] . ".";
                $uploadOk = 0;
            } else {
                $s_video_upload['msg'] = "File is not an image.";
                $uploadOk = 1;
            }
        }
        #TODO make it so it doesn't accept image files
        // Check file size
        if ($file_to_upload["size"] > 4000000000) {
            $s_video_upload['msg'] = "File is too large.";
            $uploadOk = 0;
        }

        // Allow certain file formats
        if ($original_ext == "mp4" || $original_ext == "wmv") {
            #secretly allow and convert wmv videos to MP4
            $uploadOk = 1;
        } else {
            $uploadOk = 0;
            $s_video_upload['msg'] = "Only MP4 and WMV files are allowed.";
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
//            $update_msg .= " Sorry, your file was not uploaded";
            // if everything is ok, try to upload file
            $s_video_upload['status'] = false;
            $s_video_upload['msg'] .= " Sorry, your file was not uploaded ";
        } else {

            if (file_exists($target_file)) {
                #overwrite file with same name
                move_uploaded_file($file_to_upload["tmp_name"], $target_file);
                $s_video_upload['status'] = true;
                $s_video_upload['msg'] = "File overwritten.";
                $s_video_upload['path'] = $target_file;
            } else if (move_uploaded_file($file_to_upload["tmp_name"], $target_file)) {
                #upload file for the first time
                $s_video_upload['status'] = true;
                $s_video_upload['msg'] = "The file " . basename($file_to_upload["name"]) . " has been uploaded";
                $s_video_upload['msg'] = "";
                $s_video_upload['path'] = $target_file;
            }
            else{
                $s_video_upload['status'] = false;
                $s_video_upload['msg'] = " Sorry, your file was not uploaded ";
            }
        }
        return $s_video_upload;

    }
}
