<?php
include('jp_library/jp_lib.php');
include('jp_library/s_upload.php');

if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}
if (!isset($_POST['edit_id'])) {
    header("Location: " . "events.php");
    die();
}
if (isset($_POST['update'])) {

    $all_ok = 0;

    #convert time format
    $_POST['time'] = date('H:i:s', strtotime($_POST['time']));
    #HACK

    $date = $_POST['date'];
    $date = explode("-", $date);
    $_POST['date'] = date('Y-m-d', strtotime("$date[2]-$date[0]-$date[1]"));

    $params['table'] = 'events';
    $params['data'] = array(
        'title' => $_POST['title'],
        'sub_title' => $_POST['sub_title'],
        'description' => $_POST['description'],
        'location' => $_POST['location'],
        'weblink' => $_POST['weblink'],
        'date' => $_POST['date'],
        'month_id' => $date[0],
        'time' => $_POST['time'],
    );
    $edit_id = $_POST['edit_id'];
    $params['where'] = "id = $edit_id";

    $result = jp_update($params);

    $s_upload['status'] = 1;

    if ($_FILES['thumbnail']['name'] != "") {
        $s_upload = s_upload("uploads/", "event_thumbnail_", $_FILES["thumbnail"], $_POST['edit_id']);

        if ($s_upload['status']) { #if upload is successful
            $thumbnail = $BASE_URL . $s_upload['path']; #get file URL

            unset($params); #unset our favourite variable right here
            unset($result); #unset our favourite variable right here

            $params['table'] = 'events';
            $params['where'] = "id = $edit_id";

            $params['data'] = array(
                "thumbnail" => $thumbnail,
            );

            $result = jp_update($params);
        }
    }

    if ($s_upload['status']) {
        $status_msg = "Updated successfully.<br><a href='" . $_POST['edit_page'] . "'>&laquo; Back to events</a>";
        $all_ok = 1;
    } else {
        $status_msg = "Update failed.";
    }
}

$DYNAMIC_TABLE = true;
$PICKERS = true;

$params['table'] = "events";
$params['where'] = "id = '" . $_POST['edit_id'] . "'";
$result = mysqli_fetch_assoc(jp_get($params));

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
<section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
        <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
        <!--logo start-->
        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
            <!--  notification end -->
        </div>
        <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- page start-->

            <div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?= $_POST['edit_page'] ?>">&laquo; Back to events</a></li>
                    <li>Edit</a></li>
                </ul>
                <!--breadcrumbs end -->
            </div>

            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading"> Edit events
                        <br> <sub
                            <?php if (isset($all_ok)) {
                                if ($all_ok) {
                                    echo "class='status-ok'";
                                } else {
                                    echo "class='status-not-ok'";
                                }
                                ?>
                            <?php } ?>
                        ><?php echo isset($status_msg) ? $status_msg : ''; ?></sub>
                    </header>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form"
                              action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post"
                              enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-lg-2 col-sm-2 control-label">Title</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="title" name="title"
                                           placeholder="Event title" required value="<?= $result['title'] ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="sub_title" class="col-lg-2 col-sm-2 control-label">Sub title</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="sub_title" name="sub_title" required
                                           placeholder="Sub title" value="<?= $result['sub_title'] ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-lg-2 col-sm-2 control-label">Description</label>
                                <div class="col-lg-10">
                                        <textarea style="resize:vertical" class="form-control" placeholder="Description"
                                                  name="description" id="description"
                                                  required><?= $result['description'] ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="location" class="col-lg-2 col-sm-2 control-label">Location</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="location" name="location" required
                                           placeholder="Event location" value="<?= $result['location'] ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="weblink" class="col-lg-2 col-sm-2 control-label">Web link</label>
                                <div class="col-lg-10">
                                    <input type="url" class="form-control" id="weblink" name="weblink" required
                                           placeholder="Link to external websites" value="<?= $result['weblink'] ?>"></div>
                            </div>
                            <?php $result['date'] = date('m-d-Y', strtotime($result['date'])); ?>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Date</label>
                                <div class="col-lg-10">
                                    <input class="form-control form-control-inline input-medium default-date-picker"
                                           required placeholder="MM-DD-YYYY"
                                           name="date" type="text" value="<?= $result['date'] ?>">
                                </div>
                            </div>


                            <?php $result['time'] = date('h:i A', strtotime($result['time'])); ?>
                            <div class="form-group">
                                <label for="time" class="col-lg-2 col-sm-2 control-label">Time</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="" name="time" required pattern="\b((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))"
                                           placeholder="Time" value="<?= $result['time'] ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="thumbnail_preview" class="col-lg-2 col-sm-2 control-label">Thumbnail
                                    preview</label>
                                <div class="col-lg-10">
                                    <img class="img-responsive" src="<?= $result['thumbnail'] ?>"/></div>
                            </div>
                            <div class="form-group">
                                <label for="home_file" class="col-lg-2 col-sm-2 control-label">Thumbnail</label>
                                <div class="col-lg-10">
                                    <input type="file" id="thumbnail" name="thumbnail">
                                    <p class="help-block">Image files only. (jpeg, jpg, png, gif)</p>
                                </div>
                            </div>
                            <input type="hidden" value="1" name="update">
                            <input type="hidden" value="<?= $_POST['edit_id'] ?>" name="edit_id">
                            <input type="hidden" value="<?= $_POST['edit_page'] ?>" name="edit_page">
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>


            <!-- page end-->
        </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
</section>
<?php include('scripts.php'); ?>
</body>

</html>
