<footer class="site-footer">
    <div class="text-center"> 2016 &copy;
        <?php echo $SITE_NAME ?>
        <a href="#" class="go-top"> <i class="fa fa-angle-up"></i> </a>
    </div>
</footer>
