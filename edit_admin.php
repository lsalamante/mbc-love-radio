<?php
#FOR TEMPLATE PURPOSES ONLY
#INCLUDES
include('jp_library/jp_lib.php');

if (!isset($_SESSION['is_logged_in'])) {
    header("Location: " . "login.php");
    die();
}
if (!isset($_POST['edit_id'])) {
    header("Location: " . "index.php");
    die();
}
if (isset($_POST['update'])) {
    
    $all_ok = 0;

    $params['table'] = 'admin';
    $params['data'] = array(
        'fname' => $_POST['fname'],
        'lname' => $_POST['lname'],
        'email' => $_POST['email'],
    );
    $edit_id = $_POST['edit_id'];
    $params['where'] = "id = $edit_id";

    $result = jp_update($params);
    if ($result) {
        $status_msg = "Updated successfully.<br><a href='" . $_POST['edit_page'] . "'>&laquo; Back to admin</a>";
        $all_ok = 1;
    } else {
        $status_msg = "Update failed.";
    }
}

$params['table'] = "admin";
$params['where'] = "id = '" . $_POST['edit_id'] . "'";
$result = mysqli_fetch_assoc(jp_get($params));

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body>
<section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
        <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
        <!--logo start-->
        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
        <!--logo end-->
        <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
            <!--  notification end -->
        </div>
        <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- page start-->

            <div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?= $_POST['edit_page'] ?>">&laquo; Back to admin</a></li>
                    <li>Edit</a></li>
                </ul>
                <!--breadcrumbs end -->
            </div>

            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading"> Edit admin
                     <br> <sub
                            <?php if (isset($all_ok)) {
                                if ($all_ok) {
                                    echo "class='status-ok'";
                                } else {
                                    echo "class='status-not-ok'";
                                }
                                ?>
                            <?php } ?>
                        ><?php echo isset($status_msg) ? $status_msg : ''; ?></sub></header>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form"
                              action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?> method="post">
                            <div class="form-group">
                                <label for="fname" class="col-lg-2 col-sm-2 control-label">First Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="fname" name="fname"
                                           placeholder="First name" value="<?= $result['fname'] ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="lname" class="col-lg-2 col-sm-2 control-label">Last Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="lname" name="lname"
                                           placeholder="Last name" value="<?= $result['lname'] ?>"></div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-lg-2 col-sm-2 control-label">Email</label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="Email" value="<?= $result['email'] ?>" required>
                                    <p class="help-block">This field is required.</p>
                                </div>
                            </div>
                            <input type="hidden" value="1" name="update">
                            <input type="hidden" value="<?= $_POST['edit_id'] ?>" name="edit_id">
                            <input type="hidden" value="<?= $_POST['edit_page'] ?>" name="edit_page">
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>


            <!-- page end-->
        </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
</section>
<?php include('scripts.php'); ?>
</body>

</html>
