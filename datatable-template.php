<section class="panel">
    <header class="panel-heading">
        Dynamic Table
        <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <table class="display table table-bordered table-striped" id="dynamic-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th class="hidden-phone">Engine version</th>
                        <th class="hidden-phone">CSS grade</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th class="hidden-phone">Engine version</th>
                        <th class="hidden-phone">CSS grade</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</section>
