<?php

class Commercial
{

    private $title;
    private $sub_title;
    private $author;
    private $thumbnail;

    function __construct()
    {

    }

    public function setTitle($arg)
    {
        $this->title = $arg;
    }

    public function setAuthor($arg)
    {
        $this->author = $arg;
    }

    public function setSubTitle($arg)
    {
        $this->sub_title = $arg;
    }

    public function setThumbnailUrl($arg)
    {
        $this->thumbnail = $arg;
    }

    public function getCommercial()
    {
        $res = array();
        $res['data']['status'] = true;
        $res['data']['category'] = "commercial";

        $res['data']['title'] = $this->title;
        $res['data']['sub_title'] = $this->sub_title;
        $res['data']['author'] = $this->author;
        $res['data']['thumbnail'] = $this->thumbnail;

        $res['data']['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }

}
